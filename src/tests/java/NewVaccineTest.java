/*import app.domain.model.Vaccine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.text.ParseException;


/*public class NewVaccineTest {
    private static final String Brand = "Pfizer";
    private static final String Code = "qw23F";
    private static final String VaccineTechnology = "Toxoid";
    private static final String Description = "Tetanus vaccine made by Pfizer";
    private static final String AgeGroup = "18-24";
    private static final String DoseNumber = "2";
    private static final String Dosage = "100";
    private static final String TimeBetweenDoses = "15";
    Vaccine NewVaccine = new Vaccine(Code,Description, Brand, VaccineTechnology,AgeGroup,DoseNumber,Dosage,TimeBetweenDoses);
    public NewVaccineTest() throws ParseException {

    }
    @Test
    // A method that returns the brand of the vaccine.
    public void getBrand(){
        Assertions.assertEquals(Brand,NewVaccine.getBrand());
    }


    @Test
    // Setting the brand of the vaccine.
    public void setBrand(){

        this.NewVaccine.setBrand("Moderna");
        Assertions.assertEquals("Moderna", NewVaccine.getBrand());
    }


    @Test
    // A method that returns the code of the vaccine.
    public void getCode(){
        Assertions.assertEquals(Code, NewVaccine.getCode());
    }


    @Test
    // Setting the code of the vaccine.
    public void setCode(){
        this.NewVaccine.setCode("1S65c");
        Assertions.assertEquals("1S65c", NewVaccine.getCode());
    }

    @Test
    // A method that returns the vaccine technology of the vaccine.
    public void getVaccineTechnology(){
        Assertions.assertEquals(VaccineTechnology, NewVaccine.getVaccineTechnology());
    }


    @Test
    // Setting the vaccine technology of the vaccine.
    public void setVaccineTechnology(){
        this.NewVaccine.setVaccineTechnology("Viral Vector");
        Assertions.assertEquals("Viral Vector", NewVaccine.getVaccineTechnology());
    }


    @Test
    // A method that returns the description of the vaccine.
    public void getDescription(){
        Assertions.assertEquals(Description, NewVaccine.getDescription());
    }


    @Test
    // Setting the description of the vaccine.
    public void setDescription(){
        this.NewVaccine.setDescription("Hepatitis B vaccine made by Moderna");
        Assertions.assertEquals("Hepatitis B vaccine made by Moderna", NewVaccine.getDescription());
    }

    @Test
    // A method that returns the age group of the vaccine.
    public void getAgeGroup(){
        Assertions.assertEquals(AgeGroup, NewVaccine.getAgeGroup());
    }
    @Test
    // Setting the age group of the vaccine.
    public void setAgeGroup(){
        this.NewVaccine.setAgeGroup("45-55");
        Assertions.assertEquals("45-55", NewVaccine.getAgeGroup());
    }
    @Test
    // A method that returns the dose number of the vaccine.
    public void getDoseNumber(){
        Assertions.assertEquals(DoseNumber, NewVaccine.getDoseNumber());
    }
    @Test
    // Setting the dose number of the vaccine.
    public void setDoseNumber(){
        this.NewVaccine.setDoseNumber("3");
        Assertions.assertEquals("3", NewVaccine.getDoseNumber());
    }
    @Test
    // A method that returns the dosage of the vaccine.
    public void getDosage(){
        Assertions.assertEquals(Dosage, NewVaccine.getDosage());
    }
    @Test
    // Setting the dosage of the vaccine.
    public void setDosage(){
        this.NewVaccine.setDosage("100");
        Assertions.assertEquals("100", NewVaccine.getDosage());
    }
    @Test
    // A method that returns the time between doses of the vaccine.
    public void getTimeBetweenDoses(){
        Assertions.assertEquals(TimeBetweenDoses, NewVaccine.getTimeBetweenDoses());
    }
    @Test
    // Setting the time between doses of the vaccine.
    public void setTimeBetweenDoses(){
        this.NewVaccine.setTimeBetweenDoses("15");
        Assertions.assertEquals("15", NewVaccine.getTimeBetweenDoses());
    }

}
*/