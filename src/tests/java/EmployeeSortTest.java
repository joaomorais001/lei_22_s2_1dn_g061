import app.domain.model.BubbleSort;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.mappers.dto.UserDTO;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class EmployeeSortTest {
    private static final String name = "Claudio";
    private static final String email = "Claudio@isep.ipp.pt";
    private static final String name1 = "Andre";
    private static final String email1 = "Andre@isep.ipp.pt";

    private BubbleSort bubbleSort;
    String password = Utils.generatePassword();
    private AuthFacade authFacade;



    @Test
    // A test method that tests if the list is sorted by name.
    public void isSortedByName(){
        authFacade = new AuthFacade();
        bubbleSort = new BubbleSort();
        authFacade.addUserRole(String.valueOf("Nurse"),"Nurse");
        authFacade.addUserWithRole(name, email, password, "NURSE");
        authFacade.addUserWithRole(name1, email1, password, "NURSE");
        List<UserDTO> listasortedbubble =  bubbleSort.sortByName(authFacade.getUsersWithRole("NURSE"));
        List<UserDTO> listamanuallist =  authFacade.getUsersWithRole("NURSE");
        Assertions.assertNotEquals(listasortedbubble,listamanuallist);
    }

}