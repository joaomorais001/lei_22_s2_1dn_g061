
import app.controller.UserVaccineRecordController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class AddReverseTest {

    @Test
    public void testReveser() throws ParseException {

        final int SNSNumber = 162780105;
        LocalDate date= LocalDate.now();
        UserVaccineRecordController controller = new UserVaccineRecordController();
        controller.createUserVacRecord(SNSNumber,"ABC12","brand","1","25","Covid-19", date);
        controller.addReaction("Reação",SNSNumber);
        Assertions.assertTrue(controller.getUserVaccineRecord().get(0).getReaction()=="Reação");
    }
}
