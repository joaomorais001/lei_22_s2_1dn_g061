import app.controller.RegisterSNSUserController;
import app.controller.SortLegacyDataController;
import app.domain.model.*;
import app.domain.model.InsertionSort;
import app.domain.store.RegisterSNSUserStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import app.domain.model.VacCenter;


public class SortLegacyDataTest {

    @Test
    public void BubbleSortIsSorted() throws ParseException {
        BubbleSort bubbleSort = new BubbleSort();
        String[][] list = new String[2][8], listsortemanual = new String[2][8];
        String dados1 = "161593120;Spikevax;Primeira;21C16-05;5/30/2022 8:00;5/30/2022 8:24;5/30/2022 9:11;5/30/2022 9:43";
        String dados2 = "161593121;Spikevax;Primeira;21C16-05;5/30/2022 8:00;5/30/2022 8:00;5/30/2022 8:17;5/30/2022 8:51";
        String[] dadosf1 = dados1.split(";"), dadosf2 = dados2.split(";");
        list[0] = dadosf1;
        list[1] = dadosf2;
        listsortemanual[0] = dadosf2;
        listsortemanual[1] = dadosf1;
        String[][] listsortedbuble = bubbleSort.sortByTime(list, 5);
        Assertions.assertArrayEquals(listsortemanual[0], listsortedbuble[0]);
    }

    @Test
    public void InsertionSortIsSorted() throws ParseException {
        InsertionSort InsertionSort = new InsertionSort();
        String[][] list = new String[2][8], listsortemanual = new String[2][8];
        String dados1 = "161593120;Spikevax;Primeira;21C16-05;5/30/2022 8:00;5/30/2022 8:24;5/30/2022 9:11;5/30/2022 9:43";
        String dados2 = "161593121;Spikevax;Primeira;21C16-05;5/30/2022 8:00;5/30/2022 8:00;5/30/2022 8:17;5/30/2022 8:51";
        String[] dadosf1 = dados1.split(";"), dadosf2 = dados2.split(";");
        list[0] = dadosf1;
        list[1] = dadosf2;
        listsortemanual[0] = dadosf2;
        listsortemanual[1] = dadosf1;
        String[][] listsortedbuble = InsertionSort.sortByTime(list, 5,2);
        Assertions.assertArrayEquals(listsortemanual[0], listsortedbuble[0]);
    }

    @Test
    public void Howmanylines() throws IOException {
        SortLegacyDataController controller = new SortLegacyDataController();
        File file = new File("C:\\Users\\jnmte\\Documents\\lei_22_s2_1dn_g061\\testes.csv");
        int lines = controller.HowManyLines(file);
        Assertions.assertTrue(lines == 4998);
    }

    @Test
    public void ListwithSnsName() {
        RegisterSNSUserController registerSNSUserController = new RegisterSNSUserController();
        SortLegacyDataController sortLegacyDataController = new SortLegacyDataController();
        SNSUser snsUser1 = new SNSUser("dasd@gmail.com", "Ruas", 123456789, 161593120, "Joao Pedro", 12345678, new Date(), "male");
        registerSNSUserController.saveSNSUser(snsUser1);
        String dados1 = "161593120;Spikevax;Primeira;21C16-05;5/30/2022 8:00;5/30/2022 8:24;5/30/2022 9:11;5/30/2022 9:43";
        String[] dadosf1 = dados1.split(";");
        String[][] listfinal = new String[1][9];
        listfinal[0] = dadosf1;
        Assertions.assertTrue(sortLegacyDataController.getListWithExtraInfo(listfinal)[0][1] == "Joao Pedro");
    }

    @Test
    public void Importfile() throws IOException {
        SortLegacyDataController controller = new SortLegacyDataController();
        File filemanual = new File("C:\\Users\\jnmte\\Documents\\lei_22_s2_1dn_g061\\onlyfortestes.csv");
        File imported = controller.ImportFile("onlyfortestes.csv");
        Assertions.assertEquals(filemanual.getName(), imported.getName());
    }

    @Test
    public void SNSNumber() {
        RegisterSNSUserController registerSNSUserController = new RegisterSNSUserController();
        SortLegacyDataController sortLegacyDataController = new SortLegacyDataController();
        String name = "João";
        String email = "joao@isep.ipp.pt";
        String address = "Rua sao joao";
        int PhoneNumber = 123456789;
        int CCNumber = 12345678;
        int SNSNumber = 123456789;
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date birthday;
        {
            try {
                birthday = df.parse("24-09-2003");
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }
        Date birthdate = birthday;
        String sex = "masculino";
        SNSUser snsuser = new SNSUser(email, address, PhoneNumber, SNSNumber, name, CCNumber, birthdate, sex);
        registerSNSUserController.saveSNSUser(snsuser);
        Assertions.assertTrue(sortLegacyDataController.getSNSUserByNumber(SNSNumber)==snsuser);

    }
    @Test
    public void addLegacyData(){
        SortLegacyDataController sortLegacyDataController = new SortLegacyDataController();
        VacCenter vacCenter = new VacCenter(
                "Vacinação Guimaraes",
                "Rua de Guimaraes",
                "912626999",
                "019283746",
                "website@test.com",
                9,
                19,
                12,
                200);
        String[][] list = new String[1][8];
        String dados1 = "161593120;Spikevax;Primeira;21C16-05;5/30/2022 8:00;5/30/2022 8:24;5/30/2022 9:11;5/30/2022 9:43";
        list[0]= dados1.split(";");
        sortLegacyDataController.addLegacyData(vacCenter,list);
        Assertions.assertTrue(vacCenter.getLegacydata().get(0).contains("161593120"));
    }
}
