import app.controller.AnalysePerformanceController;
import app.domain.model.DepartureRecords;
import app.domain.model.EntryRecords;
import app.domain.model.VacCenter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class PerformanceAnalysisTests {

    @Test
    public void ensureEntryandDeparturePopulation(){
        VacCenter center = new VacCenter("Centro", "Rua","0","0","centro.pt", 8, 20, 5,10);

        center.addEntryRecord(LocalDateTime.now(), 123456789);
        center.addDepartureRecord(LocalDateTime.now(), 123456789);

        boolean res = center.getEntryRecords().size() == center.getDepartureRecords().size();

        Assertions.assertTrue(res);
    }

    @Test
    public void ensureReturnRightDateRecords(){
        VacCenter center = new VacCenter("Centro", "Rua","0","0","centro.pt", 8, 20, 5,10);
        AnalysePerformanceController controller = new AnalysePerformanceController();

        center.addEntryRecord(LocalDateTime.now(), 123456789);
        center.addDepartureRecord(LocalDateTime.now(), 123456789);

        List<EntryRecords> entries = controller.getEntryRecordsFromDate(LocalDate.now(), center.getEntryRecords());
        List<DepartureRecords> departures = controller.getDepartureRecordsFromDate(LocalDate.now(), center.getDepartureRecords());

        Assertions.assertEquals(entries.size(), departures.size());
    }

    @Test
    public void ensureDoesntReturnWrongDateRecords(){
        VacCenter center = new VacCenter("Centro", "Rua","0","0","centro.pt", 8, 20, 5,10);
        AnalysePerformanceController controller = new AnalysePerformanceController();

        String date = "10/10/2022 08:00";
        String date2 = "20/10/2022 10:00";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

        LocalDateTime localDateTime = LocalDateTime.parse(date, formatter);
        LocalDate localDate = LocalDate.parse(date2, formatter);

        center.addEntryRecord(localDateTime, 123456789);
        center.addDepartureRecord(localDateTime, 123456789);

        List<EntryRecords> entries = controller.getEntryRecordsFromDate(localDate, center.getEntryRecords());
        List<DepartureRecords> departures = controller.getDepartureRecordsFromDate(localDate, center.getDepartureRecords());

        boolean res = (entries.size() == 0 && departures.size() == 0);

        Assertions.assertTrue(res);
    }

}
