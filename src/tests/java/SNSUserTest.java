import app.domain.model.SNSUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SNSUserTest {


    private static final String name="joao";
    private static final String email="1211366@isep.ipp.pt";
    private static final int CCNumber = 30189829;
    private static final int PhoneNumber = 962041130 ;
    private static final int SNSNumber = 162780105;
    private static final String sex = "Masculino";
    private static final String Date = "24-04-2003";
    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    Date birthdate = df.parse(Date);

    private String address = "Rua da Asprela";

    SNSUser SNSUser = new SNSUser(email,address,PhoneNumber,SNSNumber,name, CCNumber,birthdate,sex);

    public SNSUserTest() throws ParseException {

    }

    /**
     * > This function tests the getName() function of the SNSUser class
     */
    @Test
    public void getName(){
        Assertions.assertEquals(name, SNSUser.getName());
    }

    /**
     * This function tests if the setName function works properly.
     */
    @Test
    public void setName(){
        this.SNSUser.setName("Ana Alves");
        Assertions.assertEquals("Ana Alves", SNSUser.getName());
    }

    /**
     * It tests the getEmail() function.
     */
    @Test
    public void getEmail(){
        Assertions.assertEquals(email, SNSUser.getEmail());
    }

    /**
     * It tests if the email is set correctly.
     */
    @Test
    public void setEmail(){
        this.SNSUser.setEmail("1211367@isep.ipp.pt");
        Assertions.assertEquals("1211367@isep.ipp.pt", SNSUser.getEmail());
    }

    /**
     * This function tests the getCCNumber() function in the SNSUser class
     */
    @Test
    public void getCCNumber(){
        Assertions.assertEquals(CCNumber, SNSUser.getCCNumber());
    }

    /**
     * This function tests the setCCNumber function in the SNSUser class
     */
    @Test
    public void setCCNumber(){
        this.SNSUser.setCCNumber(45691234);
        Assertions.assertEquals(45691234, SNSUser.getCCNumber());
    }

    /**
     * This function tests the getPhoneNumber() function in the SNSUser class
     */
    @Test
    public void getPhoneNumber(){
        Assertions.assertEquals(PhoneNumber, SNSUser.getPhoneNumber());
    }

    /**
     * This function tests the setPhoneNumber function in the SNSUser class
     */
    @Test
    public void setPhoneNumber(){
        this.SNSUser.setPhoneNumber(962041150);
        Assertions.assertEquals(962041150, SNSUser.getPhoneNumber());
    }

    /**
     * This function tests the getSNSNumber() function in the SNSUser class
     */
    @Test
    public void getSNSNumber(){
        Assertions.assertEquals(SNSNumber, SNSUser.getSNSNumber());
    }

    /**
     * This function tests the setSNSNumber function in the SNSUser class
     */
    @Test
    public void setSNSNumber(){
        this.SNSUser.setSNSNumber(12345678);
        Assertions.assertEquals(12345678, SNSUser.getSNSNumber());
    }

    /**
     * > This function tests the getSex() function of the SNSUser class
     */
    @Test
    public void getSex(){
        Assertions.assertEquals(sex, SNSUser.getSex());
    }

    /**
     * This function tests if the setSex function is working properly.
     */
    @Test
    public void setSex(){
        this.SNSUser.setSex("Feminino");
        Assertions.assertEquals("Feminino", SNSUser.getSex());
    }

    /**
     * This function tests the getBirthdate() function in the SNSUser class
     */
    @Test
    public void getBirthday(){
        Assertions.assertEquals(birthdate, SNSUser.getBirthdate());
    }

    /**
     * This function tests the setBirthdate function of the SNSUser class
     */
    @Test
    public void setBirthday() throws ParseException {
        this.SNSUser.setBirthdate("22-09-2003");
        String Date = "22-09-2003" ;
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date birthdate = df.parse(Date);

        Assertions.assertEquals(birthdate, SNSUser.getBirthdate());
    }

    /**
     * This function tests the getAddress() function in the SNSUser class
     */
    @Test
    public void getAddress(){
        Assertions.assertEquals(address, SNSUser.getAddress());
    }

    /**
     * It tests if the setAddress function is working properly.
     */
    @Test
    public void setAddress(){
        this.SNSUser.setAddress("Avenida São João");
        Assertions.assertEquals("Avenida São João", SNSUser.getAddress());
    }


    /**
     * This function tests the toString method of the SNSUser class
     */
    @Test
    public void toStringTest(){
        String data = String.format("\n--SNS User--\n" + "Name= " + name+ "\n" + "Email= " + email +"\n"+ "CC= " + CCNumber + "\n" + "Phonenumber= " + PhoneNumber +"\n"+ "SNSNumber= " + SNSNumber + "\n" + "Sex= " + sex + "\n" + "Birthdate= " + birthdate+"\n");
        Assertions.assertEquals(data, SNSUser.toString());
    }
}
