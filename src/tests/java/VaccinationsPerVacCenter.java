import app.controller.App;
import app.domain.model.*;
import app.domain.shared.Constants;
import app.domain.store.RegisterSNSUserStore;
import app.domain.store.VacCenterStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.w3c.dom.css.Counter;

import java.io.File;
import java.util.Properties;

public class VaccinationsPerVacCenter {

    private Company company;

    private VacCenterStore centerStore;


    private VaccinationsPerVacCenter() {
        this.company = App.getInstance().getCompany();
        this.centerStore = this.company.getVacStore();

    }

    @Test
    public void ensureDirectoryIscreated() {
        boolean res = false;
        File file = new File("Report");

        if (file.exists())
            res=true;
        Assertions.assertTrue(res);
    }

    @Test
    public void ensureFileIsCreated() {
        boolean res = false;
        File file = new File("Report/1");

        if (file.exists())
            res=true;
        Assertions.assertTrue(res);
    }

    @Test
    public void ConfirmReport() {
        boolean res = false;
        CounterReport novo = new CounterReport("Porto Vaccination center",40);
           if (novo.getVacName()=="Porto Vaccination center" && novo.getTotalNumberOfVaccinations()==40)
               res = true;
        Assertions.assertTrue(res);
    }

    @Test
    public void ConfirmCounter() {
        boolean res = false;
        VacCenter centro1;
        VacCenter centro2;
        centro1 =centerStore.getVacCenters().get(0);
        centro2 =centerStore.getVacCenters().get(1);

        RecoveryRoom user = new RecoveryRoom(123456789);
        centro1.addRecoveryRoom(user);
        centro2.addRecoveryRoom(user);

        System.out.println(centro1.getRecoveryRoom().size());
        System.out.println(centro2.getRecoveryRoom().size());

        if (centro1.getRecoveryRoom().size()==1 && centro2.getRecoveryRoom().size()==1)
            res = true;
        Assertions.assertTrue(res);
    }

}
