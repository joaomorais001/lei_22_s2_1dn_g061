import app.domain.model.Employee;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EmployeeTest {

    private static final String name = "Claudio";
    private static final String email = "Claudio@isep.ipp.pt";
    private static final String address = "Rua";
    private static final int phoneNumber = 123456789;
    private static final int CCNumber = 12345678;
    private static final String id = "12345";

    Employee employee = new Employee(name, email, address, phoneNumber, CCNumber, id);

    public EmployeeTest(){

    }

    @Test
    public void getName(){
        Assertions.assertEquals(name, employee.getName());
    }

    @Test
    public void setName(){
        this.employee.setName("Andre");
        Assertions.assertEquals("Andre", employee.getName());
    }

    @Test
    public void getAddress(){
        Assertions.assertEquals(address, employee.getAddress());
    }

    @Test
    public void setAddress(){
        this.employee.setAddress("Avenida");
        Assertions.assertEquals("Avenida", employee.getAddress());
    }

    @Test
    public void getPhoneNumber(){
        Assertions.assertEquals(phoneNumber, employee.getPhonenumber());
    }

    @Test
    public void setPhoneNumber(){
        this.employee.setPhoneNumber(987654321);
        Assertions.assertEquals(987654321, employee.getPhonenumber());
    }

    @Test
    public void getCCNumber(){
        Assertions.assertEquals(CCNumber, employee.getCCnumber());
    }

    @Test
    public void setCCNumber(){
        this.employee.setCCNumber(87654321);
        Assertions.assertEquals(87654321, employee.getCCnumber());
    }

    @Test
    public void getID(){
        Assertions.assertEquals(id, employee.getID());
    }

    @Test
    public void setID(){
        this.employee.setID("54321");
        Assertions.assertEquals("54321", employee.getID());
    }

    @Test
    public void toStringTest(){
        String data = String.format(" Name: %s\n Address: %s\n Email: %s\n Phone Number: %d\n CC number: %d\n ID: %s\n", name,
                address, email, phoneNumber, CCNumber, id);
        Assertions.assertEquals(data, employee.toString());
    }
}
