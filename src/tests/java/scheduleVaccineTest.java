import app.domain.model.SNSUser;
import app.domain.model.ScheduleVaccine;
import app.domain.model.VacCenter;
import app.domain.store.ScheduleVaccineStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class scheduleVaccineTest {


    private final ScheduleVaccineStore appointmentList;

    public static final String data = "24-02-2003";
    public static final String time = "11:00";
    private static final String name = "João";
    public static final int SNSNumber = 123456789;
    private static final String email = "joao@isep.ipp.pt";
    private static final String address = "Rua sao joao";
    private static final int PhoneNumber = 123456789;
    private static final int CCNumber = 12345678;
    static SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    static Date birthday;

    static {
        try {
            birthday = df.parse("24-09-2003");
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
    private static final Date birthdate = birthday;
    private static final String sex = "masculino";
    private static final String vaccineType = "covid";
    private static final String vacName = "centro";
    private static final String vacAddress = "Rua de Guimaraes";
    private static final String vacPhoneNumber = "912626999";
    private static final String vacFaxNumber = "019283746";
    private static final String vacWebsite = "website@test.com";
    private static final int vacOpeningHour = 9;
    private static final int vacClosingHour = 19;
    private static final int vacSlotDuration = 12;
    private static final int vacMaxVaccines = 200;

    SNSUser snsuser = new SNSUser(email,address,PhoneNumber,SNSNumber,name, CCNumber,birthdate,sex);

    VacCenter vacCenter = new VacCenter(vacName, vacAddress, vacPhoneNumber, vacFaxNumber, vacWebsite, vacOpeningHour, vacClosingHour, vacSlotDuration, vacMaxVaccines);



    public scheduleVaccineTest() {
        this.appointmentList = new ScheduleVaccineStore();
    }


    @Test
    public void ensureAppointmentIsSaved() {
        ScheduleVaccine Appointment = new ScheduleVaccine(data,time,vaccineType,vacName,SNSNumber);
        boolean res;
        res = ScheduleVaccineStore.addSNSUserAppointment(Appointment);
        Assertions.assertTrue(res);
    }


}
