import app.domain.model.SNSUser;
import app.domain.store.RegisterSNSUserStore;
import app.ui.console.utils.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SNSUserStoreTest {

    private final RegisterSNSUserStore userList;

    private static final String name = "João";
    private static final String email = "joao@isep.ipp.pt";
    private static final String address = "Rua sao joao";
    private static final int PhoneNumber = 123456789;
    private static final int CCNumber = 12345678;

    private static final int SNSNumber = 123456789;
    static SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    static Date birthday;

    static {
        try {
            birthday = df.parse("24-09-2003");
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private static final Date birthdate = birthday;

    private static final String sex = "masculino";


    public SNSUserStoreTest()  {
        userList = new RegisterSNSUserStore();
    }

    /**
     * Ensure password is generated
     */
    @Test
    public void ensurePasswordIsGenerated() {
        String password = Utils.generatePassword();
        boolean res;
        res = password.length() == 7;
        Assertions.assertTrue(res);
    }


    /**
     * This function is used to validate the SNSUser
     */
    @Test
    public void ensureValidateSNSUSer() {
        SNSUser snsuser = new SNSUser(email,address,PhoneNumber,SNSNumber,name, CCNumber,birthdate,sex);
        boolean res;
        res = RegisterSNSUserStore.validateSNSUser(snsuser);
        Assertions.assertTrue(res);
    }

    /**
     * This function tests the saveSNSUser function in the RegisterSNSUserStore class
     */
    @Test
    public void ensureSNSUserIsSaved() {
        SNSUser snsuser = new SNSUser(email,address,PhoneNumber,SNSNumber,name, CCNumber,birthdate,sex);
        boolean res;
        res = RegisterSNSUserStore.saveSNSUser(snsuser);
        Assertions.assertTrue(res);
    }

}
