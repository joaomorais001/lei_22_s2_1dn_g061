import app.domain.model.Employee;
import app.domain.store.EmployeeStore;
import app.ui.console.utils.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pt.isep.lei.esoft.auth.AuthFacade;


public class EmployeeStoreTest {

    private final AuthFacade authFacade;
    private final EmployeeStore employeeStore;

    private static final String name = "Claudio";
    private static final String email = "Claudio@isep.ipp.pt";
    private static final String address = "Rua";
    private static final int phoneNumber = 123456789;
    private static final int CCNumber = 12345678;
    private static final String id = "12345";
    private static final String name1 = "Andre";
    private static final String email1 = "Andre@isep.ipp.pt";
    private static final String address1 = "Avenida";
    private static final int phoneNumber1 = 987654321;
    private static final int CCNumber1 = 87654321;
    private static final String id1 = "54321";

    public EmployeeStoreTest(){
        authFacade = new AuthFacade();
        employeeStore = new EmployeeStore(authFacade);
    }

    @Test
    public void sameName(){
        Employee employee = new Employee(name,email,address,phoneNumber,CCNumber,id);
        Employee employee1 = new Employee(name,email1,address1,phoneNumber1,CCNumber1,id1);
        boolean res = employee.getName().equals(employee1.getName());
        Assertions.assertTrue(res);
    }

    @Test
    public void sameAddress(){
        Employee employee = new Employee(name,email,address,phoneNumber,CCNumber,id);
        Employee employee1 = new Employee(name1,email1,address,phoneNumber1,CCNumber1,id1);
        boolean res = employee.getAddress().equals(employee1.getAddress());
        Assertions.assertTrue(res);
    }

    @Test
    public void samePhoneNumber(){
        Employee employee = new Employee(name,email, address,phoneNumber,CCNumber, id);
        Employee employee1 = new Employee(name1,email1,address1,phoneNumber,CCNumber1,id1);
        boolean res = employee.getPhonenumber() == employee1.getPhonenumber();
        Assertions.assertTrue(res);
    }

    @Test
    public void sameCCNumber(){
        Employee employee = new Employee(name,email, address,phoneNumber,CCNumber, id);
        Employee employee1 = new Employee(name1,email1,address1,phoneNumber1,CCNumber,id1);
        boolean res = employee.getCCnumber() == employee1.getCCnumber();
        Assertions.assertTrue(res);
    }

    @Test
    public void ensurePasswordIsGenerated() {
        String password = Utils.generatePassword();
        boolean res;
        res = password.length() == 7;
        Assertions.assertTrue(res);
    }

    @Test
    public void ensureIdIsGenerated() {
        String id = Utils.generateID();
        boolean res;
        res = id.length() == 5;
        Assertions.assertTrue(res);
    }

    @Test
    public void ensureValidateEmployee() {
        Employee employee = new Employee(name, email, address, phoneNumber, CCNumber, id);
        boolean res;
        res = employeeStore.validateEmployee(employee);
        Assertions.assertTrue(res);
    }

    @Test
    public void ensureEmployeeIsSaved() {
        Employee employee = new Employee(name, email, address, phoneNumber, CCNumber, id);
        boolean res;
        res = employeeStore.saveEmployee(employee);
        Assertions.assertTrue(res);
    }

}
