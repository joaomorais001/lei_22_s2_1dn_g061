import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import app.controller.ListWaitingRoomController;
import app.domain.model.SNSUser;
import app.mapper.dto.SNSUserDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ListWaitingRoomTest {

    private ListWaitingRoomController controller;

    private ListWaitingRoomTest() {
        controller = new ListWaitingRoomController();
    }

    @Test
    public void testList() throws ParseException {
        int i = 0;

        List<SNSUserDTO> actual;
        List<SNSUserDTO> expected = new ArrayList<>();
        List<SNSUser> list2 = new ArrayList<>();

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String date = "10-10-2020";
        Date birthdate = df.parse(date);

        SNSUserDTO user1 = new SNSUserDTO(123456789, 123456789, "fabio", birthdate, "M");
        SNSUser user2 = new SNSUser("fabio@email.com", "rua", 123456789, 123456789, "fabio", 12345678, birthdate,
                "M");

        expected.add(user1);
        list2.add(user2);

        actual = controller.createWaitingRoomList(list2);

        Assertions.assertTrue(
                expected.size() == actual.size() && expected.get(i).toString().equals(actual.get(i).toString()) && expected.get(i).getClass() == actual.get(i).getClass());
    }
}
