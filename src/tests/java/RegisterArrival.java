
import app.controller.WaitingRoomController;
import app.domain.model.SNSUser;
import app.domain.model.ScheduleVaccine;
import app.domain.model.VacCenter;
import app.domain.store.ScheduleVaccineStore;
import app.domain.store.VacCenterStore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RegisterArrival {


    private ScheduleVaccineStore appointmentList;
    private VacCenterStore vacCenterStore;
    private static SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    public String datenow = df.format(new Date());
    public static final String time = "11:00";
    private static final String name = "João";
    public static final int SNSNumber = 123456789;
    private static final String email = "joao@isep.ipp.pt";
    private static final String address = "Rua sao joao";
    private static final int PhoneNumber = 123456789;
    private static final int CCNumber = 12345678;
    static Date birthday;

    static {
        try {
            birthday = df.parse("24-09-2003");
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private static final Date birthdate = birthday;
    private static final String sex = "masculino";
    private static final String vaccineType = "covid";
    private static final String vacName = "centro";
    private static final String vacAddress = "Rua de Guimaraes";
    private static final String vacPhoneNumber = "912626999";
    private static final String vacFaxNumber = "019283746";
    private static final String vacWebsite = "website@test.com";
    private static final int vacOpeningHour = 9;
    private static final int vacClosingHour = 19;
    private static final int vacSlotDuration = 12;
    private static final int vacMaxVaccines = 200;


    @Test
    public void RegisterArrival() {
        boolean res;
        vacCenterStore = new VacCenterStore();
        appointmentList = new ScheduleVaccineStore();
        WaitingRoomController wrc = new WaitingRoomController();
        VacCenter vacCenter = new VacCenter(vacName, vacAddress, vacPhoneNumber, vacFaxNumber, vacWebsite, vacOpeningHour, vacClosingHour, vacSlotDuration, vacMaxVaccines);
        SNSUser snsuser = new SNSUser(email, address, PhoneNumber, SNSNumber, name, CCNumber, birthdate, sex);

        ScheduleVaccine Appointment = new ScheduleVaccine(datenow,time,vaccineType,vacName,SNSNumber);
        if (vacCenter.getName() == Appointment.getVaccineCenter()) {
            if (vacCenter.getWaitroom().contains(snsuser)){}
            else vacCenter.addWaitRoom(snsuser);
        }

        res = vacCenter.getWaitroom().contains(snsuser);
        Assertions.assertTrue(res);
    }

}
