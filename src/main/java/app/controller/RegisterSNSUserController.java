package app.controller;

import app.domain.model.SNSUser;
import app.domain.store.RegisterSNSUserStore;
import app.domain.model.Company;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.util.List;

import static app.domain.shared.Constants.ROLE_SNSUSER;

public class RegisterSNSUserController {

    private final AuthFacade authFacade;
    private Company company;

    private final RegisterSNSUserStore snsuserstore;


    public RegisterSNSUserController() {
        this.company = App.getInstance().getCompany();
        authFacade = company.getAuthFacade();
        snsuserstore = company.getSNSUserStore();
    }

    /**
     * "If the user is successfully registered, then add the user to the database."
     *
     * The first thing that happens is that the user is registered. If the user is successfully registered, then the user
     * is added to the database
     *
     * @param snsuser The SNSUser object that contains the user's information.
     * @return A boolean value.
     */
    public boolean saveSNSUser(SNSUser snsuser) {
        boolean success = RegisterSNSUserStore.saveSNSUser(snsuser);

        if (success == true) {
            String password = Utils.generatePassword();
            System.out.println("\nSNS User's Important Information\n");
            System.out.println("Email: "+snsuser.getEmail());
            System.out.println("Password: "+password+"\n");
            authFacade.addUserWithRole(snsuser.getName(), snsuser.getEmail(), password,ROLE_SNSUSER);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get all the SNS users in the company.
     *
     * @return A list of SNSUsers
     */
    public List<SNSUser> getSNSUsers() {
        return this.company.getSNSUserStore().getSNSUsers();
    }

}