package app.controller;

import app.domain.model.*;
import app.domain.store.RegisterSNSUserStore;
import app.domain.store.RegisterVaccineStore;
import app.domain.store.VacCenterStore;
import app.domain.store.ScheduleVaccineStore;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WaitingRoomController {

    private Company company;
    private final RegisterSNSUserStore snsuserstore;
    private static RegisterVaccineStore vaccinestore;
    private static ScheduleVaccineStore scheduleVaccineStore;
    private static VacCenterStore vacCenterStore;

    public WaitingRoomController() {
        this.company = App.getInstance().getCompany();
        snsuserstore = company.getSNSUserStore();
        vaccinestore = company.getRegisterVaccineStore();
        scheduleVaccineStore = company.getScheduleStore();
        vacCenterStore=company.getVacStore();
    }

    /**
     * This function adds a user to the wait room of a vaccine center
     *
     * @param schedule the schedule that the user is trying to add to the wait room
     * @param snsuser The user who is trying to add himself to the wait room.
     * @return A boolean value.
     */
    public boolean addUsertoWaitRoom(ScheduleVaccine schedule, SNSUser snsuser) {
        // Iterating through the list of vaccine centers.
        for (int i = 0; i < vacCenterStore.getVacCenterlist().size(); i++) {
            // Comparing the name of the vaccine center with the name of the vaccine center of the schedule.
            if (vacCenterStore.getVacCenterlist().get(i).getName() == schedule.getVaccineCenter()) {
                if (vacCenterStore.getVacCenterlist().get(i).getWaitroom().contains(snsuser))
                    return false;
                else
                    vacCenterStore.getVacCenterlist().get(i).addWaitRoom(snsuser);
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");
                    String aux = formatter.format(LocalDateTime.now());
                    LocalDateTime entryTime = LocalDateTime.parse(aux, formatter);
                    vacCenterStore.getVacCenterlist().get(i).addEntryRecord(entryTime, snsuser.getSNSNumber());
                return true;
            }
        }
        return false;
    }

    /**
     * > This function returns the SNSUser object that has the same SNSNumber as the parameter snsnum
     *
     * @param snsnum The SNS number of the user you want to get.
     * @return The SNSUser object that matches the snsnum.
     */
    public SNSUser getSNSUserByNumber(int snsnum) {
        SNSUser usersns = null;
        for (int i = 0; i < snsuserstore.getSNSUsers().size(); i++) {
            if (snsnum == snsuserstore.getSNSUsers().get(i).getSNSNumber()) {
                usersns = snsuserstore.getSNSUsers().get(i);
            }
        }
        return usersns;
    }


    /**
     * This function returns a list of appointments for a specific user and a specific vaccine center
     *
     * @param snsUser The user who is logged in.
     * @param vacCenter Vaccine Center
     * @return A list of appointments for a specific user and a specific date.
     */
    public List<ScheduleVaccine> getScheduleforSNSUserToday(SNSUser snsUser,VacCenter vacCenter) {
        List<ScheduleVaccine> apoitments = scheduleVaccineStore.getAppointments();
        List<ScheduleVaccine> apoitmentuser= new ArrayList<>();;
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String datenow = df.format(new Date());
        for (int i = 0; i < apoitments.size(); i++) {
            if (snsUser.getSNSNumber() == apoitments.get(i).getSNSNumber() && apoitments.get(i).getData().equals(datenow) && apoitments.get(i).getVaccineCenter()==vacCenter.getName()) {
                apoitmentuser.add(apoitments.get(i));
            }
        }return apoitmentuser;
    }

}

