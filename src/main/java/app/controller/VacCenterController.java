package app.controller;


import app.domain.model.Company;
import app.domain.model.Employee;
import app.domain.model.SNSUser;
import app.domain.model.VacCenter;
import app.domain.store.EmployeeStore;
import app.domain.store.RegisterSNSUserStore;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class VacCenterController {

    private Company company;



    public VacCenterController() {
        this.company = App.getInstance().getCompany();
    }



    public VacCenter registerVC(String name, String address, String phoneNumber, String faxNumber, String website, int openingHour, int closingHour, int slotDuration, int maxVaccines) {
        return createVaccinationCenter(name, address, phoneNumber, faxNumber, website, openingHour, closingHour, slotDuration, maxVaccines);
    }

    public VacCenter createVaccinationCenter(String name, String address, String phoneNumber, String faxNumber, String website, int openingHour, int closingHour, int slotDuration, int maxVaccines) {
        VacCenter vacCenter = new VacCenter(name, address, phoneNumber, faxNumber, website, openingHour, closingHour, slotDuration, maxVaccines);
        company.getVacStore().addVacCenter(vacCenter);
        return vacCenter;
    }


    public List<VacCenter> getVacCenterlist() {
        return company.getVacStore().getVacCenterlist();
    }



}
