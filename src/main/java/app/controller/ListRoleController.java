package app.controller;

import app.domain.model.Employee;
import app.domain.store.EmployeeStore;
import app.domain.model.Company;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.mappers.dto.UserDTO;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.util.List;

public class ListRoleController {

    private final EmployeeStore employeeStore;
    private final AuthFacade authFacade;
    private Company company;
    public ListRoleController(){
        this.company = App.getInstance().getCompany();
        employeeStore = company.getEmployeeStore();
        authFacade = company.getAuthFacade();
    }
    /**
     * Get all users with a given role.
     *
     * @param role The role you want to get users with.
     * @return A list of UserDTO objects.
     */
    public List<UserDTO> getUsersWithRoles(String role){
        return this.authFacade.getUsersWithRole(role);
    }
    /**
     * > This function returns a list of all users in the database
     *
     * @return A list of UserDTO objects.
     */
    public List<UserDTO> getUsers() {
        return  this.authFacade.getUsers();
    }
    /**
     * Get all employees from the company's employee store.
     *
     * @return A list of employees
     */
    public List<Employee> getEmployee() {
        return this.company.getEmployeeStore().getEmployees();
    }
    /**
     * Get the list of roles for the current user.
     *
     * @return A list of UserRoleDTO objects.
     */
    /**
     * Get the list of roles for the current user.
     *
     * @return A list of UserRoleDTO objects.
     */
    public List<UserRoleDTO> getRolesList() {
        return company.getUserRoles();
    }
}