package app.controller;

import java.util.List;

import app.domain.model.Company;
import app.domain.model.SNSUser;
import app.domain.model.VacCenter;
import app.mapper.SNSUserMapper;
import app.mapper.dto.SNSUserDTO;

public class ListWaitingRoomController {

    private Company company;
    private SNSUserMapper mapper;

    public ListWaitingRoomController() {
        this.company = App.getInstance().getCompany();
        this.mapper = new SNSUserMapper();
    }

    public List<SNSUserDTO> listWaitingRoom;

    /**
     * This function returns a list of users who are waiting for a vaccination.
     *
     * @param center The VacCenter object that you want to get the waiting room for.
     * @return A list of SNSUser objects.
     */
    public List<SNSUser> getWaitingRoom(VacCenter center) {
        return center.getWaitroom();
    }

    /**
     * It takes a list of SNSUser objects and converts them to a list of SNSUserDTO objects
     *
     * @param list The list of SNSUser objects that you want to convert to SNSUserDTO objects.
     * @return A list of SNSUserDTO objects.
     */
    public List<SNSUserDTO> createWaitingRoomList(List<SNSUser> list) {
        listWaitingRoom = mapper.toListDTO(list);
        return listWaitingRoom;
    }

    /**
     * > This function returns the list of users in the waiting room
     *
     * @return A list of SNSUserDTO objects.
     */
    public List<SNSUserDTO> getWaitingRoomList() {
        return listWaitingRoom;
    }

}