package app.controller;

import app.domain.model.*;
import app.domain.shared.Constants;
import app.domain.store.EmployeeStore;
import app.domain.store.RegisterSNSUserStore;
import app.domain.store.VacCenterStore;
import app.ui.console.utils.Utils;
import app.ui.gui.SNSUserGUI;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.UserSession;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class App {

    private Company company;
    private AuthFacade authFacade;
    private EmployeeStore employeeStore;
    private VacCenterStore centerStore;

    private App() {
        Properties props = getProperties();
        this.company = new Company(props.getProperty(Constants.PARAMS_COMPANY_DESIGNATION));
        this.authFacade = this.company.getAuthFacade();
        this.employeeStore = this.company.getEmployeeStore();
        this.centerStore = this.company.getVacStore();
        bootstrap();
    }

    public Company getCompany() {
        return this.company;
    }

    public UserSession getCurrentUserSession() {
        return this.authFacade.getCurrentUserSession();
    }

    public boolean doLogin(String email, String pwd) {
        return this.authFacade.doLogin(email, pwd).isLoggedIn();
    }

    public void doLogout() {
        this.authFacade.doLogout();
    }

    private Properties getProperties() {
        Properties props = new Properties();

        // Add default properties and values
        props.setProperty(Constants.PARAMS_COMPANY_DESIGNATION, "DGS/SNS");

        // Read configured values
        try {
            InputStream in = new FileInputStream(Constants.PARAMS_FILENAME);
            props.load(in);
            in.close();
        } catch (IOException ex) {

        }
        return props;
    }

    private void bootstrap() {
        VacCenter centro1;
        VacCenter centro2;
        //centro1 =centerStore.getVacCenters().get(0);
        //centro2 =centerStore.getVacCenters().get(1);

        RecoveryRoom user = new RecoveryRoom(123456789);
        //centro1.addRecoveryRoom(user);
        //centro2.addRecoveryRoom(user);

        //Roles
        this.authFacade.addUserRole(Constants.ROLE_ADMIN, Constants.ROLE_ADMIN);
        this.authFacade.addUserRole(Constants.ROLE_RECEPTIONIST, Constants.ROLE_RECEPTIONIST);
        this.authFacade.addUserRole(Constants.ROLE_NURSE, Constants.ROLE_NURSE);
        this.authFacade.addUserRole(Constants.ROLE_CENTERCOORDINATOR, Constants.ROLE_CENTERCOORDINATOR);
        this.authFacade.addUserRole(Constants.ROLE_SNSUSER, Constants.ROLE_SNSUSER);

        //Users
        this.authFacade.addUserWithRole("Main Administrator", "admin@lei.sem2.pt", "123456", Constants.ROLE_ADMIN);
        this.authFacade.addUserWithRole("Receptionist", "recep@lei.sem2.pt", "123456", Constants.ROLE_RECEPTIONIST);
        this.authFacade.addUserWithRole("SNS User", "snsuser@lei.sem2.pt", "123456", Constants.ROLE_SNSUSER);
        this.authFacade.addUserWithRole("Nurse", "nurse@lei.sem2.pt", "123456", Constants.ROLE_NURSE);
        this.authFacade.addUserWithRole("Center Coordinator", "centercoordinator@lei.sem2.pt", "123456",
                Constants.ROLE_CENTERCOORDINATOR);
    }

    // Extracted from
    // https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
    private static App singleton = null;

    public static App getInstance() {
        if (singleton == null) {
            synchronized (App.class) {
                singleton = new App();
            }
        }
        return singleton;
    }
}
