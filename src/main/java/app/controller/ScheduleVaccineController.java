package app.controller;


import app.domain.model.*;
import app.domain.store.RegisterSNSUserStore;
import app.domain.store.ScheduleVaccineStore;
import app.domain.store.VacCenterStore;
import app.mapper.VacCenterMapper;

import java.util.ArrayList;
import java.util.List;
import static java.lang.Integer.valueOf;

public class ScheduleVaccineController {
    private Company company;

    private final ScheduleVaccineStore Store;

    private VacCenterMapper mapper;

    private final VacCenterStore vacCenters;

    private static ArrayList<Vaccine> vaccineList = null;
    private RegisterSNSUserStore snsuserstore;




    public ScheduleVaccineController(){
        this.company = App.getInstance().getCompany();
        Store = company.getScheduleStore();
        vacCenters = company.getVacStore();
        this.mapper = new VacCenterMapper();
    }


    /**
     * It saves the appointment data to the database.
     *
     * @param appointment This is the appointment object that you want to save.
     */
    public void saveAppointmentData(ScheduleVaccine appointment){
        Store.addSNSUserAppointment(appointment);

    }

    /**
     * Get all the appointments from the company's schedule store.
     *
     * @return A list of all the appointments in the schedule store.
     */
    public List<ScheduleVaccine> getAppointments() {
        return this.company.getScheduleStore().getAppointments();
    }



    /**
     * The function returns a list of strings that contains the names of the vaccines
     *
     * @return A list of strings
     */
    public static List<String> VaccineTypes(){
        ArrayList<String> Vaccinetypes = new ArrayList<>();

        Vaccinetypes.add("Covid-19");
        Vaccinetypes.add("Tetanus");
        Vaccinetypes.add("HIV");
        Vaccinetypes.add("Hepatitis");

        return Vaccinetypes;
    }

    /**
     * This function checks if the user is registered in the system
     *
     * @param SNSNumber The SNS number of the user.
     */
    public static void validateSNSUser(int SNSNumber){
        boolean val= false;
        for (int i = 0; i < RegisterSNSUserStore.getSNSUsers().size(); i++) {

            if(SNSNumber == RegisterSNSUserStore.getSNSUsers().get(i).getSNSNumber())
                val = true;

        }
        if (val == false)
            throw new IllegalArgumentException("User is not registered in the system");
    }

    /**
     * It validates the time of the appointment
     *
     * @param time the time that the user wants to book
     * @param centro The name of the center
     */
    public void validateTime(String time, String centro){
        VacCenter center = null;
        
        for (int i = 0; i < company.getVacStore().getVacCenters().size(); i++) {

            if(centro == company.getVacStore().getVacCenters().get(i).getName())
                center = company.getVacStore().getVacCenters().get(i);

        }
        
        String[] timeSplit = time.split(":");
        int[] times = new int[2];
        times[0] = Integer.parseInt(String.valueOf(valueOf(timeSplit[0])));
        times[1] = Integer.parseInt(String.valueOf(valueOf(timeSplit[1])));
            
        if (!(times[0] >= center.getOpeningHour() && times[0] < center.getClosingHour() && times[1] >= 0  && times[1] <60))
            throw new IllegalArgumentException("Invalid Hours.");

        if (time == null){
            throw new IllegalArgumentException("Please insert a time");}
    }

    /**
     * This function checks if the vaccine type is valid or not
     *
     * @param VaccineType The type of vaccine that is being administered.
     */
    public void validateVaccinetype(String VaccineType){

        if(!ScheduleVaccineController.VaccineTypes().contains(VaccineType))
            throw new IllegalArgumentException("Vaccine Type Invalid!!");
    }

    public boolean validateMaxVaccines(String date, String centro){
        int count = 0;
        int maxvaccines;

        for (int i = 0; i < company.getScheduleStore().getAppointments().size(); i++) {
            if(centro == company.getScheduleStore().getAppointments().get(i).getVaccineCenter() && date == company.getScheduleStore().getAppointments().get(i).getData())
                count ++;
        }

        for (int j = 0; j < company.getVacStore().getVacCenters().size(); j++) {
            if(centro == company.getVacStore().getVacCenters().get(j).getName()) {
                maxvaccines = company.getVacStore().getVacCenters().get(j).getMaxVaccines();
                if (maxvaccines <= count) {
                    throw new IllegalArgumentException("Max vaccines achieved");
                }
            }
        }
        return true;
    }

    public SNSUser getSNSUserByNumber(int snsnum) {
        SNSUser usersns = null;
        for (int i = 0; i < snsuserstore.getSNSUsers().size(); i++) {
            if (snsnum == snsuserstore.getSNSUsers().get(i).getSNSNumber()) {
                usersns = snsuserstore.getSNSUsers().get(i);
            }
        }
        return usersns;
    }

}
