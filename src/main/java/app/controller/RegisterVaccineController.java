package app.controller;


import app.domain.model.Vaccine;
import app.domain.store.RegisterVaccineStore;
import app.domain.model.Company;
import java.util.List;

public class RegisterVaccineController {

    private Company company;
    private final RegisterVaccineStore vaccineStore;


    public RegisterVaccineController() {
        this.company = App.getInstance().getCompany();
        vaccineStore = company.getRegisterVaccineStore();
    }



    public List<Vaccine> getVaccines() {
        return this.company.getRegisterVaccineStore().getVaccines();
    }

    public boolean saveVaccine(Vaccine vaccine) {
        boolean success =vaccineStore.saveVaccine(vaccine);
        if (success == true) {
            return true;
        } else {return false;}
        }

    }

