package app.controller;

import app.domain.model.Company;
import app.domain.model.RecoveryRoom;
import app.domain.model.VacCenter;
import app.domain.model.Vaccine;
import app.domain.store.RegisterVaccineStore;
import app.domain.store.ScheduleVaccineStore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VaccinceSelectionController {
    private final Company company;
    private String VaccineType;
    private ScheduleVaccineStore store;
    private RegisterVaccineStore vaccineStore;
    private List<Vaccine> showVaccines = new ArrayList<>();
    private int SNSNumber;
    public VaccinceSelectionController() {
        this.company = App.getInstance().getCompany();
        store = company.getScheduleStore();
        vaccineStore= company.getRegisterVaccineStore();

    }


    public int VaccineSelectionSNSN(VacCenter center)
    {
        SNSNumber = center.getWaitroom().get(0).getSNSNumber();
        center.getWaitroom().remove(0);
        RecoveryRoom novo = new RecoveryRoom(SNSNumber);
        center.getRecoveryRoom().add(novo);
        return SNSNumber;
    }


    public String VaccineSelectionVacType() {

            for (int i = 0; i < company.getScheduleStore().getAppointments().size(); i++) {
                if (SNSNumber == company.getScheduleStore().getAppointments().get(i).getSNSNumber())
                    VaccineType = store.getAppointments().get(i).getVaccine();
            }
            return VaccineType;
        }


    public List<Vaccine> ShowVaccineOptions(String VaccineType){
        for (int i = 0; i <vaccineStore.getVaccines().size() ; i++) {
            if(VaccineType.equals(vaccineStore.getVaccines().get(i).getVaccineType())){
              showVaccines.add(vaccineStore.getVaccines().get(i));
            }
        }
        return showVaccines;
    }
}
