package app.controller;

import app.domain.model.*;
import app.domain.store.VacCenterStore;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.Timer;
import java.util.concurrent.TimeUnit;


public class VaccineCounterController {
    private LocalDate today;
    private Company company;

    private VacCenter center;

    private String vacname;

    private static VacCenterStore vacCenterStore;

    private int Counter=0;



    /**
     * This function generates a report of the number of patients who have been vaccinated in each vaccination center
     */
    public void GenerateReport(){

        this.company = App.getInstance().getCompany();
        this.vacCenterStore = company.getVacStore();

        try(FileReader reader =  new FileReader("config.properties")) {

            // This is reading the config.properties file and getting the time from it.
            Properties properties = new Properties();
            properties.load(reader);
            String runTime = properties.getProperty("Time");
            LocalTime time = LocalTime.parse(runTime);
            Timer timer = new Timer();
            LocalDateTime currentTime = LocalDateTime.now();


            TimerTask task = new TimerTask() {
                @Override
                public void run() {

                    LocalDate todaysdate = LocalDate.now();


                    // This is creating a file called Report and a file called 1 in the Report folder.
                    File file = new File("Report");
                    if (!file.exists()) {
                        try {
                            Files.createDirectory(Path.of(String.valueOf(file)));
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    file = new File("Report/1");
                    if (!file.exists()) {
                        try {
                            Files.createFile(Path.of(String.valueOf(file)));
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }

                    PrintWriter writer;
                    try {
                        writer = new PrintWriter(file);
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException(e);
                    }

                    // This is a nested for loop that is iterating through the list of vaccination centers and the recovery
                    // rooms in each vaccination center.
                    for (int i = 0; i < vacCenterStore.getVacCenterlist().size(); i++) {
                        center = vacCenterStore.getVacCenters().get(i);
                        vacname = vacCenterStore.getVacCenterlist().get(i).getName();
                        for (int j = 0; j < center.getRecoveryRoom().size(); j++) {
                            if (center.getRecoveryRoom().get(j).getDay().equals(todaysdate)) {
                                Counter++;
                                CounterReport report = new CounterReport(vacname, Counter);
                                writer.println(report);
                                Counter =0;
                            }
                        }
                    }
                    writer.close();
                }

            };

            Calendar date = Calendar.getInstance();
            date.set(Calendar.HOUR_OF_DAY, time.getHour());
            date.set(Calendar.MINUTE, time.getMinute());
            date.set(Calendar.SECOND, time.getSecond());
            timer.schedule(task, date.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));
        }
        catch (FileNotFoundException e ) {
            System.out.println("Could not save data, file not found.");
        }
        catch (Exception e ) {
            e.printStackTrace();
        }
    }
}