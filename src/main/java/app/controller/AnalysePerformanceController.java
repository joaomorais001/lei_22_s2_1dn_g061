package app.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import app.domain.model.Algorithms;
import app.domain.model.Company;
import app.domain.model.DepartureRecords;
import app.domain.model.Employee;
import app.domain.model.EntryRecords;
import app.domain.model.VacCenter;
import app.domain.store.EmployeeStore;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.UserSession;

public class AnalysePerformanceController {

    // Declaring a final variable called company.
    private final Company company;
    // Creating a private final variable called authFacade.
    private final AuthFacade authFacade;
    // Creating a final variable called store.
    private final EmployeeStore store;

    // Creating a new instance of the class AnalysePerformanceController.
    public AnalysePerformanceController() {
        this.company = App.getInstance().getCompany();
        this.authFacade = company.getAuthFacade();
        this.store = company.getEmployeeStore();
    }

    /**
     * Get all the entry records for a given vaccination center.
     *
     * @param center The VacCenter object that is being displayed.
     * @return A list of EntryRecords
     */
    public List<EntryRecords> getEntryRecords(VacCenter center) {
        return center.getEntryRecords();
    }

    /**
     * Get all the departure records for a given vaccination center.
     *
     * @param center The VacCenter object that you want to get the departure records from.
     * @return A list of departure records.
     */
    public List<DepartureRecords> getDepartureRecords(VacCenter center) {
        return center.getDepartureRecords();
    }

    /**
     * It takes a VacCenter object and adds the entry and departure records to the respective lists
     *
     * @param center VacCenter object
     */
    public void getImportedRecords(VacCenter center) {

        ArrayList<String> data = center.getLegacydata();
        String[] dataInfo;
        String[] infoArrival;
        String[] infoDeparture;

        for (int i = 0; i < data.size(); i++) {
            boolean existsEntry = false, existsDeparture = false;

            dataInfo = data.get(i).split(",|]");

            String strArrival = dataInfo[7].trim();
            String strDeparture = dataInfo[9].trim();

            infoArrival = strArrival.split(" ");
            infoDeparture = strDeparture.split(" ");

            String[] arrivalDate = infoArrival[0].trim().split("/"), departureDate = infoDeparture[0].trim().split("/");
            String dayArrival = arrivalDate[1], dayDeparture = departureDate[1];
            String monthArrival = arrivalDate[0], monthDeparture = departureDate[0];
            String hourArrival = infoArrival[1].trim(), hourDeparture = infoDeparture[1].trim();
            String finalArrivalDate, finalDepartureDate;

            if (dayArrival.length() == 1) {
                dayArrival = "0".concat(dayArrival);
            }
            if (monthArrival.length() == 1) {
                monthArrival = "0".concat(monthArrival);
            }
            if (hourArrival.length() == 4) {
                hourArrival = "0".concat(hourArrival);
            }

            if (dayDeparture.length() == 1) {
                dayDeparture = "0".concat(dayDeparture);
            }
            if (monthDeparture.length() == 1) {
                monthDeparture = "0".concat(monthDeparture);
            }
            if (hourDeparture.length() == 4) {
                hourDeparture = "0".concat(hourDeparture);
            }

            finalArrivalDate = monthArrival + "/" + dayArrival + "/" + arrivalDate[2];
            finalDepartureDate = monthDeparture + "/" + dayDeparture + "/" + departureDate[2];

            strArrival = finalArrivalDate + " " + hourArrival;
            strDeparture = finalDepartureDate + " " + hourDeparture;

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");

            LocalDateTime entryTime = LocalDateTime.parse(strArrival, formatter);

            LocalDateTime departureTime = LocalDateTime.parse(strDeparture, formatter);

            int SNSnumber = Integer.parseInt(dataInfo[0].substring(1));

            for (EntryRecords records : center.getEntryRecords()) {
                if (records.getSnsUser() == SNSnumber && records.getEntryRecord().equals(entryTime)) {
                    existsEntry = true;
                    break;
                }
            }

            for (DepartureRecords records : center.getDepartureRecords()) {
                if (records.getSnsUser() == SNSnumber && records.getDepartureRecord().equals(departureTime)) {
                    existsDeparture = true;
                    break;
                }
            }

            if (!existsEntry) {
                center.addEntryRecord(entryTime, SNSnumber);
            }

            if (!existsDeparture) {
                center.addDepartureRecord(departureTime, SNSnumber);
            }
        }
    }

    /**
     * This function takes a date and a list of EntryRecords and returns a list of EntryRecords that have the same date as
     * the date passed in
     *
     * @param date The date you want to filter by.
     * @param list The list of EntryRecords that you want to filter.
     * @return A list of EntryRecords objects.
     */
    public List<EntryRecords> getEntryRecordsFromDate(LocalDate date, List<EntryRecords> list) {
        List<EntryRecords> updatedList = new ArrayList<>();

        for (EntryRecords record : list) {
            if (record.getEntryRecord().toLocalDate().equals(date)) {
                updatedList.add(record);
            }
        }

        return updatedList;
    }

    /**
     * > This function takes a date and a list of departure records as parameters and returns a list of departure records
     * that match the date
     *
     * @param date The date you want to filter by
     * @param list The list of DepartureRecords that you want to filter.
     * @return A list of DepartureRecords that have a departure date that matches the date passed in.
     */
    public List<DepartureRecords> getDepartureRecordsFromDate(LocalDate date, List<DepartureRecords> list) {
        List<DepartureRecords> updatedList = new ArrayList<>();

        for (DepartureRecords record : list) {
            if (record.getDepartureRecord().toLocalDate().equals(date)) {
                updatedList.add(record);
            }
        }

        return updatedList;
    }

    /**
     * It gets the current user's email address
     *
     * @return The user's email address.
     */
    public String getUserEmail() {
        UserSession info = authFacade.getCurrentUserSession();

        return info.getUserId().toString();
    }

    /**
     * Get the vaccination center of the employee with the given email.
     *
     * @param email the email of the employee
     * @return The vaccination center that the employee works at.
     */
    public VacCenter getVaccinationCenter(String email) {

        VacCenter center = null;

        List<Employee> employeeList = store.getEmployees();

        for (Employee employee : employeeList) {
            if (email.equals(employee.getEmail())) {
                center = employee.getVacCenter();
            }
        }
        return center;
    }

    /**
     * This function reads the email from the LoginInfo.txt file and returns it
     *
     * @return The email address of the user.
     */
    public String readLoginInfoFile() throws FileNotFoundException {

        File file = new File("LoginInfo.txt");
        Scanner reader = new Scanner(file);
        String email = reader.nextLine();
        reader.close();

        return email;
    }

    /**
     * It takes an integer as an argument and returns an array of Time objects, each of which is the sum of the previous
     * Time object and the integer argument
     *
     * @param timeInterval the interval of time in minutes that you want to use.
     * @return An array of Time objects.
     */
    private static Time[] timeInt(int timeInterval) {
        Time[] interval = new Time[720 / timeInterval];
        long millisSum = 0;

        for (int i = 0; i < interval.length; i++) {
            Time time = new Time(8, 0, 0);
            millisSum = millisSum + (timeInterval * 60000);
            interval[i] = time;
            time.setTime(time.getTime() + millisSum);
        }
        return interval;
    }

    /**
     * This function takes a list of EntryRecords and a time interval in minutes and returns an array of integers where
     * each element represents the number of entries in the list that occurred in the time interval
     *
     * @param entryRecords a list of EntryRecords objects
     * @param timeInterval the time interval in minutes that you want to use to divide the day.
     * @return An array of integers.
     */
    private int[] entryRecordInterval(List<EntryRecords> entryRecords, int timeInterval) {

        int[] recordsTimeInterval = new int[720 / timeInterval];
        Time[] interval = new Time[720 / timeInterval];
        long millisSum = 0;

        for (int i = 0; i < interval.length; i++) {
            Time time = new Time(8, 0, 0);
            millisSum = millisSum + (timeInterval * 60000);
            interval[i] = time;
            time.setTime(time.getTime() + millisSum);
        }

        for (EntryRecords record : entryRecords) {
            Time actualTime = new Time(record.getEntryRecord().getHour(), record.getEntryRecord().getMinute(), record.getEntryRecord().getSecond());
            for (int i = 0; i < interval.length; i++) {
                if ((actualTime.getTime()) < interval[i].getTime()) {
                    recordsTimeInterval[i]++;
                    break;
                }
            }
        }

        return recordsTimeInterval;
    }

    /**
     * This function takes a list of departure records and a time interval and returns an array of integers where each
     * element represents the number of departure records that occurred within the time interval
     *
     * @param departureRecords a list of DepartureRecords objects
     * @param timeInterval the time interval in minutes that you want to group the records by.
     * @return An array of integers.
     */
    private int[] departureRecordInterval(List<DepartureRecords> departureRecords, int timeInterval) {
        int[] recordsByTimeInterval = new int[720 / timeInterval];
        Time[] hoursInterval = new Time[720 / timeInterval];
        long millis = 0;

        for (int i = 0; i < hoursInterval.length; i++) {
            Time time = new Time(8, 0, 0);
            millis = millis + (timeInterval * 60000);
            hoursInterval[i] = time;
            time.setTime(time.getTime() + millis);
        }

        for (DepartureRecords record : departureRecords) {
            Time actualTime = new Time(record.getDepartureRecord().getHour(), record.getDepartureRecord().getMinute(), record.getDepartureRecord().getSecond());
            for (int i = 0; i < hoursInterval.length; i++) {
                if ((actualTime.getTime()) < hoursInterval[i].getTime()) {
                    recordsByTimeInterval[i]++;
                    break;
                }
            }
        }

        return recordsByTimeInterval;
    }

    /**
     * It takes in a list of entry records, a list of departure records, and a time interval, and returns an array of the
     * differences between the number of entries and departures for each time interval
     *
     * @param entryRecords a list of EntryRecords objects
     * @param departureRecords a list of DepartureRecords objects
     * @param timeInterval the time interval in minutes
     * @return The difference between the number of entries and the number of departures for each time interval.
     */
    public int[] calculateDifferencesEntryDeparture(
            List<EntryRecords> entryRecords, List<DepartureRecords> departureRecords, int timeInterval) {

        int[] entryRecord = entryRecordInterval(entryRecords, timeInterval);
        int[] departureRecord = departureRecordInterval(departureRecords, timeInterval);

        int[] difference = new int[departureRecord.length];

        for (int i = 0; i < difference.length; i++) {
            difference[i] = entryRecord[i] - departureRecord[i];
        }

        return difference;
    }

    /**
     * It reads the algorithm name from the config.properties file and returns it
     *
     * @return The algorithm being used.
     */
    public String getAlgorithm() {
        String alg = "";
        try {
            FileReader file = new FileReader("config.properties");
            Properties prop = new Properties();
            prop.load(file);
            alg = prop.getProperty("Algorithm");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return alg;
    }

    /**
     * The function takes in a string and an array of integers. If the string is equal to "1", then the function returns
     * the result of the BruteForceAlgorithm function. If the string is not equal to "1", then the function returns the
     * result of the BenchmarkAlgorithm function
     *
     * @param alg The algorithm to use.
     * @param seq The sequence of numbers to be used in the algorithm.
     * @return The maximum sum of the subarray and the start and end indices of the subarray.
     */
    public int[] calculateMaxSumSub(String alg, int[] seq) {
        if (alg.equals("1")) {
            return Algorithms.BruteForceAlgorithm(seq);
        } else {
            return Algorithms.BenchmarkAlgorithm(seq);
        }
    }

    /**
     * This function calculates the sum of all the elements in the given array.
     *
     * @param seq an array of integers
     * @return The sum of all the values in the array.
     */
    public int calculateMaxSum(int[] seq) {
        int sum = 0;
        for(Integer val: seq) {
            sum += val;
        }
        return sum;
    }
    
    /**
     * > The function finds the sublist of the input list that has the maximum sum
     *
     * @param seq the sequence of integers
     * @return The start and end indices of the sublist with the largest sum.
     */
    private static int[] startEndSublist(int[] seq) {
        int maxSoFar = 0;
        int maxEndingHere = 0;
        int startMaxSoFar = 0;
        int endMaxSoFar = 0;
        int startMaxEndingHere = 0;
        for (int i = 0; i < seq.length; ++i) {
            final int elem = seq[i];
            final int endMaxEndingHere = i + 1;
            if (maxEndingHere + elem < 0) {
                maxEndingHere = 0;
                startMaxEndingHere = i + 1;
            }
            else {
                maxEndingHere += elem;
            }
            if (maxSoFar < maxEndingHere) {
                maxSoFar = maxEndingHere;
                startMaxSoFar = startMaxEndingHere;
                endMaxSoFar = endMaxEndingHere;
            }
        }

        return new int[]{startMaxSoFar, endMaxSoFar-1};
    }

    /**
     * It takes in an array of differences between the current time and the time of the last tweet, and a time interval,
     * and returns the start and end time of the period of time with the most tweets
     *
     * @param differences an array of integers that represent the differences between the times of the events.
     * @param timeInterval the time interval in minutes between each time in the array of times
     * @return The start and end times of the period of time when the most people were in the building.
     */
    public String[] getPeriod(int[] differences, int timeInterval) {

        Time[] hoursInterval = timeInt(timeInterval);
        int[] startAndEnd = startEndSublist(differences);

        hoursInterval[startAndEnd[0]].setTime(hoursInterval[startAndEnd[0]].getTime() - timeInterval*60000);
        return new String[]{hoursInterval[startAndEnd[0]].toString(), hoursInterval[startAndEnd[1]].toString()};
    } 
}
