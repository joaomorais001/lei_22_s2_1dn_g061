package app.controller;
import app.domain.model.Company;
import app.domain.model.UserVaccineRecord;
import app.domain.model.Vaccine;
import app.domain.store.UserVaccineRecordStore;
import pt.isep.lei.esoft.auth.domain.model.User;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class UserVaccineRecordController {
    private Company company;
    private UserVaccineRecordStore store;


    public UserVaccineRecordController(){
        this.company = App.getInstance().getCompany();
        store = company.getUserVacStore();
    }
    public UserVaccineRecord createUserVacRecord (int SNSNumber, String Code, String Brand, String DoseNumber, String Dosage, String VaccineType, LocalDate Date){
        UserVaccineRecord userVacRecord = new UserVaccineRecord(SNSNumber,Code,Brand,DoseNumber,Dosage,VaccineType,Date);
        company.getUserVacStore().saveVaccineAdministration(userVacRecord);
                return userVacRecord;
    }
    public boolean addReaction(String reaction,int SNSNumber){
        for (int i = 0; i < getUserVaccineRecord().size(); i++) {
            if(getUserVaccineRecord().get(i).getSNSNumber()==SNSNumber){ getUserVaccineRecord().get(i).setReaction(reaction);
            return true;}
        }
        return false;
    }
    public List<UserVaccineRecord> getUserVaccineRecord(){return company.getUserVacStore().getUserVaccineRecord();}


}
