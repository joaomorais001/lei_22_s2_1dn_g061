package app.controller;

import app.domain.model.*;
import app.domain.store.EmployeeStore;
import app.domain.store.RegisterSNSUserStore;
import app.domain.store.VacCenterStore;
import app.mapper.SNSUserMapper;
import app.mapper.dto.SNSUserDTO;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class SortLegacyDataController {
    boolean val;
    private String fileName;
    private File file;
    private BufferedReader csvReader;
    public String[] header;
    private String firstline;
    private String line;
    private BubbleSort bubbleSort;
    public int totallines;
    private boolean isnull;
    private final RegisterSNSUserStore snsuserstore;
    private Company company;
    private SNSUserMapper mapper;
    public InsertionSort InsertionSort;
    private RegisterVaccineController vacinecontroller;
    private VacCenterStore vacCenterStore;
    private AuthFacade authFacade;
    private EmployeeStore employeeStore;
    private int lines;
    public int SNSUSerNumber = 0, VaccineName = 1, Dose = 2, LotNumber = 3, ScheduledDateTime = 4, ArrivalDateTime = 5, NurseAdministrationDateTime = 6, LeavingDateTime = 7;

    public SortLegacyDataController() {
        this.company = App.getInstance().getCompany();
        bubbleSort = new BubbleSort();
        InsertionSort = new InsertionSort();
        snsuserstore = company.getSNSUserStore();
        this.mapper = new SNSUserMapper();
        vacinecontroller = new RegisterVaccineController();
        vacCenterStore = new VacCenterStore();
        authFacade = new AuthFacade();
        this.employeeStore = company.getEmployeeStore();
    }

    /**
     * It checks if the file exists and if it's a csv file, if it's not, it asks the user to insert a new file name
     *
     * @param fileName The name of the file to be imported.
     * @return The file that was imported.
     */
    public File ImportFile(String fileName) {
        if (fileName == "onlyfortestes.csv") {
            file = new File(fileName);
        } else {
            do {
                fileName = Utils.readLineFromConsole("Please insert the CSV file name (filename.csv): ");
                file = new File(fileName);
                val = true;

                if (!Files.exists(Path.of(fileName)) || !fileName.endsWith(".csv")) {
                    System.out.println("File does not exists or its not a csv file");
                    val = false;
                }

            } while (!val);
        }
        return file;
    }

    /**
     * It reads a CSV file and returns a 2D array with the data
     *
     * @param file the file you want to read
     * @return A list of lists with the data from the csv file.
     */
    public String[][] ReadFileAndCreateList(File file) throws IOException {
        csvReader = new BufferedReader(new FileReader(file));
        firstline = csvReader.readLine();
        header = firstline.split(";");
        String[] dados;
        int count = 0;
        Boolean hasHeader = firstline.contains("SNSUSerNumber");
        if (hasHeader == true)
            lines = -1;
        else
            lines = 0;
        int totallines = HowManyLines(file);
        String[][] listdata = new String[totallines][8];
        if (hasHeader == true)
            for (int i = 0; i < header.length; i++) {
                switch (header[i].toUpperCase()) {
                    case "SNSUSERNUMBER":
                        SNSUSerNumber = i;
                        break;
                    case "VACCINENAME":
                        VaccineName = i;
                        break;
                    case "DOSE":
                        Dose = i;
                        break;
                    case "LOTNUMBER":
                        LotNumber = i;
                        break;
                    case "SCHEDULEDATETIME":
                        ScheduledDateTime = i;
                        break;
                    case "ARRIVALDATETIME":
                        ArrivalDateTime = i;
                        break;
                    case "NURSEADMINISTRATIONDATETIME":
                        NurseAdministrationDateTime = i;
                        break;
                    case "LEAVINGDATETIME":
                        LeavingDateTime = i;
                        break;
                }
            }
        while ((line = csvReader.readLine()) != null) {
            dados = line.split(";");
            listdata[count][0] = dados[SNSUSerNumber];
            listdata[count][1] = dados[VaccineName];
            listdata[count][2] = dados[Dose];
            listdata[count][3] = dados[LotNumber];
            listdata[count][4] = dados[ScheduledDateTime];
            listdata[count][5] = dados[ArrivalDateTime];
            listdata[count][6] = dados[NurseAdministrationDateTime];
            listdata[count][7] = dados[LeavingDateTime];
            count++;
        }
        return listdata;}

    /**
     * It reads the file line by line, and counts the number of lines
     *
     * @param file The file you want to read.
     * @return The number of lines in the file.
     */
    public int HowManyLines(File file) throws IOException {
        int lines = 0;
        BufferedReader readfile = new BufferedReader(new FileReader(file));
        firstline = readfile.readLine();
        while ((line = readfile.readLine()) != null)
            lines++;
        return lines;
    }

    /**
     * It takes a list of vaccines and returns a list of vaccines with extra information
     *
     * @param list the list of vaccines that the user has received
     * @return A list of all the vaccines that have been administered to a specific user.
     */
    public String[][] getListWithExtraInfo(String[][] list) {
        String[][] finalversion = new String[list.length][10];
        String name;
        SNSUserDTO user;
        String descripton = "Not registered";
        int snsnumber;
        for (int i = 0; i < list.length; i++) {
            finalversion[i][0] = list[i][SNSUSerNumber];
            snsnumber = Integer.parseInt(list[i][SNSUSerNumber]);
            if (getSNSUserByNumber(snsnumber) == null)
                name = "Not registered";
            else
                name = mapper.toDTO(getSNSUserByNumber(snsnumber)).getName();
            finalversion[i][1] = name;
            finalversion[i][2] = list[i][VaccineName];
            if (vacinecontroller.getVaccines() != null) {
                for (int j = 0; j < vacinecontroller.getVaccines().size(); j++) {
                    if (vacinecontroller.getVaccines().get(j).getBrand() == (list[i][VaccineName])) {
                        finalversion[j][3] = vacinecontroller.getVaccines().get(i).getDescription();
                    }
                }
            }
            finalversion[i][3] = descripton;
            finalversion[i][4] = list[i][Dose];
            finalversion[i][5] = list[i][LotNumber];
            finalversion[i][6] = list[i][ScheduledDateTime];
            finalversion[i][7] = list[i][ArrivalDateTime];
            finalversion[i][8] = list[i][NurseAdministrationDateTime];
            finalversion[i][9] = list[i][LeavingDateTime];

        }

        return finalversion;
    }

    /**
     * This function takes a 2D array of Strings and prints each row of the array on a separate line.
     *
     * @param list The list to be shown.
     */
    public void ShowList(String[][] list) {
        for (int i = 0; i < list.length; i++) {
            System.out.println(Arrays.toString(list[i]));
        }
    }

    /**
     * This function takes in a 2D array of strings and an integer, and returns a 2D array of strings
     *
     * @param listdata the list of data to be sorted
     * @param timechosen 0 for ascending, 1 for descending
     * @return The method is returning a 2D array of strings.
     */
    public String[][] bubbleSort(String[][] listdata, int timechosen) throws ParseException {
        return (bubbleSort.sortByTime(listdata, timechosen));
    }

    /**
     * This function takes in a 2D array of strings and an integer, and returns a 2D array of strings
     *
     * @param listdata The list of data to be sorted.
     * @param timechosen 0 for ascending, 1 for descending
     * @return The method returns a 2D array of strings.
     */
    public String[][] insertionsort(String[][] listdata, int timechosen, int lines) throws ParseException {
        return (InsertionSort.sortByTime(listdata, timechosen,lines));
    }

    /**
     * This function returns the SNSUser object that has the same SNSNumber as the parameter snsnum
     *
     * @param snsnum The SNS number of the user you want to get.
     * @return The SNSUser object that matches the snsnum.
     */
    public SNSUser getSNSUserByNumber(int snsnum) {
        SNSUser usersns = null;
        for (int i = 0; i < snsuserstore.getSNSUsers().size(); i++) {
            if (snsnum == snsuserstore.getSNSUsers().get(i).getSNSNumber()) {
                usersns = snsuserstore.getSNSUsers().get(i);
            }
        }
        return usersns;
    }

    /**
     * This function adds legacy data to the VacCenter object
     *
     * @param centeratm The VacCenter object that you want to add the data to.
     * @param list A 2D array of strings. The first column is the name of the data, the second column is the value.
     * @return A boolean value.
     */
    public boolean addLegacyData(VacCenter centeratm, String[][] list) {
       centeratm.addLegacyData(list);
        return false;

    }

    /**
     * "Get the vaccination center of the employee with the given email."
     *
     * The function is not very long, but it's not very clear either. It's not clear what the function does, and it's not
     * clear what the function returns
     *
     * @param email The email of the employee
     * @return The vaccination center that the employee works at.
     */
    public VacCenter getVaccinationCenter(String email) {

        VacCenter center = null;

        List<Employee> employeeList = employeeStore.getEmployees();

        for (Employee employee : employeeList) {
            if (email.equals(employee.getEmail())) {
                center = employee.getVacCenter();
            }
        }
        return center;
    }
    /**
     * This function reads the email from the LoginInfo.txt file and returns it
     *
     * @return The email address of the user.
     */
    public String readLoginInfoFile() throws FileNotFoundException {

        File file = new File("LoginInfo.txt");
        Scanner reader = new Scanner(file);
        String email = reader.nextLine();
        reader.close();

        return email;
    }

}
