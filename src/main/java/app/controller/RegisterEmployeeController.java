package app.controller;

import app.domain.model.Employee;
import app.domain.store.EmployeeStore;
import app.domain.model.Company;
import app.ui.console.utils.Utils;
import jdk.jshell.execution.Util;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class RegisterEmployeeController {

    private final EmployeeStore employeeStore;
    private final AuthFacade authFacade;
    private Company company;

    public RegisterEmployeeController() {
        this.company = App.getInstance().getCompany();
        employeeStore = company.getEmployeeStore();
        authFacade = company.getAuthFacade();
    }

    /**
     * Get the list of roles for the current user.
     *
     * @return A list of UserRoleDTO objects.
     */
    public List<UserRoleDTO> getRolesList() {
        return company.getUserRoles();
    }

    /**
     * @param employee Employee to register
     * @return if the employee was registered or not
     */
    public boolean saveEmployee(Employee employee, String role) {
        boolean success = employeeStore.saveEmployee(employee);
        if (success == true) {
            String password = Utils.generatePassword();
            writeEmployeeInfo(employee.getEmail(), password);
            System.out.println(password);
            authFacade.addUserWithRole(employee.getName(), employee.getEmail(), password, role);
            return true;
        } else {
            return false;
        }
    }

    /**
     * This function returns a list of employees from the employee store.
     *
     * @return A list of employees
     */
    public List<Employee> getEmployees() {
        return employeeStore.getEmployees();
    }

    private void writeEmployeeInfo(String email, String password){
        ArrayList list = new ArrayList<>();
        list.add(email);
        list.add(password);
        Utils.writeSerialization("userData",list);

    }
}
