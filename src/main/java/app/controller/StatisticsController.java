package app.controller;

import app.domain.model.*;
import app.domain.store.EmployeeStore;
import app.domain.store.RegisterSNSUserStore;

import app.mapper.dto.FullVaccinationDTO;
import app.mapper.dto.UserVaccineRecordDTO;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class StatisticsController {



    private EmployeeStore employeeStore;
    private VacCenter center;
    private VacCenter vacCenter;
    private Company company;

    public StatisticsController(Company company) {
        this.company=company;
    }


   /**
    * It takes two dates as parameters, then it goes through all the users in the system, and for each user it goes through
    * all the vaccines that the user has, and if the user has finished the vaccine, it checks if the last dose date is
    * between the two dates, and if it is, it adds the user to a list of fully vaccinated users
    *
    * @param localDate The start date of the period you want to check
    * @param localDate2 The date of the last dose of the vaccine
    */
   public static void VacinationStatisticsGUI(LocalDate localDate, LocalDate localDate2) throws FileNotFoundException {
        List<SNSUser> snsUsers = RegisterSNSUserStore.getSNSUsers();
        List<FullVaccinationDTO> fullyVaccinated = new ArrayList<>();
        for (SNSUser snsUser : snsUsers) {
            if (!snsUser.getUserVaccineRecord().getUserVaccineRecordDto().isEmpty()) {
                for (UserVaccineRecordDTO userVaccine : snsUser.getUserVaccineRecord().getUserVaccineRecordDto()) {
                    if (userVaccine.endedVaccination()) {
                        LocalDate userLastDoseDate = userVaccine.lastDoseDate().toLocalDate();
                        if (userLastDoseDate.isAfter(localDate) && userLastDoseDate.isBefore(localDate2))
                            fullyVaccinated.add(new FullVaccinationDTO(snsUser.getSNSNumber(), userVaccine.lastDoseDate().toLocalDate()));
                    }
                }
            }
        }
        treatData(fullyVaccinated);
    }

    /**
     * It takes a list of objects, sorts them by date, counts the amount of objects per date and writes the result to a csv
     * file
     *
     * @param fullyVaccinated a list of all the users who have been fully vaccinated
     */
    private static void treatData(List<FullVaccinationDTO> fullyVaccinated) throws FileNotFoundException {
        List<LocalDate> differentDates = new ArrayList<>();

        for (FullVaccinationDTO vac : fullyVaccinated) {
            if (!differentDates.contains(vac.dateOfFullVaccination)) {
                differentDates.add(vac.dateOfFullVaccination);
            }
        }
        differentDates.sort(Comparator.comparing(LocalDate::atStartOfDay));
        List<Integer> amountPerDate = new ArrayList<>();
        for (int i = 0; i < differentDates.size(); i++) {
            amountPerDate.add(0);
            for (FullVaccinationDTO vac : fullyVaccinated) {
                if (differentDates.get(i).equals(vac.dateOfFullVaccination)) {
                    amountPerDate.set(i, amountPerDate.get(i) + 1);

                }

            }
        }

        String dir = System.getProperty("user.dir");
        PrintWriter pw = new PrintWriter(new File(dir + "/src/main/resources/files/newFile.csv"));
        StringBuilder sb = new StringBuilder();
        sb.append("date");
        sb.append(",");
        sb.append("amount of users");

        sb.append("\r\n");
        int i = 0;
        for (LocalDate date : differentDates) {
            sb.append(date.toString());
            sb.append(",");
            sb.append(amountPerDate.get(i));
            sb.append("\r\n");
            i++;
        }

        pw.write(sb.toString());
        pw.close();
    }


    public List<SNSUser> getVacinationStatisticssGUI() {
        return center.getWaitroom();
    }


    public void VacinationStatisticssGUI(List<SNSUser> getVacinationStatisticssGUI) {
        return;
    }

    // A constructor
    public StatisticsController() {
        this.company = App.getInstance().getCompany();
        this.employeeStore = company.getEmployeeStore();
    }
    public VacCenter getVaccinationCenter(String email) {

        VacCenter center = null;

        List<Employee> employeeList = employeeStore.getEmployees();

        for (Employee employee : employeeList) {
            if (email.equals(employee.getEmail())) {
                center = employee.getVacCenter();
            }
        }
        return center;
    }
    /**
     * This function reads the email from the LoginInfo.txt file and returns it
     *
     * @return The email address of the user.
     */
    public String readLoginInfoFile() throws FileNotFoundException {

        File file = new File("LoginInfo.txt");
        Scanner reader = new Scanner(file);
        String email = reader.nextLine();
        reader.close();

        return email;
    }

    public void treatVaccinationData(List<FullVaccinationDTO> fullVaccinationDTOS,LocalDate localDate, LocalDate localDate2) throws FileNotFoundException {
        FileCreator.treatVaccinationData(fullVaccinationDTOS, localDate, localDate2);
    }
}
