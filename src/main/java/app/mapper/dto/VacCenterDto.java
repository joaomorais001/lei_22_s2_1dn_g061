package app.mapper.dto;

public class VacCenterDto {

    private int openingHour;
    private int closingHour;
    private int maxVaccines;


    public VacCenterDto(int openingHour, int closingHour,int maxVaccines) {


        this.openingHour = openingHour;
        this.closingHour = closingHour;
        this.maxVaccines = maxVaccines;
    }

    public VacCenterDto(){

    }
    public int getOpeningHour() {
        return openingHour;
    }

    public int getClosingHour() {
        return closingHour;
    }

    public int getMaxVaccines() {
        return maxVaccines;
    }

    public void setOpeningHour(int openingHour) {
        this.openingHour = openingHour;
    }

    public void setClosingHour(int ClosingHour) {
        this.closingHour = ClosingHour;
    }

    public void setMaxVaccines(int maxVaccines) {
        this.maxVaccines = maxVaccines;
    }
}
