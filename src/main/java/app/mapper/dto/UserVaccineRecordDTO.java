package app.mapper.dto;

import java.time.LocalDateTime;
import java.util.List;
public class UserVaccineRecordDTO {
    public List<LocalDateTime> doseDateTime;
    public int currentDose;
    private int maxDose;
    public LocalDateTime lastDoseDate(){
        int size=doseDateTime.size();
        return doseDateTime.get(size-1);
    }

    public boolean endedVaccination(){
        return this.currentDose==maxDose;
    }
}
