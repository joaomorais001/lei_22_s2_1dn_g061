package app.mapper.dto;

import java.util.Date;

public class SNSUserDTO {

    private String name;
    private int phoneNumber;
    private int SNSNumber;
    private String sex;
    private Date birthdate;

    public SNSUserDTO(int phoneNumber, int SNSNumber, String name, Date birthdate, String sex) {

        this.name = name;
        this.phoneNumber = phoneNumber;
        this.SNSNumber = SNSNumber;
        this.birthdate = birthdate;
        this.sex = sex;
    }

    public SNSUserDTO() {

    }

    /**
     * This function returns the name of the person.
     *
     * @return The name of the person.
     */
    public String getName() {
        return name;
    }

    /**
     * This function returns the phone number of the person
     *
     * @return The phone number of the contact.
     */
    public int getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * This function returns the SNSNumber of the user
     *
     * @return The SNSNumber is being returned.
     */
    public int getSNSNumber() {
        return SNSNumber;
    }

    /**
     * This function returns the birthdate of the person.
     *
     * @return The birthdate of the person.
     */
    public Date getBirthdate() {
        return birthdate;
    }

    /**
     * This function returns the sex of the person
     *
     * @return The sex of the person.
     */
    public String getSex() {
        return sex;
    }

    /**
     * This function sets the name of the object to the value of the parameter name.
     *
     * @param name The name of the parameter.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This function sets the phone number of the user
     *
     * @param phonenumber The phone number of the user.
     */
    public void setPhoneNumber(int phonenumber) {
        this.phoneNumber = phonenumber;
    }

    /**
     * This function sets the SNSNumber of the user
     *
     * @param snsnumber The number of SNS messages to send.
     */
    public void setSNSNumber(int snsnumber) {
        this.SNSNumber = snsnumber;
    }

    /**
     * This function sets the birthdate of the person to the given birthdate.
     *
     * @param birthdate The date of birth of the person.
     */
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    /**
     * This function sets the sex of the person
     *
     * @param Sex The sex of the person.
     */
    public void setSex(String Sex) {
        this.sex = Sex;
    }

    /**
     * The function returns a string that contains the name, sex, phone number, SNS
     * number, and birthdate of the person
     *
     * @return The string representation of the object.
     */
    public String toString() {
        return String.format(" Name: %s\n Sex: %s\n Phone Number: %d\n SNS number: %d\n Birthdate: %s\n", name,
                sex, phoneNumber, SNSNumber, birthdate);
    }
}