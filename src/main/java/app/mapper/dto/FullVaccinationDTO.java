package app.mapper.dto;

import java.time.LocalDate;

public class FullVaccinationDTO {

    public int snsNumber;

    public LocalDate dateOfFullVaccination;

    public FullVaccinationDTO(int snsNumber, LocalDate dateOfFullVaccination){
        this.snsNumber=snsNumber;
        this.dateOfFullVaccination=dateOfFullVaccination;
    }
}
