package app.mapper;

import app.domain.model.VacCenter;
import app.mapper.dto.VacCenterDto;

public class VacCenterMapper {


    public VacCenterMapper() {

    }

    public VacCenterDto toDTO(VacCenter center){
        VacCenterDto vacDTO = new VacCenterDto();

        vacDTO.setClosingHour(center.getClosingHour());
        vacDTO.setOpeningHour(center.getOpeningHour());
        vacDTO.setMaxVaccines(center.getMaxVaccines());

        return vacDTO;
    }
}
