package app.mapper;

import app.domain.model.SNSUser;
import app.mapper.dto.SNSUserDTO;

import java.util.ArrayList;
import java.util.List;

public class SNSUserMapper {

    public SNSUserMapper() {

    }

    /**
     * Convert a list of SNSUser objects to a list of SNSUserDTO objects.
     *
     * @param users The list of users to convert to DTOs.
     * @return A list of SNSUserDTO objects.
     */
    public List<SNSUserDTO> toListDTO(List<SNSUser> users) {
        List<SNSUserDTO> usersDTO = new ArrayList();

        for (SNSUser user : users) {
            usersDTO.add(toDTO(user));
        }

        return usersDTO;
    }

    /**
     * > This function converts a SNSUser object to a SNSUserDTO object
     *
     * @param user The user object that is to be converted to a DTO.
     * @return A SNSUserDTO object.
     */
    public SNSUserDTO toDTO(SNSUser user) {
        SNSUserDTO userDTO = new SNSUserDTO();

        userDTO.setName(user.getName());
        userDTO.setBirthdate(user.getBirthdate());
        userDTO.setSex(user.getSex());
        userDTO.setSNSNumber(user.getSNSNumber());
        userDTO.setPhoneNumber(user.getPhoneNumber());

        return userDTO;
    }
}
