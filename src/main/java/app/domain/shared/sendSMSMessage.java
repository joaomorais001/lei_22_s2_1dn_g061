package app.domain.shared;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.TimerTask;


public interface sendSMSMessage {

    String BlankSpace = String.format("-------------------------------------------------------%n");


    static void sendSMS(String day,String time,String vaccinationCenter,int i) throws FileNotFoundException {
        File file;

        file = new File("SMSs");
        if (!file.exists()) {
            try {
                Files.createDirectory(Path.of(String.valueOf(file)));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        do {
            i++;
            file = new File("SMSs/SMS"+i+".txt");

        }while (file.exists());

        if (!file.exists()) {
            try {
                Files.createFile(Path.of(String.valueOf(file)));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        PrintWriter pw = new PrintWriter(file);


        pw.printf("SMS\n");
        pw.printf(BlankSpace);
        pw.printf("Hello! DGS informs this number that Mr/Mrs has scheduled a vaccine with success.\n");
        pw.printf(BlankSpace);
        pw.printf("The important information is indicated below:\n");
        pw.printf("-Date: %s%n",day);
        pw.printf("-Time: %s%n",time);
        pw.printf("-Vaccination Center: %s%n",vaccinationCenter);
        pw.printf(BlankSpace);
        pw.printf("Regards from our team.");

        pw.close();
    }
    static void sendSMSRecoveryRoom() throws FileNotFoundException{

        int x=1;
        File file = new File("RecoverySMS"+x+".txt");
        x++;

        PrintWriter pw = new PrintWriter(file);
        pw.printf("SMS\n");
        pw.printf(BlankSpace);
        pw.printf("Hello! DGS informs this number that Mr/Mrs can leave the Recovery Room\n");
        pw.printf(BlankSpace);
        pw.printf("Regards from our team.");

        pw.close();

    }
}
