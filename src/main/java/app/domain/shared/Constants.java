package app.domain.shared;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Constants {
    public static final String ROLE_ADMIN = "ADMINISTRATOR";
    public static final String ROLE_RECEPTIONIST = "RECEPTIONIST";
    public static final String ROLE_SNSUSER = "SNS USER";
    public static final String ROLE_NURSE = "NURSE";
    public static final String ROLE_CENTERCOORDINATOR = "CENTER COORDINATOR";


    public static final String PARAMS_FILENAME = "config.properties";
    public static final String PARAMS_COMPANY_DESIGNATION = "Company.Designation";
    public static final String ROLE_CENTER_COORDINATOR = null;
}
