package app.domain.store;

import app.domain.model.Employee;
import app.domain.model.SNSUser;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.AuthFacade;

import java.util.ArrayList;
import java.util.List;

public class EmployeeStore {

    private static ArrayList<Employee> EmployeeStore;
    private AuthFacade authFacade;

    public EmployeeStore(AuthFacade authfacade) {
        EmployeeStore = (ArrayList<Employee>) Utils.readSerialization("EmployeeData");
        authFacade = authfacade;
    }


    /**
     * Return a copy of the EmployeeStore.
     *
     * @return A list of employees
     */
    public List<Employee> getEmployees() {
        return new ArrayList<>(this.EmployeeStore);
    }

    /**
     * validates the employee
     * 
     * @param employee that was registered
     * @return True if the user is valid, otherwise, returns false
     */
    public boolean validateEmployee(Employee employee) {
        if (!authFacade.existsUser(employee.getEmail())) {
            return true;
        } else
            return false;
    }

    /**
     * saves the employee if its valid
     * 
     * @param employee that was registered
     * @return True if the employee was saved sucessfully, otherwise, returns false
     */
    public boolean saveEmployee(Employee employee) {
        if (!validateEmployee(employee)) {
            return false;
        }

        EmployeeStore.add(employee);
        Utils.writeSerialization("EmployeeData", EmployeeStore);
        return true;
    }

    public void addEmployee(Employee employee){
        EmployeeStore.add(employee);
    }

}
