package app.domain.store;

import app.controller.App;
import app.domain.model.SNSUser;
import app.domain.model.ScheduleVaccine;
import app.domain.shared.sendSMSMessage;
import app.ui.console.utils.Utils;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class ScheduleVaccineStore implements sendSMSMessage {

    private static ArrayList<ScheduleVaccine> appointmentList;


    public ScheduleVaccineStore() {
        appointmentList = (ArrayList<ScheduleVaccine>) Utils.readSerialization("AppointmentData");
    }

    /**
     * This function adds a new appointment to the appointment list
     *
     * @param Appointment The appointment object that is to be added to the list.
     * @return A boolean value.
     */
    public static boolean addSNSUserAppointment(ScheduleVaccine Appointment){
        if (!validateAppointment(Appointment)) {
            return false;
        }
        else {
            try {
                int i=0;
                sendSMSMessage.sendSMS(Appointment.getData(),Appointment.getTime(),Appointment.getVaccineCenter(),i);
            } catch (FileNotFoundException e) {
                throw new IllegalArgumentException("Error") ;
            }
        }
        appointmentList.add(Appointment);
        Utils.writeSerialization("AppointmentData", appointmentList);
        return true;
    }

    /**
     * This function checks if the appointment is already made
     *
     * @param Appointment The appointment that is being validated.
     * @return The method is returning a boolean value.
     */
    public static boolean validateAppointment(ScheduleVaccine Appointment) {
        if (Appointment == null) {
            throw new IllegalArgumentException("You have to insert a SNSuser!");

        }
        if (appointmentList.contains(Appointment)) {
            throw new IllegalArgumentException("Appointment already made!");
        }
        return !appointmentList.contains(Appointment);
    }

    /**
     * It returns a copy of the appointmentList
     *
     * @return A list of all the appointments
     */
    public List<ScheduleVaccine> getAppointments() {
        return  new ArrayList<>(appointmentList);
    }

}
