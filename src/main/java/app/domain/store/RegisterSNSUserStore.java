package app.domain.store;
import app.domain.model.SNSUser;
import app.ui.console.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RegisterSNSUserStore {
    private static ArrayList<SNSUser> userList;

    public RegisterSNSUserStore() {
        userList = (ArrayList<SNSUser>) Utils.readSerialization("SNSUserData");

    }


    /**
     * > This function adds a user to the userList if the user is valid
     *
     * @param user The user object to be saved.
     * @return A boolean value.
     */
    public static boolean saveSNSUser(SNSUser user) {
        if (!validateSNSUser(user)) {
            return false;
        }

        userList.add(user);
        Utils.writeSerialization("SNSUserData", userList);

        return true;
    }

    /**
     * This function checks if the user is already registered. If the user is already registered, it throws an exception
     *
     * @param user The user to be validated.
     * @return A boolean value.
     */
    public static boolean validateSNSUser(SNSUser user) {
        if (user == null) {
            throw new IllegalArgumentException("You have to insert a SNSuser!");

        }
        if (userList.contains(user)) {
            throw new IllegalArgumentException("User is already registered!");
        }
        return !userList.contains(user);
    }

    /**
     * Return a copy of the userList.
     *
     * @return A copy of the list of users.
     */
    public static List<SNSUser> getSNSUsers() {
        return new ArrayList<>(userList);
    }

}
