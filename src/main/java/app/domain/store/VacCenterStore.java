package app.domain.store;

import app.domain.model.SNSUser;
import app.domain.model.VacCenter;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class VacCenterStore {

    private static ArrayList<VacCenter> vacCenters;

    public VacCenterStore(){
        vacCenters = (ArrayList<VacCenter>) Utils.readSerialization("VacCenterData");
    }

    public List<VacCenter> getVacCenterlist() {
        return new ArrayList<>(this.vacCenters);
    }

    public boolean validationVacCenter(VacCenter vacCenter) {
        if (vacCenter == null) {
            return false;
        }
        return true;
    }

    public boolean addVacCenter(VacCenter vacCenter) {

        vacCenters.add(vacCenter);
        Utils.writeSerialization("VacCenterData", vacCenters);
        return true;

    }

    public ArrayList<VacCenter> getVacCenters() {
        return vacCenters;
    }


}
