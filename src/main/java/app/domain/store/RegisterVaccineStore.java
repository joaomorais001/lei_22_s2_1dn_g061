package app.domain.store;
import app.domain.model.SNSUser;
import app.domain.model.Vaccine;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class RegisterVaccineStore {
    private static ArrayList<Vaccine> vaccinesList;

    public RegisterVaccineStore() {
        vaccinesList = (ArrayList<Vaccine>) Utils.readSerialization("VaccineData");
    }



    public boolean saveVaccine(Vaccine vaccine) {
       if (!validateVaccine(vaccine)) {
            return false;
        }

       vaccinesList.add(vaccine);
       Utils.writeSerialization("VaccineData", vaccinesList);

       return true;
    }

    public static boolean validateVaccine(Vaccine vaccine) {
        if (vaccine == null) {
            throw new IllegalArgumentException("You have to insert a Vaccine!");

        }
        if (vaccinesList.contains(vaccine)) {
            throw new IllegalArgumentException("Vaccine is already registered!");
        }
        return !vaccinesList.contains(vaccine);
    }

    /**
     * Return a copy of the userList.
     *
     * @return A copy of the list of users.
     */
    public List<Vaccine> getVaccines() {
        return new ArrayList<>(this.vaccinesList);
    }

}
