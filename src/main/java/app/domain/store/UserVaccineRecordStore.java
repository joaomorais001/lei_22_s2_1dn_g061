package app.domain.store;
import app.domain.model.UserVaccineRecord;
import java.util.ArrayList;
import java.util.List;

public class UserVaccineRecordStore {
    private static ArrayList<UserVaccineRecord> userVaccineRecordsList;

    public UserVaccineRecordStore(){userVaccineRecordsList = new ArrayList<>();}

    public  void saveVaccineAdministration (UserVaccineRecord userVacRecord){

        if (!userVaccineRecordsList.contains(userVacRecord)) {
            userVaccineRecordsList.add(userVacRecord);
        }
    }
  public  List<UserVaccineRecord> getUserVaccineRecord (){return userVaccineRecordsList;}
}