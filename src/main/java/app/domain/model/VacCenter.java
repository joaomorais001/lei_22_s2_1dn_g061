package app.domain.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VacCenter implements Serializable {
    private String name;
    private String address;
    private String phoneNumber;
    private String faxNumber;
    private String website;
    private int openingHour;
    private int closingHour;
    private int slotDuration;
    private int maxVaccines; // max no. of vaccines that can be given per slot

    private int vaccineCounter=0;

    private List <VacCenter> vacCenters;

    private List <Employee> employeeList = new ArrayList<>();
    private List<SNSUser> waitroom= new ArrayList<>();
    private List <RecoveryRoom> recoveryRoom = new ArrayList<>();
    private ArrayList<String> legacydata = new ArrayList<String>();
    private List<EntryRecords> entryRecords = new ArrayList<>();
    private List<DepartureRecords> departureRecords = new ArrayList<>();
    public VacCenter(String name, String address, String phoneNumber, String faxNumber, String website, int openingHour,
            int closingHour, int slotDuration, int maxVaccines) {

        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.website = website;
        this.openingHour = openingHour;
        this.closingHour = closingHour;
        this.slotDuration = slotDuration;
        this.maxVaccines = maxVaccines;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public int getOpeningHour() {
        return openingHour;
    }

    public int getClosingHour() {
        return closingHour;
    }

    public int getSlotDuration() {
        return slotDuration;
    }

    public int getMaxVaccines() {
        return maxVaccines;
    }

    public void addWaitRoom(SNSUser snsUser) {
        this.waitroom.add(snsUser);
    }

    public void addRecoveryRoom(RecoveryRoom user){
        this.recoveryRoom.add(user);
    }

    public List<SNSUser> getWaitroom() {
        return waitroom;
    }

    public List<RecoveryRoom> getRecoveryRoom(){
        return recoveryRoom;
    }

    public void addLegacyData(String[][] list){
        for (int i = 0; i < list.length; i++) {
            this.legacydata.add(Arrays.deepToString(list[i]));
        }
    }

    public ArrayList<String> getLegacydata(){ return legacydata;}

    public VacCenter createVaccinationCenter(String name, String address, String phoneNumber, String faxNumber,
            String website, int openingHour, int closingHour, int slotDuration, int maxVaccines) {
        VacCenter vacCenter = new VacCenter(name, address, phoneNumber, faxNumber, website, openingHour, closingHour,
                slotDuration, maxVaccines);
        this.vacCenters.add(vacCenter);
        return vacCenter;
    }

    public List<VacCenter> showAllVacCenters() {

        return this.vacCenters;
    }

    public List<EntryRecords> getEntryRecords() {
        return entryRecords;
    }

    public List<DepartureRecords> getDepartureRecords() {
        return departureRecords;
    }

    public void addEntryRecord(LocalDateTime entryRecord, int SNSnumber) {
        EntryRecords record = new EntryRecords(entryRecord, SNSnumber);
        this.entryRecords.add(record);
    }

    public void addDepartureRecord(LocalDateTime departureRecord, int SNSnumber) {
        DepartureRecords record = new DepartureRecords(departureRecord, SNSnumber);
        this.departureRecords.add(record);
    }

    public String toString(){
        return String.format(" Name: %s\n", name);
    }

}
