package app.domain.model;

import java.time.LocalDate;

public class RecoveryRoom {

    private LocalDate day;
    private int user;



    public RecoveryRoom(int user) {
        this.day = LocalDate.now();
        this.user = user;

    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public LocalDate getDay() {
        return day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }


    @Override
    public String toString() {
        return "RecoveryRoom{" +
                "day=" + day +
                ", user=" + user +
                '}';
    }
}
