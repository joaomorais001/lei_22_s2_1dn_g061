package app.domain.model;

import java.time.LocalDateTime;

public class DepartureRecords {
    
    // Declaring a variable called departureRecord of type LocalDateTime.
    private LocalDateTime departureRecord;
    // Declaring a variable called SNSNumber of type int.
    private int SNSNumber;
    
    // A constructor.
    public DepartureRecords(LocalDateTime departureRecord, int number){
        this.departureRecord = departureRecord;
        this.SNSNumber = number;
    }

    /**
     * This function sets the departure record of the vehicle
     *
     * @param departureRecord The time the user left the parking lot.
     */
    public void setDepartureRecord(LocalDateTime departureRecord){
        this.departureRecord = departureRecord;
    }

    /**
     * This function sets the SNSNumber variable to the number passed in.
     *
     * @param number The number of the SNS user you want to set.
     */
    public void setSNSUser(int number){
        this.SNSNumber = number;
    }

    /**
     * This function returns the departure record of the flight
     *
     * @return The departureRecord is being returned.
     */
    public LocalDateTime getDepartureRecord(){
        return departureRecord;
    }

    /**
     * This function returns the number of SNS users
     *
     * @return The number of SNS users.
     */
    public int getSnsUser(){
        return SNSNumber;
    }
    
    /**
     * The function returns a string that contains the departure record and the SNS number
     *
     * @return The string representation of the object.
     */
    public String toString(){
        return String.format("Departure time: %s\nSNS number: %s\n", departureRecord, SNSNumber);
    }
}
