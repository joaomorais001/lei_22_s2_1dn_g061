package app.domain.model;

import javafx.beans.property.SimpleStringProperty;

import java.time.LocalDate;

public class FullyVaccinated {
    private SimpleStringProperty snsNumber;
    private LocalDate dateOfFullVaccination;

    // The above code is creating a class called FullyVaccinated. It has two properties, snsNumber and
    // dateOfFullVaccination. The snsNumber is a SimpleStringProperty, which is a JavaFX class. The dateOfFullVaccination
    // is a LocalDate, which is a Java class.
    public FullyVaccinated(String snsNumber, LocalDate dateOfFullVaccination){
        this.snsNumber=new SimpleStringProperty(snsNumber);
        this.dateOfFullVaccination=dateOfFullVaccination;
    }

    public SimpleStringProperty snsNumberProperty() {
        return snsNumber;
    }

    public void setSnsNumber(String snsNumber) {
        this.snsNumber.set(snsNumber);
    }

    public LocalDate getDateOfFullVaccination() {
        return dateOfFullVaccination;
    }

    public void setDateOfFullVaccination(LocalDate dateOfFullVaccination) {
        this.dateOfFullVaccination = dateOfFullVaccination;
    }


}


