package app.domain.model;

import app.mapper.dto.FullVaccinationDTO;
import java.util.List;
import java.time.LocalDate;
import java.util.ArrayList;

// A class that is used to store data for testing purposes.
public class FullVaccinatedData {
    public static List<FullVaccinationDTO> fullVaccinationDTOS = new ArrayList<>();
    public static LocalDate localDate1;
    public static LocalDate localDate2;
}
