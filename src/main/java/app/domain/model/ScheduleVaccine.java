package app.domain.model;

import java.io.Serializable;

public class ScheduleVaccine implements Serializable {

    private int SNSNumber;

    private String Data;

    private String Time;

    private String Vaccinetype;

    private String VaccineCenter;
    private String SNSUserName;
    private String VaccineName;
    private String Dose;
    private String LotNumber;
    private String ScheduledDateTime;
    private String ArrivalDateTime;
    private String NurseAdministrationDateTimeLeavingDateTime;

    public ScheduleVaccine(String data, String Time, String vaccinetype, String vaccineCenter, int SNSNumber) {
        this.SNSNumber = SNSNumber;
        this.Data = data;
        this.Time = Time;
        this.Vaccinetype = vaccinetype;
        this.VaccineCenter = vaccineCenter;
    }
    public ScheduleVaccine(String SNSUSerNumber,String SNSUserName,String VaccineName,String Dose,String LotNumber,String ScheduledDateTime,String ArrivalDateTime,String NurseAdministrationDateTimeLeavingDateTime){
        this.SNSNumber= Integer.parseInt(SNSUSerNumber);
        this.SNSUserName=SNSUserName;
        this.VaccineName=VaccineName;
        this.Dose=Dose;
        this.LotNumber=LotNumber;
        this.ScheduledDateTime=ScheduledDateTime;
        this.ArrivalDateTime=ArrivalDateTime;
        this.NurseAdministrationDateTimeLeavingDateTime=NurseAdministrationDateTimeLeavingDateTime;
    }

    /**
     * The toString() method returns a string representation of the object
     *
     * @return The toString method is returning a string with the information of the appointment.
     */
    @Override
    public String toString() {
        return "\n--Appointment--\n" + "SNSNumber= " + SNSNumber+ "\n" + "Data= " + Data +"\n"+ "Time= " + Time + "\n" + "Vaccine Type= " + Vaccinetype +"\n"+ "Vaccine center= "+VaccineCenter+"\n";
    }

    /**
     * This function returns the number of SNS messages that have been sent
     *
     * @return The SNSNumber is being returned.
     */
    public int getSNSNumber() {
        return SNSNumber;
    }

    /**
     * This function returns the data of the node
     *
     * @return The data is being returned.
     */
    public String getData() {
        return Data;
    }

    /**
     * This function returns the time of the event
     *
     * @return The time of the event.
     */
    public String getTime() {
        return Time;
    }

    /**
     * This function returns the vaccine type of the patient
     *
     * @return The Vaccine type is being returned.
     */
    public String getVaccine() {
        return Vaccinetype;
    }

    /**
     * This function returns the VaccineCenter of the object
     *
     * @return The VaccineCenter is being returned.
     */
    public String getVaccineCenter() {
        return VaccineCenter;
    }

    /**
     * This function sets the SNSNumber of the user
     *
     * @param SNSNumber The number of SNS messages to send.
     */
    public void setSNSNumber(int SNSNumber) {
        this.SNSNumber = SNSNumber;
    }

    /**
     * This function sets the data of the object to the data passed in.
     *
     * @param data The data to be sent to the server.
     */
    public void setData(String data) {
        Data = data;
    }

    /**
     * This function sets the time of the event
     *
     * @param time The time of the event.
     */
    public void setTime(String time) {
        Time = time;
    }

    /**
     * This function sets the vaccine type
     *
     * @param vaccinetype The type of vaccine.
     */
    public void setVaccine(String vaccinetype) {
        Vaccinetype = vaccinetype;
    }

    /**
     * This function sets the vaccine center of the patient
     *
     * @param vaccineCenter The name of the vaccine center
     */
    public void setVaccineCenter(String vaccineCenter) {
        VaccineCenter = vaccineCenter;
    }


}
