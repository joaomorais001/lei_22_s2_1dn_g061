package app.domain.model;

import org.apache.commons.lang3.time.StopWatch;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class InsertionSort {
    /**
     * It takes a 2D array of strings and an integer as parameters, and sorts the 2D array by the integer
     *
     * @param listdata   The 2D array that contains the data to be sorted
     * @param timechosen the column number of the time you want to sort by.
     */
    public static String[][] sortByTime(String[][] listdata, int timechosen, int lines) throws ParseException {
        StopWatch watch = new StopWatch();
        watch.start();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        int n = listdata.length;
        for (int j = 1; j < n; j++) {
            String[] key = listdata[j];
            Date datekey = df.parse(listdata[j][timechosen]);
            int i = j - 1;
            while ((i > -1) && (df.parse(listdata[i][timechosen]).after(datekey))) {
                listdata[i + 1] = listdata[i];
                i--;
            }
            listdata[i + 1] = key;
        }
        watch.stop();
        System.out.println(watch.getTime(TimeUnit.MILLISECONDS));
        return listdata;
    }
}
