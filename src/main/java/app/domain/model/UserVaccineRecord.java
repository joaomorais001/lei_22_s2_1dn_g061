package app.domain.model;
import app.mapper.dto.UserVaccineRecordDTO;

import java.time.LocalDate;
import java.util.List;
public class UserVaccineRecord {
    private int SNSNumber;
    private String Code;
    private String Brand;
    private int DoseNumber;
    private String Dosage;
    private String VaccineType;
    private LocalDate Date;
    private String Reaction;
    private List<UserVaccineRecordDTO> vaccineList;


    public UserVaccineRecord(int SNSNumber,String Code,String Brand,String DoseNumber,String Dosage,String VaccineType,LocalDate Date){
        setUserVaccineRecord(SNSNumber,Code,Brand,DoseNumber,Dosage,VaccineType,Date);
    }

      public void setUserVaccineRecord (int SNSNumber,String Code,String Brand,String DoseNumber,String Dosage,String VaccineType,LocalDate Date){

        this.SNSNumber = SNSNumber;
        this.Code = Code;
        this.Brand = Brand;
        this.DoseNumber = Integer.parseInt(DoseNumber);
        this.Dosage = Dosage;
        this.VaccineType = VaccineType;
        this.Date = Date;

    }
    public UserVaccineRecord(int SNSNumber,String Code,String Brand,String DoseNumber,String Dosage,String VaccineType,LocalDate Date,String Reaction){
        setUserVaccineRecord(SNSNumber,Code,Brand,DoseNumber,Dosage,VaccineType,Date,Reaction);
    }

    public void setUserVaccineRecord (int SNSNumber,String Code,String Brand,String DoseNumber,String Dosage,String VaccineType,LocalDate Date,String Reaction){

        this.SNSNumber = SNSNumber;
        this.Code = Code;
        this.Brand = Brand;
        this.DoseNumber = Integer.parseInt(DoseNumber);
        this.Dosage = Dosage;
        this.VaccineType = VaccineType;
        this.Date = Date;
        this.Reaction = Reaction;
    }
    public String getReaction(){return Reaction;}
    public void  setReaction(String reaction){this.Reaction=Reaction;}
    public int getSNSNumber(){return SNSNumber;}
    public String getDoseNumber(){return String.valueOf(DoseNumber);}
    public String getDosage(){return Dosage;}
    public String getVaccineType(){return VaccineType;}
    public String getCode(){return Code;}
    public String getBrand(){return Brand;}
    public LocalDate getDate(){
        return Date;
    }
    public void setBrand(String Brand) {
        this.Brand = Brand;
    }
    public void setCode(String Code) {
        this.Code = Code;
    }
    public void setDosage(String Dosage) {
        this.Dosage = Dosage;
    }
    public void setDoseNumber(String DoseNumber) {
        this.DoseNumber = Integer.parseInt(DoseNumber);
    }
    public void setSNSNumber(Integer SNSNumber) {
        this.SNSNumber = SNSNumber;
    }
    public void setVaccineType(String VaccineType) {
        this.VaccineType = VaccineType;
    }
    public void setDate (LocalDate Date){this.Date = Date;}

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof UserVaccineRecord))
            return false;
        UserVaccineRecord userVacRecord = (UserVaccineRecord) o;
        return SNSNumber == (userVacRecord.SNSNumber) &&
                Code.equals(userVacRecord.Code) &&
                Brand.equals(userVacRecord.Brand) &&
                DoseNumber == (userVacRecord.DoseNumber) &&
                Dosage.equals(userVacRecord.Dosage) &&
                VaccineType.equals(userVacRecord.VaccineType)&&
                Date.equals(userVacRecord.Date);
    }

    public List<UserVaccineRecordDTO> getUserVaccineRecordDto(){
        return this.vaccineList;
    }

}
