package app.domain.model;

import app.domain.store.*;
import org.apache.commons.lang3.StringUtils;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;
import java.util.List;

/**
     *
     * @author Paulo Maio <pam@isep.ipp.pt>
     */
    public class Company {

        private String designation;
        private AuthFacade authFacade;
        private RegisterSNSUserStore userStore;
        private RegisterVaccineStore newVaccine;

        //private WaitRoomStore newWrs;
        private EmployeeStore EmployeeStore;

        private ScheduleVaccineStore scheduleStore;

        private VacCenterStore vacStore;
        private UserVaccineRecordStore userVacStore;

        public Company(String designation)
        {
            if (StringUtils.isBlank(designation))
                throw new IllegalArgumentException("Designation cannot be blank.");

            this.designation = designation;
            this.authFacade = new AuthFacade();
            this.userStore = new RegisterSNSUserStore();
            this.newVaccine = new RegisterVaccineStore();
            //this.newWrs = new WaitRoomStore();
            this.scheduleStore = new ScheduleVaccineStore();
            this.EmployeeStore = new EmployeeStore(authFacade);
            this.vacStore = new VacCenterStore();
            this.userVacStore = new UserVaccineRecordStore();
        }

        public String getDesignation() {
            return designation;
        }

        public AuthFacade getAuthFacade() {
            return authFacade;
        }

        public List<UserRoleDTO> getUserRoles() {
        return getAuthFacade().getUserRoles();
    }

        public RegisterSNSUserStore getSNSUserStore() {
            return userStore;
        }

        public RegisterVaccineStore getRegisterVaccineStore() {
            return newVaccine;
        }

        public EmployeeStore getEmployeeStore() {
            return EmployeeStore;
        }

        //public WaitRoomStore getNewWrs(){return newWrs;}

        public ScheduleVaccineStore getScheduleStore(){
            return scheduleStore;
        }

        public VacCenterStore getVacStore() {
            return vacStore;
        }
        public UserVaccineRecordStore getUserVacStore(){return userVacStore;}

    }

