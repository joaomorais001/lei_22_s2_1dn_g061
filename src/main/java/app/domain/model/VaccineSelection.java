package app.domain.model;

public class VaccineSelection {
    private String AdministrationTime;
    private int SNSNumber;
    private String Code;
    private String Brand;
    private int DoseNumber;
    private String Dosage;
    private String VaccineType;


    public VaccineSelection (String AdminstrationTime, int SNSNumber, String Code, String Brand, int DoseNumber, String Dosage, String VaccineType){
        this.AdministrationTime = AdminstrationTime;
        this.SNSNumber = SNSNumber;
        this.Code = Code;
        this.Brand = Brand;
        this.DoseNumber = DoseNumber;
        this.Dosage = Dosage;
        this.VaccineType = VaccineType;
    }
    public String getAdministrationTime() {
        return AdministrationTime;
    }

    public int getSNSNumber() {
        return SNSNumber;
    }

    public int getDoseNumber() {
        return DoseNumber;
    }

    public String getBrand() {
        return Brand;
    }

    public String getCode() {
        return Code;
    }

    public String getDosage() {
        return Dosage;
    }

    public String getVaccineType() {
        return VaccineType;
    }

    public void setSNSNumber(int SNSNumber) {
        this.SNSNumber = SNSNumber;
    }

    public void setCode(String code) {
        Code = code;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public void setAdministrationTime(String administrationTime) {
        AdministrationTime = administrationTime;
    }

    public void setDosage(String dosage) {
        Dosage = dosage;
    }

    public void setDoseNumber(int doseNumber) {
        DoseNumber = doseNumber;
    }

    public void setVaccineType(String vaccineType) {
        VaccineType = vaccineType;
    }
}
