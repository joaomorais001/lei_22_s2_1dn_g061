package app.domain.model;

import java.time.LocalDateTime;

public class EntryRecords {
    
    // Declaring a private variable that is used to store the entry record of the user.
    private LocalDateTime entryRecord;
    // A private variable that is used to store the SNS number of the user.
    private int SNSNumber;
    
    // A constructor that is used to create an object of the class EntryRecords.
    public EntryRecords(LocalDateTime entryRecord, int number){
        this.entryRecord = entryRecord;
        this.SNSNumber = number;
    }

    /**
     * This function sets the entryRecord of the object to the value of the parameter entryRecord
     *
     * @param entryRecord The date and time the entry was created.
     */
    public void setEntryRecord(LocalDateTime entryRecord){
        this.entryRecord = entryRecord;
    }

    /**
     * This function sets the SNSNumber variable to the number passed in.
     *
     * @param number The number of the SNS user you want to set.
     */
    public void setSNSUser(int number){
        this.SNSNumber = number;
    }

    /**
     * This function returns the entryRecord of the object
     *
     * @return The entryRecord variable is being returned.
     */
    public LocalDateTime getEntryRecord(){
        return entryRecord;
    }

    /**
     * This function returns the number of SNS users
     *
     * @return The number of SNS users.
     */
    public int getSnsUser(){
        return SNSNumber;
    }
    
    /**
     * The function returns a string that contains the entry time and the SNS number of the entry record
     *
     * @return The string representation of the object.
     */
    public String toString(){
        return String.format("Entry time: %s\nSNS number: %s\n", entryRecord, SNSNumber);
    }
}