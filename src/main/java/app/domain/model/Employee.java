package app.domain.model;

import java.io.Serializable;

public class Employee implements Serializable {

    /**
     * Name of the Employee
     */
    private String name;
    /**
     * Email of the Employee
     */
    private String email;
    /**
     * CC Number of the Employee
     */
    private int CCNumber;
    /**
     * Phone Number of the Employee
     */
    private int phoneNumber;
    /**
     * Address of the Employee
     */
    private String address;
    /**
     * ID of the Employee
     */
    private String id;

    private VacCenter center;
    /**
     * Defines the max characters of the name and address
     */
    private static int nameMax = 100;
    private static int adressMax = 100;
    /**
     * Defines the portuguese format of the CC number and the phone number
     */
    private static int CCDigits = 8;
    private static int phoneDigits = 9;

    /**
     * builds and instance of an employee receiving the name, email, address,phone
     * number, cc number, role
     *
     * @param name        name of the employee
     * @param email       email of the employee
     * @param address     address of the employee
     * @param phoneNumber phone number of the employee
     * @param CCNumber    CC number of the employee
     * @param id          id of the employee
     */
    public Employee(String name, String email, String address, int phoneNumber, int CCNumber, String id) {
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.CCNumber = CCNumber;
        this.address = address;
        this.id = id;
    }

    public Employee(String name, String email, String address, int phoneNumber, int CCNumber, String id, VacCenter center){
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.CCNumber = CCNumber;
        this.address = address;
        this.id = id; 
        this.center = center;
    }

    /**
     * Returns the Name of the employee
     * 
     * @return Name of the employee
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name
     * 
     * @param name name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the email of the employee
     * 
     * @return email of the employee
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email
     * 
     * @param email email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Returns the adress of the employee
     * 
     * @return adress of the employee
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the address
     * 
     * @param address address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Returns the PhoneNumber of the employee
     * 
     * @return phone number of the employee
     */
    public int getPhonenumber() {
        return phoneNumber;
    }

    /**
     * Sets the phone number
     * 
     * @param phoneNumber phone number to set
     */
    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Returns the CCNumber of the employee
     * 
     * @return CCNumber of the employee
     */
    public int getCCnumber() {
        return CCNumber;
    }

    /**
     * Sets the CC number
     * 
     * @param CCNumber CC number to set
     */
    public void setCCNumber(int CCNumber) {
        this.CCNumber = CCNumber;
    }

    /**
     * Returns the id of the employee
     * 
     * @return id of the employee
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the id
     * 
     * @param id id to set
     */
    public void setID(String id) {
        this.id = id;
    }

    public VacCenter getVacCenter(){
        return center;
    }

    public void setVacCenter(VacCenter center){
        this.center = center;
    }

    /**
     *
     * @return returns the employee details in a String format
     */
    public String toString() {
        return String.format(" Name: %s\n Address: %s\n Email: %s\n Phone Number: %d\n CC number: %d\n ID: %s\n", name,
                address, email, phoneNumber, CCNumber, id);
    }

    /**
     * This function checks if the name is null, or if the name is longer than the
     * maximum length, or if the name is empty, then throw an
     * IllegalArgumentException.
     *
     * @param name The name of the person.
     */
    public static void validateName(String name) {
        if (name == null || (name.length() > nameMax) || name.equals("")) {
            throw new IllegalArgumentException("Invalid Name!");
        }
    }

    /**
     * This function checks if the address is null, if it's longer than the maximum
     * length, or if it's empty. If any of
     * these conditions are true, it throws an IllegalArgumentException
     *
     * @param address The address of the customer.
     */
    public static void validateAddress(String address) {
        if (address == null || address.length() > adressMax || address.equals("")) {
            throw new IllegalArgumentException("Invalid Address!");
        }
    }

    /**
     * If the length of the phone number is not equal to the number of digits in a
     * phone number, throw an exception.
     *
     * @param phoneNumber The phone number to validate.
     */
    public static void validatePhoneNumber(int phoneNumber) {
        if (String.valueOf(phoneNumber).length() != phoneDigits) {
            throw new NumberFormatException("Invalid Phone Number!");
        }
    }

    /**
     * If the length of the string representation of the CCNumber is not equal to
     * the constant CCDigits, throw a
     * NumberFormatException.
     *
     * @param CCNumber The credit card number to be validated.
     */
    public static void validateCCNumber(int CCNumber) {
        if (String.valueOf(CCNumber).length() != CCDigits) {
            throw new NumberFormatException("Invalid CC Number!");
        }

    }
}
