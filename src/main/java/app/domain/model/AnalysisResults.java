package app.domain.model;

import java.time.LocalDate;

public class AnalysisResults {
    // A private variable that is being used to store the differences array.
    private int[] differences;
    // A private variable that is being used to store the maximum sum sublist.
    private int[] maximumSumSublist;
    // A private variable that is being used to store the maximum sum of any contiguous subarray in the array.
    private int maximumSum;
    // A private variable that is being used to store the date.
    private LocalDate date;
    // A private variable that is being used to store the time interval.
    private int timeInterval;
    // A private variable that is being used to store the period.
    private String[] period;

    // A constructor.
    public AnalysisResults(int[] differences, int[] maximumSumSublist, int maximumSum, LocalDate date, int timeInterval, String[] period) {
        this.differences = differences;
        this.maximumSumSublist = maximumSumSublist;
        this.maximumSum = maximumSum;
        this.date = date;
        this.timeInterval = timeInterval;
        this.period = period;
    }

    /**
     * This function returns the differences array.
     *
     * @return The differences array is being returned.
     */
    public int[] getDifferences() {
        return differences;
    }

    /**
     * Return the maximum sum sublist.
     *
     * @return The maximum sum sublist is being returned.
     */
    public int[] getMaximumSumSublist() {
        return maximumSumSublist;
    }

    /**
     * Return the maximum sum of any contiguous subarray in the array a.
     *
     * @return The maximum sum of the subarray.
     */
    public int getMaximumSum() {
        return maximumSum;
    }

    /**
     * This function returns the date of the event
     *
     * @return The date of the transaction.
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * This function returns the time interval of the timer
     *
     * @return The time interval of the timer.
     */
    public int getTimeInterval(){
        return timeInterval;
    }

    /**
     * This function returns the period of the class
     *
     * @return The array period.
     */
    public String[] getPeriod(){
        return period;
    }
}