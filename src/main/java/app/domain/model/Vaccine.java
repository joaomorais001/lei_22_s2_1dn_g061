package app.domain.model;

import java.io.Serializable;
import java.util.regex.Pattern;

import static java.lang.Integer.valueOf;


public class Vaccine implements Serializable {
    private String Code;
    private String Description;
    private String VaccineTechnology;
    private String Brand;
    private String AgeGroup;
    private String DoseNumber;
    private String Dosage;
    private String TimeBetweenDoses;
    private String VaccineType;

    public  Vaccine(String Brand,String VaccineType,String Code, String Description,  String VaccineTechnology,String DoseNumber,String Dosage,String TimeBetweenDoses, String AgeGroup   ) {
        verifyBrand(Brand);
        verifyCode(Code);
        verifyDescription(Description);
        verifyAgeGroup(AgeGroup);

        this.Brand = Brand;
        this.VaccineType = VaccineType;
        this.Code = Code;
        this.Description = Description;
        this.VaccineTechnology=VaccineTechnology;
        this.DoseNumber = DoseNumber;
        this.Dosage = Dosage;
        this.TimeBetweenDoses = TimeBetweenDoses;
        this.AgeGroup=AgeGroup;

    }

    @Override
    // A method that returns a string representation of the object.
    public String toString() {
        return String.format("***** Vaccine Info *****\n\nBrand: %s\n Vaccine Type: %s\n Code: %s\n Description: %s\n Vaccine Technology: %s\n\n***** Administration Process *****\n\n Dose Number: %s\n Dosage: %s\n Time Betweem Doses: %s\n Age Group: %s\n",Brand,VaccineType,Code,Description,VaccineTechnology,DoseNumber,Dosage,TimeBetweenDoses,AgeGroup);
    }
    /**
     * This function checks to see if the brand of the vaccine is null or has more than 20 characters. If it is null or has
     * more than 20 characters, it throws an exception
     *
     * @param Brand The brand of the vaccine.
     */
    public static void verifyBrand(String Brand) {
        if (Brand == null) {
            throw new IllegalArgumentException("Vaccine Brand Cannot be Null.");
        }
        if (Brand.length() > 20) {
            throw new IllegalArgumentException("Vaccine Brand Cannot have more than 20 characters.");
        }
    }
    public static void verifyVaccineType(String VaccineType){
        if (VaccineType == null) {
            throw new IllegalArgumentException("Vaccine Brand Cannot be Null.");
        }
        if (VaccineType.length() > 20) {
            throw new IllegalArgumentException("Vaccine Brand Cannot have more than 20 characters.");
        }
    }
    /**
     * The function verifies that the description is not empty, blank, or exceeds 150 characters
     *
     * @param Description The description of the vaccine.
     */
    public static void verifyDescription(String Description) {
        if (Description. isEmpty()) {
            throw new IllegalArgumentException("Vaccine Description can't be Empty.");
        }
        if (Description. isBlank()) {
            throw new IllegalArgumentException("Vaccine Description can't be Blank.");
        }
        if (Description.length() > 150 ) {
            throw new IllegalArgumentException("Vaccine Description can't Exceed 150 Characters.");
        }
    }
    /**
     * If the code doesn't match the regex, throw an exception.
     *
     * @param Code The code to be verified.
     */
    public static void verifyCode(String Code) {
        String codeRegex = "^[a-zA-Z0-9]{5}$";
        Pattern pat = Pattern.compile(codeRegex);
        if (!pat.matcher(Code).matches())
            throw new IllegalArgumentException("Invalid Code!");
    }
    /**
     * This function verifies that the age group is not null.
     *
     * @param AgeGroup The age group of the person.
     */
    public static int[] verifyAgeGroup(String AgeGroup) {
        if (AgeGroup == null) {
            throw new IllegalArgumentException("Age Group Can´t be Null.");
        }
        String[] AgeInt=AgeGroup.split("-");
        int[]AgeInterval = new int[2];
        AgeInterval[0]=Integer.parseInt(String.valueOf(valueOf(AgeInt[0])));
        AgeInterval[1]=Integer.parseInt(String.valueOf(valueOf(AgeInt[1])));
        if (AgeInterval == null){
            throw new IllegalArgumentException("Age Group Invalid, please type it in the following way :XX-XX");}

        return AgeInterval;
    }

    /**
     * This function verifies that the Dose Number is not null or empty
     *
     * @param DoseNumber The number of the dose.
     */
    public static void verifyDoseNumber(String DoseNumber){
        if (DoseNumber == null)
            throw new IllegalArgumentException("Dose Number Can´t be Null.");
        if (DoseNumber.isEmpty())
            throw new IllegalArgumentException("Dose Number Can´t be Empty.");
        int Dose= Integer.parseInt(String.valueOf(DoseNumber));
        if (Dose<=0 || Dose>10){
            throw new IllegalArgumentException("Dose Number Invalid.");
        }
    }
    /**
     * The function verifies that the dosage is not null or empty
     *
     * @param Dosage The dosage of the medicine.
     */
    public static void verifyDosage(String Dosage){
        if (Dosage == null) {
            throw new IllegalArgumentException("Dosage can´t be null.");
        }
        if (Dosage.isEmpty())
            throw new IllegalArgumentException("Dosage can´t be Empty.");
        int Dose= Integer.parseInt(String.valueOf(Dosage));
        if (Dose<=0 || Dose>1000){
            throw new IllegalArgumentException("Invalid Dosage .");
        }
    }
    /**
     * This function checks to see if the Time Between Doses is null or empty. If it is, it throws an exception
     *
     * @param TimeBetweenDoses The time between doses.
     */
    public static void verifyTimeBetweenDoses(String TimeBetweenDoses){
        if (TimeBetweenDoses == null) {
            throw new IllegalArgumentException("Time Between Doses Cannot be Null.");
        }
        if (TimeBetweenDoses.isEmpty())
            throw new IllegalArgumentException("Time Between Doses Can't be Empty.");
        int time= Integer.parseInt(String.valueOf(TimeBetweenDoses));
        if (time<=0||time>1000) {
            throw new IllegalArgumentException("Time Between Doses Invalid.");
        }
    }
    /**
     * This function returns the code of the country
     *
     * @return The code is being returned.
     */
    public String getCode() {
        return Code;
    }
    /**
     * This function sets the value of the Code variable to the value of the Code parameter, but only if the value of the
     * Code parameter is valid.
     *
     * @param Code The code to be verified.
     */
    public void setCode(String Code) {
        verifyCode(Code);
        this.Code = Code;
    }
    /**
     * This function returns the description of the object
     *
     * @return The description of the item.
     */
    public String getDescription() {
        return Description;
    }
    /**
     * > This function sets the description of the object
     *
     * @param Description The description of the item.
    /**
     * This function returns the brand of the car
     *
     * @return The brand of the car.
     */

    public void setDescription(String Description) {
        verifyDescription(Description);
        this.Description = Description;
    }
    /**
     * This function returns the brand of the car
     *
     * @return The brand of the car.
     */
    public String getBrand() {
        return Brand;
    }
    /**
     * This function sets the brand of the car
     *
     * @param Brand The brand of the car.
     */
    public void setBrand(String Brand) {
        verifyBrand(Brand);
        this.Brand = Brand;
    }
    /**
     * This function returns the vaccine technology
     *
     * @return The VaccineTechnology variable is being returned.
     */
    public String getVaccineTechnology() {
        return VaccineTechnology;
    }
    /**
     * This function sets the value of the VaccineTechnology variable to the value of the VaccineTechnology parameter
     *
     * @param VaccineTechnology The vaccine technology used to produce the vaccine.
     */
    public void setVaccineTechnology(String VaccineTechnology) {
        this.VaccineTechnology = VaccineTechnology;
    }
    /**
     * This function returns the age group of the person
     *
     * @return The age group of the person.
     */
    public String getAgeGroup(){
        return AgeGroup;
    }
    /**
     * The function verifies that the age group is valid, and if it is, it sets the age group to the value passed in
     *
     * @param AgeGroup The age group of the user.
     */
    public void setAgeGroup(String AgeGroup){
        verifyAgeGroup(AgeGroup);
        this.AgeGroup = AgeGroup;
    }
    /**
     * This function returns the dose number of the vaccine
     *
     * @return The DoseNumber is being returned.
     */
    public String getDoseNumber(){
        return DoseNumber;
    }
    /**
     * This function sets the DoseNumber variable to the value passed in as a parameter
     *
     * @param DoseNumber The number of the dose.
     */
    public void setDoseNumber (String DoseNumber){
        verifyDoseNumber(DoseNumber);
        this.DoseNumber = DoseNumber;
    }
    /**
     * This function returns the dosage of the drug
     *
     * @return The dosage of the medication.
     */
    public String getDosage (){
        return Dosage;
    }
    /**
     * This function sets the dosage of the medicine
     *
     * @param Dosage The dosage of the medication.
     */
    public void setDosage(String Dosage){
        verifyDosage(Dosage);
        this.Dosage = Dosage;
    }
    /**
     * This function returns the time between doses
     *
     * @return The time between doses.
     */
    public String getTimeBetweenDoses (){
        return TimeBetweenDoses;
    }
    /**
     * This function sets the time between doses of a medication
     *
     * @param TimeBetweenDoses The time between doses in hours.
     */
    public void setTimeBetweenDoses(String TimeBetweenDoses){
        verifyTimeBetweenDoses(TimeBetweenDoses);
        this.TimeBetweenDoses = TimeBetweenDoses;
    }
    public String getVaccineType(){return VaccineType;}
    public void setVaccineType (){
        verifyVaccineType(VaccineType);
        this.VaccineType= VaccineType;}


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vaccine)) return false;
        Vaccine vaccine = (Vaccine) o;
        return Brand.equals(vaccine.Brand) &&
                Code.equals(vaccine.Code) &&
                Description == (vaccine.Description) &&
                VaccineTechnology == (vaccine.VaccineTechnology) &&
                DoseNumber == (vaccine.DoseNumber) &&
                Dosage.equals(vaccine.Dosage) &&
                TimeBetweenDoses.equals(vaccine.TimeBetweenDoses)&&
                AgeGroup.equals(vaccine.AgeGroup)&&
                VaccineType.equals(vaccine.VaccineType);
    }
}
