package app.domain.model;

import java.util.Arrays;

public class Algorithms {

    /**
     * > The function iterates through the array, keeping track of the maximum sum of the subarray ending at the current
     * index, and the maximum sum of the subarray seen so far
     *
     * @param seq the sequence of integers
     * @return The subarray with the largest sum.
     */
    public static int[] BenchmarkAlgorithm(int[] seq) {
        int maxSoFar = 0;
        int maxEndingHere = 0;
        int startMaxSoFar = 0;
        int endMaxSoFar = 0;
        int startMaxEndingHere = 0;

        for(int i = 0; i < seq.length; ++i) {
            int elem = seq[i];
            int endMaxEndingHere = i + 1;
            if (maxEndingHere + elem < 0) {
                maxEndingHere = 0;
                startMaxEndingHere = i + 1;
            } else {
                maxEndingHere += elem;
            }

            if (maxSoFar < maxEndingHere) {
                maxSoFar = maxEndingHere;
                startMaxSoFar = startMaxEndingHere;
                endMaxSoFar = endMaxEndingHere;
            }
        }
        return Arrays.copyOfRange(seq, startMaxSoFar, endMaxSoFar);
    }

    /**
     * We keep track of the maximum sum we've seen so far, and the maximum sum we've seen ending at each index.
     *
     * The maximum sum ending at each index is either the value at that index, or the sum of the value at that index and
     * the maximum sum ending at the previous index.
     *
     * We keep track of the start and end indices of the maximum sum we've seen so far, and the start index of the maximum
     * sum ending at each index.
     *
     * The start index of the maximum sum ending at each index is either that index, or the start index of the maximum sum
     * ending at the previous index.
     *
     * The time complexity is O(n) and the space complexity is O(1)
     *
     * @param seq The array of integers that we are trying to find the maximum subarray of.
     * @return The maximum subarray of the given array.
     */
    public static int[] BruteForceAlgorithm(int[] seq) {
        if (seq.length <= 1) {
            return seq;
        }

        int maxSoFar = Integer.MIN_VALUE;

        int maxEndingHere = 0;

        int start = 0, end = 0;

        int beg = 0;

        for (int i = 0; i < seq.length; i++) {
            maxEndingHere = maxEndingHere + seq[i];

            if (maxEndingHere < seq[i]) {
                maxEndingHere = seq[i];
                beg = i;
            }

            if (maxSoFar < maxEndingHere) {
                maxSoFar = maxEndingHere;
                start = beg;
                end = i;
            }
        }
        return Arrays.copyOfRange(seq, start, end + 1);
    }
}
