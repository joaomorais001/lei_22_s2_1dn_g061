package app.domain.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class CounterReport {

    private String vacName;

    private int totalNumberOfVaccinations;




    public CounterReport(String vacName, int totalNumberOfVaccinations) {

        this.vacName = vacName;
        this.totalNumberOfVaccinations = totalNumberOfVaccinations;
    }


    public String getVacName() {
        return vacName;
    }

    public void setVacName(String vacName) {
        this.vacName = vacName;
    }


    public int getTotalNumberOfVaccinations() {
        return totalNumberOfVaccinations;
    }

    public void setTotalNumberOfVaccinations(int totalNumberOfVaccinations) {
        this.totalNumberOfVaccinations = totalNumberOfVaccinations;
    }
    @Override
    public String toString() {
        LocalDate today = LocalDate.now();

        return "\n--Total Number of Vaccinations per day--\n" + "Vaccination Center Name= " + vacName + "\n" + "Date= " +  today + "\n" + "Total Number of Vaccinations= " +  totalNumberOfVaccinations + "\n";
    }

}
