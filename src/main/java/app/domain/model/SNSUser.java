package app.domain.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SNSUser implements Serializable {

    private String name;
    private String email;
    private int CCNumber;
    private int PhoneNumber;
    private int SNSNumber;
    private String sex;
    private Date birthdate;
    private String address;
    private UserVaccineRecord userVaccinerecord;

    private static int CCDigits = 8;

    private static int SNSDigits = 9;

    private static int PhoneDigits = 9;

    private static int Max = 60;

    public SNSUser(String email, String address, int PhoneNumber, int SNSNumber, String name, int CCNumber,
            Date birthdate, String sex) {
        if (sex.equals("")) {
            setSNSUser(email, address, PhoneNumber, SNSNumber, name, CCNumber, birthdate, "n/a");
        } else {
            setSNSUser(email, address, PhoneNumber, SNSNumber, name, CCNumber, birthdate, sex);
        }
    }




    /**
     * This function sets the user's name, email, phone number, credit card number,
     * SNS number, birthdate, address, and sex
     *
     * @param email       The email of the user
     * @param address     The address of the user
     * @param PhoneNumber The phone number of the user.
     * @param SNSNumber   The user's social security number.
     * @param name        The name of the user
     * @param CCNumber    Credit Card Number
     * @param birthdate   Date
     * @param sex         String
     */
    public void setSNSUser(String email, String address, int PhoneNumber, int SNSNumber, String name, int CCNumber,
            Date birthdate, String sex) {

        validateSNSNumber(SNSNumber);
        validateAddress(address);
        validatePhoneNumber(PhoneNumber);
        validateName(name);
        validateCCNumber(CCNumber);

        this.name = name;
        this.email = email;
        this.PhoneNumber = PhoneNumber;
        this.CCNumber = CCNumber;
        this.SNSNumber = SNSNumber;
        this.birthdate = birthdate;
        this.address = address;
        this.sex = sex;
    }

    /**
     * The function validates the address by checking if the address is null, if the
     * address is greater than the maximum
     * length, and if the address is empty
     *
     * @param address The address of the user.
     */
    public static void validateAddress(String address) {
        if (address == null || address.length() > Max || address.equals(""))
            throw new IllegalArgumentException("Address Invalid!!");
    }

    /**
     * The function validates the phone number by checking if the length of the
     * phone number is equal to the number of
     * digits in a phone number
     *
     * @param PhoneNumber The phone number to be validated.
     */
    public static void validatePhoneNumber(int PhoneNumber) {
        if (String.valueOf(PhoneNumber).length() != PhoneDigits)
            throw new IllegalArgumentException("Phone number Invalid!!");
    }

    /**
     * If the length of the string representation of the SNSNumber is not equal to
     * the SNSDigits constant, throw an
     * IllegalArgumentException.
     *
     * @param SNSNumber The SNS number you want to validate.
     */
    public static void validateSNSNumber(int SNSNumber) {
        if (String.valueOf(SNSNumber).length() != SNSDigits)
            throw new IllegalArgumentException("SNSNumber Invalid!!");
    }

    /**
     * If the name is null, or if the name is longer than the maximum length, or if
     * the name is empty, then throw an
     * IllegalArgumentException.
     *
     * @param name The name of the person.
     */
    public static void validateName(String name) {
        if (name == null || (name.length() > Max) || name.equals(""))
            throw new IllegalArgumentException("Name Invalid!!");
    }

    /**
     * If the length of the string representation of the CCNumber is not equal to
     * the number of digits in a credit card,
     * throw an IllegalArgumentException.
     *
     * @param CCNumber The credit card number to be validated.
     */
    public static void validateCCNumber(int CCNumber) {
        if (String.valueOf(CCNumber).length() != CCDigits)
            throw new IllegalArgumentException("CC Invalid!!");
    }

    /**
     * This function returns the name of the person.
     *
     * @return The name of the person.
     */
    public String getName() {
        return name;
    }

    /**
     * This function returns the email of the user
     *
     * @return The email address of the user.
     */
    public String getEmail() {
        return email;
    }

    /**
     * This function returns the phone number of the person
     *
     * @return The phone number of the contact.
     */
    public int getPhoneNumber() {
        return PhoneNumber;
    }

    /**
     * This function returns the credit card number
     *
     * @return The CCNumber
     */
    public int getCCNumber() {
        return CCNumber;
    }

    /**
     * This function returns the SNSNumber of the user
     *
     * @return The SNSNumber is being returned.
     */
    public int getSNSNumber() {
        return SNSNumber;
    }

    /**
     * This function returns the birthdate of the person.
     *
     * @return The birthdate of the person.
     */
    public Date getBirthdate() {
        return birthdate;
    }

    /**
     * This function returns the address of the person.
     *
     * @return The address of the person.
     */
    public String getAddress() {
        return address;
    }

    /**
     * This function returns the sex of the person
     *
     * @return The sex of the person.
     */
    public String getSex() {
        return sex;
    }

    /**
     * This function sets the name of the object to the value of the parameter name.
     *
     * @param name The name of the parameter.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This function sets the email of the user
     *
     * @param email The email address of the user.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * This function sets the phone number of the user
     *
     * @param phonenumber The phone number of the user.
     */
    public void setPhoneNumber(int phonenumber) {
        this.PhoneNumber = phonenumber;
    }

    /**
     * This function sets the credit card number of the customer
     *
     * @param ccnumber The credit card number.
     */
    public void setCCNumber(int ccnumber) {
        this.CCNumber = ccnumber;
    }

    /**
     * This function sets the SNSNumber of the user
     *
     * @param snsnumber The number of SNS messages to send.
     */
    public void setSNSNumber(int snsnumber) {
        this.SNSNumber = snsnumber;
    }

    /**
     * The function takes a string as input, parses it into a date object, and then
     * sets the birthdate attribute of the
     * object to the parsed date
     *
     * @param birthday The birthday of the user in the format dd-MM-yyyy
     */
    public void setBirthdate(String birthday) throws ParseException {

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date birthdate = df.parse(birthday);
        this.birthdate = birthdate;
    }

    /**
     * This function sets the address of the object to the address passed in as a
     * parameter
     *
     * @param address The address of the location you want to search for.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * This function sets the sex of the person
     *
     * @param Sex The sex of the person.
     */
    public void setSex(String Sex) {
        this.sex = Sex;
    }

    /**
     * The toString() method returns a string representation of the object
     *
     * @return The toString method is being returned.
     */
    @Override
    public String toString() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = dateFormat.format(birthdate);
        return "\n--SNS User--\n" + "Name= " + name + "\n" + "Email= " + email + "\n" + "CC= " + CCNumber + "\n"
                + "Phonenumber= " + PhoneNumber + "\n" + "SNSNumber= " + SNSNumber + "\n" + "Sex= " + sex + "\n"
                + "Birthdate= " +  strDate + "\n";
    }

    /**
     * The function checks if the object passed in is an instance of the SNSUser
     * class, and if it is, it checks if the
     * name, email, CCNumber, PhoneNumber, SNSNumber, sex, and birthdate of the
     * object passed in are equal to the name,
     * email, CCNumber, PhoneNumber, SNSNumber, sex, and birthdate of the object
     * that called the function
     *
     * @param o The object to compare this instance with.
     * @return The hashcode of the object.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof SNSUser))
            return false;
        SNSUser user = (SNSUser) o;
        return name.equals(user.name) &&
                email.equals(user.email) &&
                CCNumber == (user.CCNumber) &&
                PhoneNumber == (user.PhoneNumber) &&
                SNSNumber == (user.SNSNumber) &&
                sex.equals(user.sex) &&
                birthdate.equals(user.birthdate);
    }

    public UserVaccineRecord getUserVaccineRecord(){
        return this.userVaccinerecord;
    }
}
