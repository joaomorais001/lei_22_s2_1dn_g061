package app.domain.model;

import org.apache.commons.lang3.time.StopWatch;
import pt.isep.lei.esoft.auth.mappers.dto.UserDTO;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BubbleSort {

    /**
     * It takes a list of users, and returns a list of users sorted by name
     *
     * @param user The list of users to be sorted.
     * @return A list of users sorted by name.
     */
    public List<UserDTO> sortByName(List<UserDTO> user) {
        for (int i = 0; i < user.size(); i++) {
            for (int j = 0; j < user.size() - 1 - i; j++) {
                if (user.get(j).getName().compareToIgnoreCase(user.get(j + 1).getName()) > 0) {
                    UserDTO aux = user.get(j);
                    user.set(j, user.get(j + 1));
                    user.set(j + 1, aux);
                }
            }
        }
        return user;
    }
    /**
     * It takes a 2D array of data, and returns it sorted by the position "timechosen" in each row, numerically
     *
     * @param listdata The list of users to be sorted.
     * @return A 2D numerically sorted.
     */
    public String[][] sortByTime(String[][] listdata,int timechosen) throws ParseException {
        StopWatch watch = new StopWatch();
        watch.start();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        String[] aux;
        for (int i = 0; i < listdata.length; i++) {
            for (int j = 0; j < listdata.length - 1 - i; j++) {
                if (df.parse(listdata[j][timechosen]).after(df.parse(listdata[j+1][timechosen]))){
                    aux=listdata[j];
                    listdata[j]=listdata[j+1];
                    listdata[j+1]=aux;
                }
            }
        }
        watch.stop();
        System.out.println("Elapsed Time in mls: "+ watch.getTime(TimeUnit.MILLISECONDS));

        return listdata;
    }

}
