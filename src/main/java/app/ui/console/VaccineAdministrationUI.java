package app.ui.console;

import app.controller.VaccinceSelectionController;
import app.domain.model.UserVaccineRecord;
import app.domain.model.VacCenter;
import app.domain.shared.sendSMSMessage;
import app.ui.console.utils.Utils;

import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class VaccineAdministrationUI implements Runnable{


    private final VaccinceSelectionController controller;

    public VaccineAdministrationUI(){controller=new VaccinceSelectionController();}

    public void run(){
        DateFormat dtf = new SimpleDateFormat("HH:mm");
        LocalDate Date = LocalDate.now();
        Date AdministrationTime = new Date();
        LocalDateTime LeavingTime;
        String VaccineType;
        int SNSNumber;
        boolean val;
        int option;
        String confirmation;
        VacCenter center;

        do {
            center =Utils.chooseVacCenter();
            SNSNumber = controller.VaccineSelectionSNSN(center);
            VaccineType = controller.VaccineSelectionVacType();
            option = Utils.showAndSelectIndex(controller.ShowVaccineOptions(VaccineType),"Available Vaccines");
            LeavingTime = LocalDateTime.now().plusMinutes(1);
            System.out.println("Vaccination Suceccfully Registered");

            Timer timer = new Timer();
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    try {
                        sendSMSMessage.sendSMSRecoveryRoom();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            };

            Calendar date = Calendar.getInstance();
            date.set(Calendar.HOUR_OF_DAY, LeavingTime.getHour());
            date.set(Calendar.MINUTE, LeavingTime.getMinute());
            date.set(Calendar.SECOND, LeavingTime.getSecond());
            timer.schedule(task, date.getTime());

            confirmation = Utils.readLineFromConsole("\n Is the information correct? \n (Type Yes to confirm)");

        } while (!Objects.equals(confirmation, "yes") && !Objects.equals(confirmation, "Yes"));

        LocalDateTime localDateTime = LocalDateTime.now();
        center.addDepartureRecord(localDateTime.plus(30, ChronoUnit.MINUTES), SNSNumber);

        UserVaccineRecord userVaccineRecord = new UserVaccineRecord(SNSNumber,controller.ShowVaccineOptions(VaccineType).get(option).getCode(),controller.ShowVaccineOptions(VaccineType).get(option).getBrand(),controller.ShowVaccineOptions(VaccineType).get(option).getDoseNumber(),controller.ShowVaccineOptions(VaccineType).get(option).getDosage(),controller.ShowVaccineOptions(VaccineType).get(option).getVaccineType(),Date);
        controller.ShowVaccineOptions(VaccineType).clear();
    }

}
