package app.ui.console;

import app.controller.RegisterSNSUserController;
import app.domain.model.SNSUser;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.AuthFacade;
import pt.isep.lei.esoft.auth.domain.model.Email;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class RegisterSNSUserUI implements Runnable {

    private final RegisterSNSUserController controller;
    private final AuthFacade authFacade;
    boolean val;

    public RegisterSNSUserUI() {
        controller = new RegisterSNSUserController();
        authFacade = new AuthFacade();
    }

    /**
     * It reads the information from the user and validates it, if it's correct it creates a new SNSUser and registers it
     */

    public void run() {
        int SNSNumber;
        int CCNumber;
        String name;
        Date birthday;
        int PhoneNumber;
        String Email;
        String Sex;
        String Address;
        String option;


        do {
            System.out.println("\nRegister SNS User:");
            do {
                name = Utils.readLineFromConsole("Write Name: ");
                val = true;
                try {
                    SNSUser.validateName(name);
                } catch (Exception e) {
                    System.out.println("Invalid Name!");
                    val = false;
                }
            } while (!val);

            do {
                SNSNumber = Utils.readIntegerFromConsole("Insert SNS Number: ");
                val = true;
                try {
                    SNSUser.validateSNSNumber(SNSNumber);
                } catch (Exception e) {
                    System.out.println("Invalid SNSNumber!");
                    val = false;
                }
            } while (!val);

            do {
                CCNumber = Utils.readIntegerFromConsole("Insert CC Number: ");
                val = true;
                try {
                    SNSUser.validateCCNumber(CCNumber);
                } catch (Exception e) {
                    System.out.println("Invalid CCNumber!");
                    val = false;
                }
            } while (!val);

            do {
                Address = Utils.readLineFromConsole("Write Address: ");
                val = true;
                try {
                    SNSUser.validateAddress(Address);
                } catch (Exception e) {
                    System.out.println("Invalid Address!");
                    val = false;
                }
            } while (!val);

            do {
                PhoneNumber = Utils.readIntegerFromConsole("Insert Phone Number: ");
                val = true;
                try {
                    SNSUser.validatePhoneNumber(PhoneNumber);
                } catch (Exception e) {
                    System.out.println("Invalid Phone Number!");
                    val = false;
                }
            } while (!val);


            birthday = Utils.readDateFromConsole("Write Birthdate (dd-mm-yyyy): ");
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String strDate = dateFormat.format(birthday);

            do {
                Email = Utils.readLineFromConsole("Write Email:");
                val = true;
                try {
                    Email mail = new Email(Email);
                } catch (IllegalArgumentException e) {
                    System.out.println("Invalid email!");
                    val = false;
                }
            } while (!val);

            Sex = Utils.readLineFromConsole("Insert Sex: ");

            displayInformation(SNSNumber, name, CCNumber, Address, strDate, PhoneNumber, Email, Sex);

            option = Utils.readLineFromConsole("\n Is the information correct? \n (Type Yes to confirm)");
        } while (!Objects.equals(option, "yes") && !Objects.equals(option, "Yes"));

        SNSUser snsuser = new SNSUser(Email, Address, PhoneNumber, SNSNumber, name, CCNumber, birthday, Sex);

        boolean success = controller.saveSNSUser(snsuser);
        if (success) {
            System.out.println("The SNS User was registered successfully!");
        } else {
            System.out.println("The operation couldn't be completed!");
        }

    }


    /**
     * It displays the information of a user
     *
     * @param snsNumber   The user's SNS number.
     * @param name        The name of the user
     * @param CCNumber    The credit card number
     * @param Address     The address of the user.
     * @param strDate     The date of birth of the user.
     * @param PhoneNumber The phone number of the user.
     * @param Email       The email of the user
     * @param Sex         M or F
     */
    public void displayInformation(int snsNumber, String name, int CCNumber, String Address, String strDate, int PhoneNumber, String Email, String Sex) {
        String info = String.format("\n *** User info *** \n SNS Number: %d\n Name: %s\n CC Number: %d\n Address: %s\n PhoneNumber: %d\n Email: %s\n Sex: %s\n Birthday: %s", snsNumber, name, CCNumber, Address, PhoneNumber, Email, Sex, strDate);
        Utils.printToConsole(info);
    }
}

