package app.ui.console;

import app.controller.ListRoleController;
import app.controller.RegisterEmployeeController;
import app.domain.model.BubbleSort;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.mappers.dto.UserDTO;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.util.ArrayList;
import java.util.List;

public class ListEmployeeUI implements Runnable {

    private ListRoleController controller;
    private BubbleSort bubbleSort;

    private RegisterEmployeeController controller1;

    public ListEmployeeUI() {
        controller = new ListRoleController();
        controller1 = new RegisterEmployeeController();
    }



    /**
     * This function is used to display the list of users with a specific role
     */
    public void run() {
        System.out.println(controller1.getEmployees());
        bubbleSort = new BubbleSort();
        int opt = 0;
        List<UserRoleDTO> rolesList = updateList(getRolesList());
        do {
            showUserRoles(rolesList);
            opt = Utils.readIntegerFromConsole("Choose Role:");
        } while (opt < 0 || opt > controller.getRolesList().size() - 1);

        String role = chooseUserRole(rolesList, opt).getId();
        List<UserDTO> lista = bubbleSort.sortByName(controller.getUsersWithRoles(role));
        System.out.println("List of "+ role);
        // Iterating through the list of employees.
        for(int i=0;i<controller.getEmployee().size();i++){
            for(int j=0;j<controller.getUsersWithRoles(role).size();j++){
            if(controller.getEmployee().get(i).getEmail().equals(lista.get(j).getId())) {
                System.out.println(controller.getEmployee().get(j));
                    }
                }
            }
    }
    /**
     * > This function takes a list of UserRoleDTO objects and returns a list of UserRoleDTO objects that do not have the
     * id "ADMINISTRATOR"
     *
     * @param rolesList The list of roles that the user has.
     * @return A list of UserRoleDTO objects.
     */
    public List<UserRoleDTO> updateList(List<UserRoleDTO> rolesList){
        List<UserRoleDTO> updateList = new ArrayList<>();
        for (int i = 0; i < rolesList.size(); i++) {
            if (rolesList.get(i).getId() != "ADMINISTRATOR") {
                updateList.add(rolesList.get(i));
            }
        }
        return updateList;
    }
    /**
     * > This function returns a list of all the roles in the database
     *
     * @return A list of UserRoleDTO objects.
     */
    public List<UserRoleDTO> getRolesList() {
        return this.controller.getRolesList();
    }
    /**
     * This function prints out a list of user roles
     *
     * @param rolesList a list of UserRoleDTO objects
     */
    public void showUserRoles(List<UserRoleDTO> rolesList) {
    /**
     * This function returns the user role that the user chose from the list of roles.
     *
     * @param rolesList List of UserRoleDTO objects
     * @param opcao the option chosen by the user
     * @return The role that the user chose.
     */
        System.out.println("\nList of employee roles:\n");
        for (int i = 0; i < rolesList.size(); i++) {
            System.out.println(i + "- " + rolesList.get(i).getId());
        }
    }
    /**
     * This function returns the user role that the user chose from the list of roles.
     *
     * @param rolesList List of UserRoleDTO objects
     * @param opcao the option chosen by the user
     * @return The role that the user chose.
     */
    public UserRoleDTO chooseUserRole(List<UserRoleDTO> rolesList, int opcao) {
        return rolesList.get(opcao);
    }


}



