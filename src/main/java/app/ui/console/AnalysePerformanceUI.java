package app.ui.console;

import app.controller.AnalysePerformanceController;
import app.domain.model.DepartureRecords;
import app.domain.model.EntryRecords;
import app.domain.model.VacCenter;
import app.ui.console.utils.Utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

public class AnalysePerformanceUI implements Runnable {

    // A private variable of the class AnalysePerformanceUI.
    private AnalysePerformanceController controller;

    // A variable that is used to validate the user input.
    private boolean validation;
    // Declaring two variables of type String.
    private String date, alg;
    // A private variable of the class AnalysePerformanceUI.
    private int timeInterval;

    // A constructor of the class AnalysePerformanceUI.
    public AnalysePerformanceUI() {
        this.controller = new AnalysePerformanceController();
    }

    public void run() {

        // Getting the vaccination center of the user.
        VacCenter center = controller.getVaccinationCenter(controller.getUserEmail());

        // Getting the imported records from the database.
        controller.getImportedRecords(center);

        System.out.println("\nCenter Perfomance analysis:");

        // Asking the user to input a date and validating it.
        do {
            date = Utils.readLineFromConsole("Type the pretended date: (MM/dd/yyyy)");
            try {
                Utils.checkDateFormat(date);
                validation = true;
            } catch (Exception e) {
                validation = false;
                System.out.println(e.getMessage());
            }
        } while (!validation);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate localDate = LocalDate.parse(date, formatter);

        // Getting the entry records from a specific date.
        List<EntryRecords> entryRecords = controller.getEntryRecordsFromDate(localDate,
                controller.getEntryRecords(center));
        // Getting the departure records from a specific date.
        List<DepartureRecords> departureRecords = controller.getDepartureRecordsFromDate(localDate,
                controller.getDepartureRecords(center));

        // This code is checking if there are any records for the chosen date. If there are no records, it will print a
        // message. If there are records, it will ask the user to input a time interval and validate it. Then, it will
        // calculate the differences between the entry and departure records, get the algorithm, calculate the maximum sum
        // sublist, calculate the maximum sum, get the period and print the results.
        if (entryRecords.size() == 0 || departureRecords.size() == 0) {
            System.out.println("\nNo records for the chosen date!");

        } else {
            // This code is asking the user to input a time interval and validating it.
            do {
                timeInterval = Utils.readIntegerFromConsole("Type the pretended time interval:");
                try {
                    String timeIntervalString = String.valueOf(timeInterval);
                    Utils.checkTimeIntervalFormat(timeIntervalString);
                    validation = true;
                } catch (Exception e) {
                    validation = false;
                    System.out.println(e.getMessage());
                }
            } while (!validation);

            // This code is calculating the differences between the entry and departure records.
            int[] differences = controller.calculateDifferencesEntryDeparture(entryRecords, departureRecords,
                    timeInterval);

            // Getting the algorithm that the user wants to use.
            alg = controller.getAlgorithm();

            // Calling the method calculateMaxSumSub from the class AnalysePerformanceController and passing the algorithm
            // and the differences as parameters.
            int[] sumSub = controller.calculateMaxSumSub(alg, differences);

            // Calculating the maximum sum.
            int sum = controller.calculateMaxSum(sumSub);

            // This code is calling the method getPeriod from the class AnalysePerformanceController and passing the
            // differences
            // and the time interval as parameters.
            String[] period = controller.getPeriod(differences, timeInterval);

            System.out.println("\nDifferences: " + Arrays.toString(differences));
            System.out.println("\nMaximum Sum Sublist: " + Arrays.toString(sumSub));
            System.out.println("\nMaximum Sum: " + sum);
            System.out.println("\nPeriod: " + date + " " + period[0] + ", " + date + " " + period[1]);
        }
    }
}
