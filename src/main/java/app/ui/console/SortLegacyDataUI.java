package app.ui.console;

import app.domain.model.VacCenter;
import app.ui.console.utils.Utils;
import app.controller.SortLegacyDataController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import org.apache.commons.lang3.time.StopWatch;

public class SortLegacyDataUI implements Runnable {
    private SortLegacyDataController controller;

    public SortLegacyDataUI() {controller = new SortLegacyDataController();}

    File file;

    public void run() {
        StopWatch stopWatch = new StopWatch();
        int time = 0;
        String email = null;
        try {
            email = controller.readLoginInfoFile();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // Getting the vaccination center that the user is logged in.
        VacCenter centeratm = controller.getVaccinationCenter(email);
        // Importing the file that the user wants to sort.
        file = controller.ImportFile("");
        try {
            // Reading the file and creating a list with the information in the file.
            String[][] list = controller.ReadFileAndCreateList(file);
            // Checking if the list is empty.
            if (list == null) {
                Utils.printToConsole("File is corrupted!");
            } else {
                if (list[0].length != 8) {
                    Utils.printToConsole("File is corrupted!");
                } else {
                    Utils.printToConsole("File imported correctly\n");
                    controller.ShowList(list);
                    boolean val1;
                    String Opt = "";
                    do {
                        val1 = false;
                        Opt = Utils.readLineFromConsole("Confirm the File? (y-yes/n-no)");
                        if (Opt.equalsIgnoreCase("y") || Opt.equalsIgnoreCase("yes") || Opt.equalsIgnoreCase("n")
                                || Opt.equalsIgnoreCase("no")) {
                            val1 = true;
                        } else
                            Utils.printToConsole("Please input only : (y-yes/n-no)");
                    } while (!val1);
                    if (Opt.equalsIgnoreCase("y") || Opt.equalsIgnoreCase("yes")) {
                        int timechosen;
                        int alg;
                        boolean val;
                        int lines = controller.HowManyLines(file);
                        do {
                            timechosen = Utils.readIntegerFromConsole("Sort by: 1- Arrival time | 2 - Center Leaving Time ");
                            if(timechosen!= 1 && timechosen!=2) Utils.printToConsole("ERROR : Chose between the options provided!");
                        } while (timechosen!= 1 && timechosen!=2);
                        do{
                            alg = Utils.readIntegerFromConsole("Sort using : 1- BubbleSort | 2-InsertionSort");
                            if(alg!=1 && alg!=2) Utils.printToConsole("ERROR : Chose between the options provided!");
                        }while(alg!=1 && alg!=2);
                        String[][] newlist = new String[0][];
                        // Checking if the user chose to sort by arrival time and if he chose to use the bubble sort
                        // algorithm.
                        if (timechosen == 1 && alg == 1) {
                            newlist = controller.bubbleSort(list, controller.ArrivalDateTime);
                        }
                        // Checking if the user chose to sort by arrival time and if he chose to use the selection sort
                        // algorithm.
                        if (timechosen == 1 && alg == 2) {
                            newlist = controller.insertionsort(list, controller.ArrivalDateTime,lines);
                        }

                        // Checking if the user chose to sort by leaving time and if he chose to use the bubble sort
                        // algorithm.
                        if (timechosen == 2 && alg == 1) {
                            newlist = controller.bubbleSort(list, controller.LeavingDateTime);
                        }
                        // Checking if the user chose to sort by leaving time and if he chose to use the selection sort
                        // algorithm.
                        if (timechosen == 2 && alg == 2) {
                            newlist = controller.insertionsort(list, controller.LeavingDateTime,lines);
                        }
                        // Getting the list with the extra information that the user wants to add to the list.
                        String[][] listextrainfo = controller.getListWithExtraInfo(newlist);
                        // Showing the list with the extra information that the user wants to add to the database.
                        controller.ShowList(listextrainfo);
                        // Adding the legacy data to the database.
                        controller.addLegacyData(centeratm,listextrainfo);

                    }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
