package app.ui.console;

import app.domain.model.ScheduleVaccine;
import app.domain.model.VacCenter;
import app.domain.model.SNSUser;
import app.controller.WaitingRoomController;
import app.ui.console.utils.Utils;

import java.util.InputMismatchException;
import java.util.List;

public class WaitRoomUI implements Runnable {
    private WaitingRoomController controller;

    public WaitRoomUI() {
        controller = new WaitingRoomController();
    }

    private SNSUser snsUserarrived;

    public void run() {
        int SNSNumber;
        SNSUser userarrived = null;
        // Asking the user to choose the center working at the moment.
        VacCenter centeratm = Utils.chooseVacCenter();
        boolean val;
        // Asking the user to insert the SNSNumber and validating it.
        do {
            SNSNumber = Utils.readIntegerFromConsole("Insert SNS Number: ");
            val = true;
            try {
                SNSUser.validateSNSNumber(SNSNumber);
            } catch (InputMismatchException e) {
                System.out.println("Invalid SNSNumber! The SNSNumber should be only digits!");
            } catch (Exception e) {
                System.out.println("Invalid SNSNumber! Is this user registered?");
                val = false;
            }
        } while (!val);
        // Getting the SNSUser by the SNSNumber.
        snsUserarrived = controller.getSNSUserByNumber(SNSNumber);
        // Checking if the user has any appointment for today at the center.
        if(controller.getScheduleforSNSUserToday(snsUserarrived,centeratm).size()==0){
            System.out.println("There is no appointment for today at this center for this user!");
        }
        else   {
        // Getting the schedule for the user that has arrived at the center.
        List<ScheduleVaccine> apoitmentuser = controller.getScheduleforSNSUserToday(snsUserarrived,centeratm);

        // A for loop that iterates through the list of appointments for the user that has arrived at the center.
        for (int i = 0; i < apoitmentuser.size(); i++) {
            boolean val1;
            String Opt = "";
                System.out.println("The SNSUser schedule is : " + apoitmentuser.get(i));
                do {
                    val1 = true;
                    try {
                        Opt = Utils.readLineFromConsole("Confirm the information? (y-yes/n-no)");
                    } catch (InputMismatchException e) {
                        System.out.println("Invalid option! (y/Y or n/N)");
                    } catch (Exception e) {
                        System.out.println("Invalid option! (y/Y or n/N)");
                        val1 = false;
                    }
                } while (!val1);
                if (Opt.equalsIgnoreCase("y") || Opt.equalsIgnoreCase("yes")){
                    if (controller.addUsertoWaitRoom(apoitmentuser.get(i), snsUserarrived)) {
                        System.out.println("Sucess, please inform the user to go to the waiting room!");
                    } else
                        System.out.println("Please confirm this is the first entry for this schedule");
                } else
                    System.out.println("Please reschedule in order to change the vaccine in another time.");
            }
        }
    }
}
