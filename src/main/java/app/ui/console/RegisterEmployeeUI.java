package app.ui.console;

import java.util.ArrayList;
import java.util.List;

import app.controller.RegisterEmployeeController;
import app.domain.model.Employee;
import app.domain.model.VacCenter;
import app.ui.console.utils.Utils;
import pt.isep.lei.esoft.auth.domain.model.Email;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

public class RegisterEmployeeUI implements Runnable {

    private final RegisterEmployeeController controller;

    public RegisterEmployeeUI() {
        controller = new RegisterEmployeeController();
    }

    public void run() {

        String name;
        String email;
        String address;
        int phoneNumber;
        int CCNumber;
        VacCenter center;

        String confirmation;
        boolean validation;
        int ind;

        List<UserRoleDTO> rolesList = updateList(getRolesList());
        listUserRoles(rolesList);
        // Asking the user to choose a role for the employee.
        do {
            ind = Utils.readIntegerFromConsole("Enter the employee option:");
        } while (ind < 0 || ind >= rolesList.size());
        String role = chooseRole(rolesList, ind).getId();

        do {
            do {
                /**
                 * creates a name instance from user input
                 */
                name = Utils.readLineFromConsole("Enter the employee name:");
                validation = true;
                // Validating the name of the employee.
                try {
                    Employee.validateName(name);
                } catch (Exception e) {
                    System.out.println("Invalid Name!");
                    validation = false;
                }
            } while (!validation);
            do {
                /**
                 * creates a address instance from user input
                 */
                address = Utils.readLineFromConsole("Enter the employee address:");
                validation = true;
                // Validating the address.
                try {
                    Employee.validateAddress(address);
                } catch (Exception e) {
                    System.out.println("Invalid Address!");
                    validation = false;
                }
            } while (!validation);
            do {
                /**
                 * creates a phone number instance from user input
                 */
                phoneNumber = Utils.readIntegerFromConsole("Enter the employee phone number:");
                validation = true;
                // Validating the phone number.
                try {
                    Employee.validatePhoneNumber(phoneNumber);
                } catch (NumberFormatException e) {
                    System.out.println("Invalid Phone Number!");
                    validation = false;
                }
            } while (!validation);
            do {
                /**
                 * creates a CC number instance from user input
                 */
                CCNumber = Utils.readIntegerFromConsole("Enter the employee CC number:");
                validation = true;
                // Validating the CC number.
                try {
                    Employee.validateCCNumber(CCNumber);
                } catch (NumberFormatException e) {
                    System.out.println("Invalid CC Number!");
                    validation = false;
                }
            } while (!validation);
            do {
                /**
                 * creates a email instance from user input
                 */
                email = Utils.readLineFromConsole("Enter the employee email:");
                validation = true;
                // Validating the email.
                try {
                    Email mail = new Email(email);
                } catch (IllegalArgumentException e) {
                    System.out.println("Invalid email!");
                    validation = false;
                }
            } while (!validation);

            showData(name, address, email, phoneNumber, CCNumber);
            confirmation = Utils.readLineFromConsole("Is the inserted data right? (YES/NO)");

        } while (!confirmation.equalsIgnoreCase("yes"));

        String id = Utils.generateID();
        if (role == "CENTER COORDINATOR") {
            do {
                center = Utils.chooseVacCenter();
                showCenter(center);
                confirmation = Utils.readLineFromConsole("Is the chosen center right? (Y/N)");
            } while (!confirmation.equalsIgnoreCase("yes") && !confirmation.equalsIgnoreCase("y"));

            Employee employee = new Employee(name, email, address, phoneNumber, CCNumber, id, center);
            boolean success = controller.saveEmployee(employee, role);

            if (success) {
                System.out.println("The employee was registered successfully!");
            } else {
                System.out.println("This operation couldn't be completed!");
            }
        } else {
            Employee employee = new Employee(name, email, address, phoneNumber, CCNumber, id);
            boolean success = controller.saveEmployee(employee, role);

            if (success) {
                System.out.println("The employee was registered successfully!");
            } else {
                System.out.println("This operation couldn't be completed!");
            }
        }

    }

    /**
     * Prints the employee data for verification
     */
    public void showData(String name, String address, String email, int phoneNumber, int CCNumber) {
        String data = String.format(" Name: %s\n Address: %s\n Email: %s\n Phone Number: %d\n CC number: %d\n", name,
                address, email, phoneNumber, CCNumber);
        Utils.printToConsole(data);
    }

    public void showCenter(VacCenter center) {
        String data = String.format("\nChosen center: %s\n", center.getName());
        Utils.printToConsole(data);
    }

    /**
     * This function takes a list of UserRoleDTO objects and returns a list of
     * UserRoleDTO objects that do not have the
     * id "ADMINISTRATOR"
     *
     * @param rolesList The list of roles that the user has.
     * @return A list of UserRoleDTO objects.
     */
    public List<UserRoleDTO> updateList(List<UserRoleDTO> rolesList) {
        List<UserRoleDTO> updateList = new ArrayList<>();
        for (int i = 0; i < rolesList.size(); i++) {
            if (rolesList.get(i).getId() != "ADMINISTRATOR" && rolesList.get(i).getId() != "SNS USER") {
                updateList.add(rolesList.get(i));
            }
        }
        return updateList;
    }

    /**
     * This function prints a list of user roles
     *
     * @param rolesList a list of UserRoleDTO objects
     */
    public void listUserRoles(List<UserRoleDTO> rolesList) {
        System.out.println("\nList of employee roles:\n");
        for (int i = 0; i < rolesList.size(); i++) {
            System.out.println(i + "- " + rolesList.get(i).getId());
        }
    }

    /**
     * This function receives a list of roles and an option, and returns the role
     * that corresponds to the option.
     *
     * @param rolesList List of roles that the user has.
     * @param opcao     the option chosen by the user
     * @return The role chosen by the user.
     */
    public UserRoleDTO chooseRole(List<UserRoleDTO> rolesList, int opcao) {
        return rolesList.get(opcao);
    }

    /**
     * This function returns a list of all the roles in the database
     *
     * @return A list of UserRoleDTO objects.
     */
    public List<UserRoleDTO> getRolesList() {
        return this.controller.getRolesList();
    }
}
