package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;

import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class AdminUI implements Runnable {
    public AdminUI() {
    }

    public void run() {
        try {
            List<MenuItem> options = new ArrayList<MenuItem>();
            options.add(new MenuItem("Register vaccine", new NewVaccineUI()));
            options.add(new MenuItem("Register vaccination center", new VacCenterUI()));
            options.add(new MenuItem("Register Employees", new RegisterEmployeeUI()));
            options.add(new MenuItem("List Employees", new ListEmployeeUI()));
            options.add(new MenuItem("Load CSV File", new ImportUsersUI()));
            int option = 0;

            do {
                option = Utils.showAndSelectIndex(options, "\n\nAdmin Menu:");

                if ((option >= 0) && (option < options.size())) {
                    options.get(option).run();
                }
            } while (option != -1);
        } catch (NumberFormatException e) {
            System.out.println("\nPlease choose a valid option!");
            run();

        }
    }
}
