package app.ui.console;

import app.controller.RegisterVaccineController;
import app.domain.model.Vaccine;
import app.ui.console.utils.Utils;
import java.util.Objects;

/**
 * It's a UI class that allows the user to register a new vaccine
 */
public class NewVaccineUI implements Runnable {
    private final RegisterVaccineController controller;
    boolean val;

    public NewVaccineUI() {

        controller = new RegisterVaccineController();

    }
    /**
     * This function is used to register a new vaccine type into the system
     */
    public void run() {
        String Code;
        String VaccineType;
        String Description;
        String VaccineTechnology;
        String Brand;
        String AgeGroup;
        String DoseNumber;
        String Dosage;
        String TimeBetweenDoses;
        String Option;
        System.out.println("Register the New Vaccine Info");
        do {
            VaccineType=readVaccineType();
            do {
                Brand = readBrand();
                val = true;
                try {
                    Vaccine.verifyBrand(Brand);
                } catch (Exception e){
                    System.out.println("Invalid Brand!");
                    val = false;
                }
            }while(!val);

            do {
               Code = readCode();
               val = true;
               try {
                   Vaccine.verifyCode(Code);
               } catch (Exception e) {
                   System.out.println("Invalid Code!");
                   val = false;
               }
                }while(!val);

            do {
            Description = readDescription();
                val = true;
            try {
                Vaccine.verifyDescription(Description);
            } catch (Exception e){
                System.out.println("Invalid Description!");
                val = false;
            }
            }while(!val);
            VaccineTechnology = readVaccineTechnology();

            Option = Utils.readLineFromConsole("Confirm the information? (Y - YES/N - NO)");
        } while (!Objects.equals(Option, "y") && !Objects.equals(Option, "Y"));
        System.out.println("Now define his administration process:");
        do {
            do {
            AgeGroup =readAgeGroup();
                val = true;
            try {
                Vaccine.verifyAgeGroup(AgeGroup);
            } catch (Exception e){
                System.out.println("Invalid Age Group!");
                val = false;
            }
            }while(!val);
            do {
            DoseNumber = readDoseNumber();
                val = true;
            try {
                Vaccine.verifyDoseNumber(DoseNumber);
            } catch (Exception e){
                System.out.println("Invalid Dose Number!");
                val = false;
            }
            }while(!val);
            do {
            Dosage = readDosage();
                val = true;
            try {
                Vaccine.verifyDosage(Dosage);
            } catch (Exception e){
                System.out.println("Invalid Dosage!");
                val = false;
            }
            }while(!val);
            do {
            TimeBetweenDoses = readTimeBetweenDoses();
                val = true;
            try {
                Vaccine.verifyTimeBetweenDoses(TimeBetweenDoses);
            } catch (Exception e){
                System.out.println("Invalid Time Between Doses!");
                val = false;
            }
            }while(!val);
            displayInformation(VaccineType,Code,Description,VaccineTechnology,Brand,AgeGroup,DoseNumber,Dosage,TimeBetweenDoses);
            Option = Utils.readLineFromConsole("Confirm the information? (Y - YES/N - NO)");
        } while (!Objects.equals(Option, "y") && !Objects.equals(Option, "Y"));


        Vaccine newVaccine = new Vaccine(Brand,VaccineType,Code,Description,VaccineTechnology,DoseNumber,Dosage,TimeBetweenDoses,AgeGroup);
        boolean sucess = controller.saveVaccine(newVaccine);
        System.out.println(controller.getVaccines());
        if (sucess) {
            Utils.printToConsole("Vaccine Type Added Sucessfully Into the System. \n");
        }
        else
            Utils.printToConsole("Vaccine Registration Failed.");




    }


    public void displayInformation(String VaccineType,String Code,String Description,String VaccineTechnology,String Brand,String AgeGroup,String DoseNumber,String Dosage,String TimeBetweenDoses) {
        String info = String.format("\n *** Vaccine info *** \n Brand: %s\n Vaccine Type: %s\n Code: %s\n Vaccine Technology: %s\n Description: %s\n Dose Number: %s\n Dosage: %s\n Time Between Doses: %s\n Age Group: %s\n", Brand,VaccineType,Code,VaccineTechnology,Description,DoseNumber,Dosage,TimeBetweenDoses,AgeGroup);
        Utils.printToConsole(info);
    }
    /**
     * This function reads a line from the console and returns it.
     *
     * @return The vaccine code.
     */
    public String readCode() {
        return Utils.readLineFromConsole("Insert the Vaccine Code: ");
    }

    /**
     * It reads a line from the console and returns it
     *
     * @return The description of the vaccine.
     */
    public String readDescription() {
        return Utils.readLineFromConsole("Insert the Vaccine Description :");
    }
    /**
     * It reads a line from the console and returns it
     *
     * @return The brand of the vaccine.
     */
    public String readBrand() {
        return Utils.readLineFromConsole("Insert the Vaccine Brand :");
    }
    /**
     * This function reads the vaccine technology from the console
     *
     * @return The Vaccine Technology
     */
    public String readVaccineTechnology() {
        int vaccinetechnology;
        String VaccineTechnology = null;
        do {
            System.out.println("Vaccine Technologies :");
            System.out.println("1.Live-attenuated vaccie");
            System.out.println("2.Inactivated vaccine");
            System.out.println("3.Subunit vaccine");
            System.out.println("4.Toxoid vaccine");
            System.out.println("5.Viral vector vaccine");
            System.out.println("6.Messenger RNA (mRNA) vaccine");
            vaccinetechnology= Utils.readIntegerFromConsole("Select the Vaccine Technology");

                   switch (vaccinetechnology) {
                       case 1:
                           VaccineTechnology = "Live-attenuated";
                           break;
                       case 2:
                           VaccineTechnology = "Inactivated";
                           break;
                       case 3:
                           VaccineTechnology = "Subunit";
                           break;
                       case 4:
                           VaccineTechnology = "Toxoid";
                           break;
                       case 5:
                           VaccineTechnology = "Viral vector";
                           break;
                       case 6:
                           VaccineTechnology = "Messenger RNA";
                           break;
                       default:
                           System.out.println("Please chose one of the given numbers");
                           break;
                   }
               }while (vaccinetechnology<=0||vaccinetechnology>6);
        return VaccineTechnology;
    }
    public String readVaccineType() {
        int vaccinetype;
        String VaccineType = null;
        do {
            System.out.println("Vaccine Types :");
            System.out.println("1.Covid-19");
            System.out.println("2.Tetanus");
            System.out.println("3.HIV");
            System.out.println("4.Hepatitis");
            vaccinetype= Utils.readIntegerFromConsole("Select the Vaccine Type");

            switch (vaccinetype) {
                case 1:
                    VaccineType = "Covid-19";
                    break;
                case 2:
                    VaccineType = "Tetanus";
                    break;
                case 3:
                    VaccineType = "HIV";
                    break;
                case 4:
                    VaccineType = "Hepatitis";
                    break;
                default:
                    System.out.println("Please chose one of the given numbers");
                    break;
            }
        }while (vaccinetype<=0||vaccinetype>4);
        return VaccineType;
    }
    /**
     * This function reads the age group from the console
     *
     * @return The age group of the user.
     */
    public String readAgeGroup(){
        return Utils.readLineFromConsole("Insert the Age Group: ");
    }
    /**
     * > This function reads a line from the console and returns it
     *
     * @return The Dose Number
     */
    public String readDoseNumber(){
        return Utils.readLineFromConsole("Insert the Dose Number: ");
    }
    /**
     * This function reads the dosage of the vaccine from the console
     *
     * @return The dosage of the vaccine.
     */
    public String readDosage(){
        return Utils.readLineFromConsole("Insert the Vaccine Dosage(mL): ");
    }
    /**
     * This function reads the time between doses from the console
     *
     * @return The time between doses.
     */
    public String readTimeBetweenDoses(){
        return Utils.readLineFromConsole("Insert the Time Between Doses (In Days): ");
    }

}
