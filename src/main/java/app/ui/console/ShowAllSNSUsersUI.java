package app.ui.console;

import app.controller.RegisterSNSUserController;

public class ShowAllSNSUsersUI implements Runnable {
    private final RegisterSNSUserController controller;

    public ShowAllSNSUsersUI() {
        controller = new RegisterSNSUserController();
    }

    public void run(){
        System.out.println("SNS Users:");
        System.out.println(controller.getSNSUsers());
    }
}
