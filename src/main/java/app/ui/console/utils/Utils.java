package app.ui.console.utils;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.SNSUser;
import app.domain.model.VacCenter;
import app.domain.store.VacCenterStore;
import app.ui.console.MainMenuUI;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Utils {

    static public String readLineFromConsole(String prompt) {
        try {
            System.out.println("\n" + prompt);

            InputStreamReader converter = new InputStreamReader(System.in);
            BufferedReader in = new BufferedReader(converter);

            return in.readLine();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public int readIntegerFromConsole(String prompt) {
        do {
            try {
                String input = readLineFromConsole(prompt);

                int value = Integer.parseInt(input);

                return value;
            } catch (NumberFormatException ex) {
                System.out.println("Please insert a number!");
            }
        } while (true);
    }

    static public double readDoubleFromConsole(String prompt) {
        do {
            try {
                String input = readLineFromConsole(prompt);

                double value = Double.parseDouble(input);

                return value;
            } catch (NumberFormatException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (true);
    }

    static public Date readDateFromConsole(String prompt) {
        do {
            try {
                String strDate = readLineFromConsole(prompt);

                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

                Date date = df.parse(strDate);

                return date;
            } catch (ParseException ex) {
                System.out.println("Please insert a valid date!");
            }
        } while (true);
    }

    static public boolean confirm(String message) {
        String input;
        do {
            input = Utils.readLineFromConsole("\n" + message + "\n");
        } while (!input.equalsIgnoreCase("s") && !input.equalsIgnoreCase("n"));

        return input.equalsIgnoreCase("s");
    }

    static public Object showAndSelectOne(List list, String header) {
        showList(list, header);
        return selectsObject(list);
    }

    static public int showAndSelectIndex(List list, String header) {
        showList(list, header);
        return selectsIndex(list);
    }

    static public void showList(List list, String header) {
        System.out.println(header);

        int index = 0;
        for (Object o : list) {
            index++;

            System.out.println(index + ". " + o.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancel");
    }

    static public Object selectsObject(List list) {
        String input;
        Integer value;
        do {
            input = Utils.readLineFromConsole("Type your option: ");
            value = Integer.valueOf(input);
        } while (value < 0 || value > list.size());

        if (value == 0) {
            return null;
        } else {
            return list.get(value - 1);
        }
    }

    static public int selectsIndex(List list) {
        String input;
        Integer value;
        do {
            input = Utils.readLineFromConsole("Type your option: ");
            value = Integer.valueOf(input);
        } while (value < 0 || value > list.size());

        return value - 1;
    }

    static public String generatePassword() {
        String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String NUMS = "1234567890";
        String all = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        int index;
        String pass = "";
        Random rnd = new Random();

        for (int i = 0; i < 2; i++) {
            index = rnd.nextInt(all.length());
            pass += all.charAt(index);
        }
        for (int i = 0; i < 3; i++) {
            index = rnd.nextInt(CHARS.length());
            pass += CHARS.charAt(index);
        }
        for (int i = 0; i < 2; i++) {
            index = rnd.nextInt(NUMS.length());
            pass += NUMS.charAt(index);
        }

        return pass;
    }

    static public String generateID() {
        String NUMS = "1234567890";

        int index;
        String id = "";
        Random rnd = new Random();

        for (int i = 0; i < 5; i++) {
            index = rnd.nextInt(NUMS.length());
            id += NUMS.charAt(index);
        }

        return id;
    }

    private static final PrintStream outToConsole = new PrintStream(new FileOutputStream(FileDescriptor.out));

    static public void printToConsole(String text) {
        outToConsole.print(text);
    }

    static public VacCenter chooseVacCenter() {
        Company company = App.getInstance().getCompany();
        VacCenterStore vacCenter = company.getVacStore();

        Integer index;
        boolean validation;
        if (vacCenter.getVacCenterlist().size() == 0) {
            System.out.println("Please confirm there are vaccine centers registered!");
            try {
                new MainMenuUI();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            System.out.println("\nList of centers:\n");

            for (int i = 0; i < vacCenter.getVacCenterlist().size(); i++) {
                System.out.println("" + i + " " + vacCenter.getVacCenterlist().get(i).getName());
            }
            do {
                index = readIntegerFromConsole("Select the center using the index: ");
                validation = true;
                try {
                    if (index < 0 || index >= vacCenter.getVacCenterlist().size() || index == null) {
                        throw new NumberFormatException("Invalid Index!");
                    }
                } catch (NumberFormatException ex) {
                    System.out.println("Invalid Index!");
                    validation = false;
                }
            } while (!validation);

            return vacCenter.getVacCenterlist().get(index);
        }
        return null;
    }

    public static boolean SNSNumberValid(String num) {
        if (num.length() != 9)
            return false;
        else
            return true;
    }

    public static List readSerialization(String fileName) {
        String serializationPath = "Serialization/"+fileName;
        List list= new ArrayList();
        try {
            File file = new File("Serialization");
            if (!file.exists())
                Files.createDirectory(Path.of(String.valueOf(file)));
            file = new File(serializationPath);
            if (!file.exists())
                Files.createFile(Path.of(String.valueOf(file)));

            FileInputStream fis = new FileInputStream(serializationPath);
            ObjectInputStream ois = new ObjectInputStream(fis);

            list = (List) ois.readObject();

            ois.close();
            fis.close();
        } catch (IOException | ClassNotFoundException ignored) {
        }

        return list;
    }

    public static boolean writeSerialization(String fileName, ArrayList list) {
        String serializationPath = "Serialization/"+fileName;
        try {
            FileOutputStream fos = new FileOutputStream(serializationPath);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(list);
            oos.close();
            fos.close();
        } catch (IOException e) {
            return false;
        }
        return true;
    }


    public static boolean checkLocalDateFormat(LocalDate date){

        if (date == null){
            throw new IllegalArgumentException("Invallid date!");
        }

        return true;
    }

    public static boolean checkDateFormat(String date) {

        if (date == null) {
            throw new IllegalArgumentException("Invallid date!");
        }

        if (date.equals("")) {
            throw new IllegalArgumentException("Date cannot be empty!");
        }
        if (date.length() != 10) {
            throw new IllegalArgumentException("Date format is invallid!");
        }
        if (date.charAt(2) != '/' && date.charAt(5) != '/') {
            throw new IllegalArgumentException("Date format is invallid!");
        }

        String[] splittedDate = date.split("/");

        String month = splittedDate[0];
        String day = splittedDate[1];
        String year = splittedDate[2];

        for (int i = 0; i < month.length(); i++) {
            if (!Character.isDigit(month.charAt(i))) {
                throw new IllegalArgumentException("The date can only contain numbers separated by '/'!");
            }
        }

        for (int i = 0; i < day.length(); i++) {
            if (!Character.isDigit(day.charAt(i))) {
                throw new IllegalArgumentException("The date can only contain numbers separated by '/'!");
            }
        }

        for (int i = 0; i < year.length(); i++) {
            if (!Character.isDigit(year.charAt(i))) {
                throw new IllegalArgumentException("The date can only contain numbers separated by '/'!");
            }
        }

        if (Integer.parseInt(month)<1 || Integer.parseInt(month)>12){
            throw new IllegalArgumentException("Invallid month!");
        }
        if (Integer.parseInt(day)<1 || Integer.parseInt(day)>31){
            throw new IllegalArgumentException("Invallid day!");
        }

        LocalDate today = LocalDate.now();
        int currentYear = today.getYear();

        if (Integer.parseInt(year)<1 || Integer.parseInt(year)> currentYear){
            throw new IllegalArgumentException("Invallid year!");
        }

        LocalDate localDate = LocalDate.of(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day));
        int days = localDate.lengthOfMonth();

        if (days<Integer.parseInt(day)){
            throw new IllegalArgumentException("The chosen day exceeds the number of days of the given month!");
        }

        return true;
    }

    public static boolean checkTimeIntervalFormat(String timeInterval) {

        if (timeInterval == null) {
            throw new IllegalArgumentException("The time interval cannot be null!");
        }
        if (timeInterval.equals("")) {
            throw new IllegalArgumentException("The time interval cannot be empty!");
        }

        for (int i = 0; i < timeInterval.length(); i++) {
            if (!Character.isDigit(timeInterval.charAt(i))) {
                throw new IllegalArgumentException("The time interval can only contain numbers!");
            }
        }

        int time = Integer.parseInt(timeInterval);

        if (time < 1 || time > 720) {
            throw new IllegalArgumentException("The chosen time interval is invallid!");
        }

        if (720 % time != 0) {
            throw new IllegalArgumentException(
                    "The division of 720 by the time interval must be 0!\nPlease choose one that follows this rule!");
        }

        return true;
    }
}