package app.ui.console;

import java.util.ArrayList;
import java.util.List;

import app.ui.console.utils.Utils;

public class CenterCoordinatorUI implements Runnable {

    public CenterCoordinatorUI() {

    }

    public void run() {
        try {
            List<MenuItem> options = new ArrayList<MenuItem>();
            options.add(new MenuItem("Analyse vaccination center performance", new AnalysePerformanceUI()));
            options.add(new MenuItem("Import Appointment Data", new SortLegacyDataUI()));
            int option = 0;

            do {
                option = Utils.showAndSelectIndex(options, "\n\nCenter Coordinator Menu:");

                if ((option >= 0) && (option < options.size())) {
                    options.get(option).run();
                }
            } while (option != -1);
        } catch (NumberFormatException e) {
            System.out.println("\nPlease choose a valid option!");
            run();

        }
    }
}
