package app.ui.console;

import app.controller.RegisterSNSUserController;
import app.domain.model.SNSUser;
import app.ui.console.utils.Utils;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImportUsersUI implements Runnable {

    private RegisterSNSUserController controller;
    boolean val;
    private String fileName;
    private File file;
    private BufferedReader csvReader;
    private String line;
    private String firstline;
    private String[] data;


    public ImportUsersUI() {
        controller = new RegisterSNSUserController();
    }

    public void run() {
        System.out.println("CSV FILE IMPORT");

        do {

                fileName = Utils.readLineFromConsole("Please insert the CSV file name (filename.csv): ");
                file = new File(fileName);
                val = true;


                if (!Files.exists(Path.of(fileName))||!fileName.endsWith(".csv"))
                {
                    System.out.println("File does not exists or its not a csv file");
                    val=false;
                }

        } while (!val);

        try {
            csvReader = new BufferedReader(new FileReader(file));
            firstline = csvReader.readLine();
            line=firstline;
            String regex = ".*\\d.*";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcherText = pattern.matcher(firstline);
            Boolean hasHeader = matcherText.matches();
            if (hasHeader == true)
            {
                data = firstline.split(",");
                createUser();
                while((line = csvReader.readLine()) != null)
                {
                    data = line.split(",");
                    createUser();
                }

                }
            else {
                while((line = csvReader.readLine()) != null)
            {
                data = line.split(";");
                createUser();
            }

            }
        } catch (Exception e) {
        }
        System.out.println("Csv file successfully uploaded");

    }

    private void createUser() throws ParseException {
        String name;
        String email;
        int CCNumber;
        int PhoneNumber;
        int SNSNumber;
        String sex;
        Date birthdate;
        String address;
        name = data[0];
        sex = data[1];
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        birthdate = df.parse(data[2].replace('/', '-'));
        address = data[3];
        PhoneNumber = Integer.parseInt(data[4]);
        email = data[5];
        SNSNumber = Integer.parseInt(data[6]);
        CCNumber = Integer.parseInt(data[7]);

        SNSUser user = new SNSUser(email, address, PhoneNumber, SNSNumber, name, CCNumber, birthdate, sex);
        controller.saveSNSUser(user);
    }

}