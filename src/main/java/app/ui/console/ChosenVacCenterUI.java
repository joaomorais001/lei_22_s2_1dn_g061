package app.ui.console;

import app.controller.VacCenterController;
import app.domain.model.VacCenter;
import app.ui.console.utils.Utils;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/*public class ChosenVacCenterUI implements Runnable{

    private VacCenter center;
    private String confirmation;

    private VacCenterController controller;

    @Override
    public void run() {

        do {
            center = Utils.chooseVacCenter();
            showData(center);
            confirmation = Utils.readLineFromConsole("Is the chosen center right? (Y/N)");
        } while (!confirmation.equalsIgnoreCase("yes") && !confirmation.equalsIgnoreCase("y"));

        controller.exportStatistics(controller.getexportStatistics());
        try {
            printList(controller.getexportStatistics());
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private void printList(Object getexportStatistics) throws FileNotFoundException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        System.out.println("Choose time interval. Format(dd/MM/yyyy)\n First date:");
        Scanner inString = new Scanner(System.in);
        String date = inString.nextLine();
        LocalDate localDate = LocalDate.parse(date, formatter);

        System.out.println("Second Date:");
        String date2 = inString.nextLine();
        LocalDate localDate2 = LocalDate.parse(date2, formatter);

        VacCenterController.exportStatistics(localDate,localDate2);
    }
    public void showData(VacCenter center) {
        String data = String.format("\nChosen center: %s\n", center.getName());
        Utils.printToConsole(data);
    }


}*/
