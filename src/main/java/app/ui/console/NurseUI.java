package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class NurseUI implements Runnable {

    public NurseUI() {

    }

    public void run() {
        try {
            List<MenuItem> options = new ArrayList<MenuItem>();
            options.add(new MenuItem("List users in the waiting room", new ListWaitingRoomUI()));
            options.add(new MenuItem("Vaccine Selection", new VaccineAdministrationUI()));
            int option = 0;
            do {
                option = Utils.showAndSelectIndex(options, "\n\nNurse Menu:");

                if ((option >= 0) && (option < options.size())) {
                    options.get(option).run();
                }
            } while (option != -1);
        } catch (NumberFormatException e) {
            System.out.println("\nPlease choose a valid option!");
            run();
        }
    }
}
