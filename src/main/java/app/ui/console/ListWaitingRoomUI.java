package app.ui.console;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import app.controller.ListWaitingRoomController;
import app.domain.model.VacCenter;
import app.mapper.dto.SNSUserDTO;
import app.ui.console.utils.Utils;

public class ListWaitingRoomUI implements Runnable {

    private ListWaitingRoomController controller;
    private String confirmation;
    private VacCenter center;

    public ListWaitingRoomUI() {
        controller = new ListWaitingRoomController();
    }

    public void run() {

        // A loop that asks the user to choose a center and then asks if the chosen
        // center is right. If the user answers
        // no, the loop will repeat.
        do {
            center = Utils.chooseVacCenter();
            showData(center);
            confirmation = Utils.readLineFromConsole("Is the chosen center right? (Y/N)");
        } while (!confirmation.equalsIgnoreCase("yes") && !confirmation.equalsIgnoreCase("y"));

        controller.createWaitingRoomList(controller.getWaitingRoom(center));
        printList(controller.getWaitingRoomList());

    }

    public List<SNSUserDTO> getWaitingRoomList() {
        return controller.getWaitingRoomList();
    }

    /**
     * It prints the list of users in the waiting room
     *
     * @param lista the list of users to be printed
     */
    private void printList(List<SNSUserDTO> lista) {

        if (lista.size() == 0) {
            System.out.println("\nThe waiting room is empty!");
        } else {
            System.out.println("\nWaiting room list:\n");

            for (SNSUserDTO user : lista) {
                System.out.println("Name: " + user.getName());
                System.out.println("SNS number: " + user.getSNSNumber());
                System.out.println("Sex: " + user.getSex());
                Date birthdate = user.getBirthdate();
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                String strDate = dateFormat.format(birthdate);
                System.out.println("Birthdate: " + strDate);
                System.out.println("Phone number: " + user.getPhoneNumber());
                System.out.println();
            }
        }
    }

    /**
     * It prints the name of the chosen center to the console
     *
     * @param center The object that is being passed to the method.
     */
    public void showData(VacCenter center) {
        String data = String.format("\nChosen center: %s\n", center.getName());
        Utils.printToConsole(data);
    }

}