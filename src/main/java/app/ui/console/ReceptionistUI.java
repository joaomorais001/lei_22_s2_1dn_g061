package app.ui.console;


import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class ReceptionistUI implements Runnable {
    public ReceptionistUI() {
    }

    public void run() {
        try {
            List<MenuItem> options = new ArrayList<MenuItem>();
            options.add(new MenuItem("Register new SNS User", new RegisterSNSUserUI()));
            options.add(new MenuItem("Consult all SNS Users", new ShowAllSNSUsersUI()));
            options.add(new MenuItem("Schedule Vaccine for SNS User", new ScheduleVaccineUI()));
            options.add(new MenuItem("Register the arrival of a SNS User",new WaitRoomUI()));
            int option = 0;
            do {
                option = Utils.showAndSelectIndex(options, "\n\nReceptionist Menu:");

                if ((option >= 0) && (option < options.size())) {
                    options.get(option).run();
                }
            }
            while (option != -1);
        } catch (NumberFormatException e) {
            System.out.println("\nPlease choose a valid option!");
            run();
        }
    }
}
