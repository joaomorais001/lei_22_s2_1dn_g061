package app.ui.console;

import app.controller.ScheduleVaccineController;
import app.domain.model.SNSUser;
import app.domain.model.ScheduleVaccine;
import app.ui.console.utils.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static app.ui.console.utils.Utils.chooseVacCenter;


public class ScheduleVaccineUI implements Runnable {

    private final ScheduleVaccineController controller;

    public ScheduleVaccineUI() {
        controller = new ScheduleVaccineController();
    }

    /**
     * This function is responsible for the scheduling of a vaccine
     */
    public void run() {

        int SNSNumber;
        String vaccinetype;
        boolean val;
        String option;
        String vaccineCenter;
        String data;
        String time;
        Date date;


        do {
            do {
                SNSNumber = Utils.readIntegerFromConsole("Insert SNS Number: ");
                val = true;
                try {
                    SNSUser.validateSNSNumber(SNSNumber);
                } catch (Exception e2) {
                    System.out.println("Invalid SNSNumber!");
                    val = false;
                }
                try {
                    controller.validateSNSUser(SNSNumber);
                } catch (Exception e) {
                    System.out.println("The user is not registered!");
                    val = false;
                }
            } while (!val);
            do {
                do {
                    System.out.println("Chose a vaccination center:");
                    vaccineCenter = displayVacCenters();
                    val = true;
                } while (!val);
                do {
                    showVaccineTypes();
                    vaccinetype = Utils.readLineFromConsole("Chose a vaccine type:");
                    val = true;
                    try {
                        controller.validateVaccinetype(vaccinetype);
                    } catch (IllegalArgumentException e) {
                        System.out.println("Invalid vaccine!");
                        val = false;
                    }
                } while (!val);

                date = Utils.readDateFromConsole("Insert Date (dd-mm-yyyy): ");
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                data = dateFormat.format(date);

                try {
                    controller.validateMaxVaccines(data, vaccineCenter);
                } catch (IllegalArgumentException e) {
                    System.out.println("\nNumber max of vaccines achived!");
                    val=false;
                }
            } while (!val);
            do {
                time = Utils.readLineFromConsole("Insert the time: (HH:MM)");
                val = true;
                try {
                    controller.validateTime(time, vaccineCenter);
                } catch (IllegalArgumentException e) {
                    System.out.println("Invalid time!");
                    val = false;
                }
            } while (!val);

            displayInformation(SNSNumber, vaccinetype, data, time, vaccineCenter);
            option = Utils.readLineFromConsole("\n Is the information correct? \n (Type Yes to confirm)");

        } while (!Objects.equals(option, "yes") && !Objects.equals(option, "Yes"));


        ScheduleVaccine newAppointment = new ScheduleVaccine(data, time, vaccinetype, vaccineCenter, SNSNumber);

        controller.saveAppointmentData(newAppointment);

        System.out.println("The vaccine was scheduled!");

    }

    /**
     * This function displays the information of the appointment
     *
     * @param SNSNumber     The SNS number of the patient
     * @param vaccinetype   The type of vaccine that the user wants to get.
     * @param data          The date of the appointment
     * @param time          The time of the appointment
     * @param vaccinecenter The name of the vaccine center
     */
    public void displayInformation(int SNSNumber, String vaccinetype, String data, String time, String vaccinecenter) {
        String info = String.format("\n *** Appointment info *** \n SNSNumber: %d\n Data: %s\n Time: %s \n Vaccine type: %s \n VaccineCenter: %s", SNSNumber, data, time, vaccinetype, vaccinecenter);
        Utils.printToConsole(info);
    }

    /**
     * The function `displayVacCenters` returns the name of the vaccination center that the user chooses
     *
     * @return The name of the vaccination center.
     */
    public String displayVacCenters() {
        return chooseVacCenter().getName();
    }

    /**
     * This function prints out all the vaccine types in the database
     */
    public void showVaccineTypes() {
        List<String> Vaccinetypes = controller.VaccineTypes();

        System.out.println("\nVaccine Types\n");
        for (String i : Vaccinetypes) {
            System.out.println(i);
        }
    }
}
