package app.ui.console;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DevTeamUI implements Runnable{

    public DevTeamUI()
    {

    }
    public void run()
    {
        System.out.println("\n");
        System.out.printf("Development Team:\n");
        System.out.printf("\t Fábio Silva - 1201942@isep.ipp.pt \n");
        System.out.printf("\t José Teixeira - 1200941@isep.ipp.pt \n");
        System.out.printf("\t João Veríssimo - 1201806X@isep.ipp.pt \n");
        System.out.printf("\t João Morais - 1211366@isep.ipp.pt \n");
        System.out.printf("\t Tiago Castro - 1201694@isep.ipp.pt \n");
        System.out.println("\n");
    }
}
