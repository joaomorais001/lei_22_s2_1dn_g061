package app.ui.gui;

import app.controller.AuthController;
import app.domain.shared.Constants;
import app.ui.JavaFX_Launcher;
import app.ui.console.*;
import app.ui.console.utils.Utils;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pt.isep.lei.esoft.auth.mappers.dto.UserRoleDTO;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class AuthGUI extends Application implements Runnable{

    private static Runnable previous;

    private AuthController ctrl;

    private int maxAttempts = 3;

    @FXML
    private TextField tf_Email;

    @FXML
    private TextField pf_Password;

    @FXML
    private Button btn_Back;

    @FXML
    private Button btn_Login;

    @FXML
    private Alert alert;

    public AuthGUI(){
        ctrl = new AuthController();
        alert = new Alert(Alert.AlertType.ERROR);
    }

    public AuthGUI(Runnable previous) {
        AuthGUI.previous = previous;
    }

    @Override
    public void run() {
        JavaFX_Launcher.myLaunch(getClass());
    }

    public void initialize() {

    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/Auth.fxml")));
        stage.setTitle("login");
        stage.setScene(new Scene(root, 700, 500));
        stage.setMaxHeight(500);
        stage.setMaxWidth(700);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }

    public void login() {
        boolean success;

        if (tf_Email.getText().isBlank() || pf_Password.getText().isBlank()) {
            alert.setContentText("Both fields must be filled.");
            alert.show();
        } else {
            maxAttempts--;
            success = ctrl.doLogin(tf_Email.getText(), pf_Password.getText());
            if (!success)
                if (maxAttempts > 0) {
                    alert.setContentText("Invalid UserId and/or Password. \n You have  " + maxAttempts + " more attempt(s).");
                    alert.show();
                } else {
                    login();
                }
            else {
                List<UserRoleDTO> roles = this.ctrl.getUserRoles();
                if ( (roles == null) || (roles.isEmpty()) ) {
                    alert.setContentText("User has not any role assigned.");
                    alert.show();
                } else {
                    UserRoleDTO role = selectsRole(roles);
                    if (!Objects.isNull(role)) {
                        List<MenuItem> rolesUI = null;
                        try {
                            rolesUI = getMenuItemForRoles();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        assert rolesUI != null;
                        this.redirectToRoleUI(rolesUI,role);
                        logout();
                        maxAttempts = 3;
                    }
                    else {
                        alert.setContentText("User did not select a role.");
                        alert.show();
                    }
                }
            }
        }
    }

    private UserRoleDTO selectsRole(List<UserRoleDTO> roles) {
        if (roles.size() == 1)
            return roles.get(0);
        else
            return (UserRoleDTO) Utils.showAndSelectOne(roles, "Select the role you want to adopt in this session:");
    }

    private List<MenuItem> getMenuItemForRoles() {
        List<MenuItem> rolesUI = new ArrayList<>();

        try {
            PrintWriter out = new PrintWriter("LoginInfo.txt");
            out.println(tf_Email.getText());
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        AdminGUI.setPrevious(this);
        ReceptionistGUI.setPrevious(this);
        NurseGUI.setPrevious(this);
        CenterCoordinatorGUI.setPrevious(this);
        SNSUserGUI.setPrevious(this);


        rolesUI.add(new MenuItem(Constants.ROLE_ADMIN, new AdminGUI()));
        rolesUI.add(new MenuItem(Constants.ROLE_NURSE, new NurseGUI()));
        rolesUI.add(new MenuItem(Constants.ROLE_RECEPTIONIST, new ReceptionistGUI()));
        rolesUI.add(new MenuItem(Constants.ROLE_CENTERCOORDINATOR, new CenterCoordinatorGUI()));
        rolesUI.add(new MenuItem(Constants.ROLE_SNSUSER, new SNSUserGUI()));


        return rolesUI;
    }


    private void redirectToRoleUI(List<MenuItem> rolesUI, UserRoleDTO role) {
        boolean found = false;
        Iterator<MenuItem> it = rolesUI.iterator();
        while (it.hasNext() && !found)
        {
            MenuItem item = it.next();
            found = item.hasDescription(role.getDescription());
            if (found)
                item.run();
            Stage stage = (Stage) btn_Login.getScene().getWindow();
            stage.close();
        }
        if (!found)
            System.out.println(("There is no UI for users with role '" + role.getDescription() + "'"));
    }

    private void logout()
    {
        ctrl.doLogout();
    }

    public void back() {
        this.logout();
        Stage stage = (Stage) btn_Back.getScene().getWindow();
        stage.close();
        previous.run();
    }
}
