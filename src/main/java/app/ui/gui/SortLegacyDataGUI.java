package app.ui.gui;

import app.controller.SortLegacyDataController;
import app.domain.model.VacCenter;
import app.ui.JavaFX_Launcher;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.util.Callback;

public class SortLegacyDataGUI extends Application implements Runnable {

    @FXML
    private Button btn_Back;

    public Label filepath=new Label();
    public Button ButtonFile = new Button();
    public RadioButton rbt1 = new RadioButton();
    public RadioButton rbt2 = new RadioButton();
    public RadioButton rbalg1 = new RadioButton();;
    public RadioButton rbalg2 = new RadioButton();;
    public static Runnable previous;
    private SortLegacyDataController us17controller;
    public File selectedfile;
    public String[][] unsortedlist;
    public String[][] sortlist ;
    public TableView<String[]> table = new TableView<>();
    ObservableList<String[]> data = FXCollections.observableArrayList();
    public SortLegacyDataGUI() {
        us17controller = new SortLegacyDataController();
    }

    public static void setPrevious(Runnable previous) {
        SortLegacyDataGUI.previous = previous;
    }
    @FXML
    private void initialize() {
        final ToggleGroup algo = new ToggleGroup();
        final ToggleGroup timefactor = new ToggleGroup();
        rbt1.setToggleGroup(timefactor);
        rbt2.setToggleGroup(timefactor);
        rbalg1.setToggleGroup(algo);
        rbalg2.setToggleGroup(algo);
        table.toBack();
        table.setOpacity(0);
    }
    @Override
    public void run() {
        JavaFX_Launcher.myLaunch(getClass());
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/SortLegacyData.fxml")));
        stage.setTitle("Sort Legacy Data");
        stage.setScene(new Scene(root, 700, 500));
        stage.setMaxHeight(500);
        stage.setMaxWidth(700);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }


    public void back() {
        Stage stage = (Stage) btn_Back.getScene().getWindow();
        stage.close();
        previous.run();
    }

    /**
     * This function is used to select a file from the user's computer and then read the file and create a list of
     * individuals
     */
    public void ButtonFile() throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        selectedfile = fileChooser.showOpenDialog(null);
        if (selectedfile != null) {
            unsortedlist = us17controller.ReadFileAndCreateList(selectedfile);
            filepath.setText(selectedfile.getPath());
        }
    }
    /**
     * It takes the unsorted list of appointments and sorts it according to the user's choice of sorting algorithm and
     * sorting criteria
     */
    public void ButtonCheck() throws ParseException, FileNotFoundException {
        if (rbt1.isSelected()&&rbalg1.isSelected()){
            sortlist = us17controller.bubbleSort(unsortedlist, us17controller.ArrivalDateTime);
        }
        if (rbt1.isSelected()&&rbalg2.isSelected()){
            sortlist = us17controller.insertionsort(unsortedlist, us17controller.ArrivalDateTime, us17controller.totallines);

        }
        if (rbt2.isSelected()&&rbalg1.isSelected()){
            sortlist = us17controller.bubbleSort(unsortedlist, us17controller.LeavingDateTime);

        }
        if (rbt2.isSelected()&&rbalg2.isSelected()){
            sortlist = us17controller.insertionsort(unsortedlist, us17controller.LeavingDateTime,us17controller.totallines);
        }
        String[][] finallist = us17controller.getListWithExtraInfo(sortlist);
        String email = us17controller.readLoginInfoFile();
        VacCenter center = us17controller.getVaccinationCenter(email);
        data.addAll(Arrays.asList(finallist));
        String[] header= {"SNSUSerNumber","SNSUser Name","VaccineName","Vaccine Description","Dose","LotNumber","ScheduledDateTime","ArrivalDateTime","NurseAdministrationDateTime","LeavingDateTime"};
        for (int i = 0; i < finallist[0].length; i++) {
            TableColumn tc = new TableColumn(header[i]);
            final int colNo = i;
            tc.setCellValueFactory(new Callback<CellDataFeatures<String[], String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(CellDataFeatures<String[], String> p) {
                    return new SimpleStringProperty((p.getValue()[colNo]));
                }
            });
            tc.setPrefWidth(90);
            table.getColumns().add(tc);
            table.setItems(data);
        }
        table.toFront();
        table.setOpacity(1.0);
        us17controller.addLegacyData(center,finallist);

    }

}
