package app.ui.gui;

import app.ui.JavaFX_Launcher;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.util.Objects;

public class MainMenuGUI extends Application implements Runnable {

    private final AuthGUI authGUI;
    @FXML
    private Button btnDevs;

    private final DevTeamGUI devTeamGUI;

    public MainMenuGUI() {
        devTeamGUI = new DevTeamGUI(this);
        authGUI = new AuthGUI(this);
    }

    @Override
    public void run() {
        JavaFX_Launcher.myLaunch(getClass());
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/MainMenu.fxml")));
        stage.setTitle("Main Menu");
        stage.setScene(new Scene(root, 700, 500));
        stage.setMaxHeight(500);
        stage.setMaxWidth(700);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }

    public void showDevelopmentTeam() {
        Stage stage = (Stage) btnDevs.getScene().getWindow();
        stage.close();
        devTeamGUI.run();
    }

    public void login() {
        Stage stage = (Stage) btnDevs.getScene().getWindow();
        stage.close();
        authGUI.run();
    }

    public void exit() {
        Platform.exit();
    }
}
