package app.ui.gui;

import app.domain.model.AnalysisResults;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.ResourceBundle;

public class PerformanceAnalysisResultsGUI implements Initializable {

    // A private variable that is used to store the results of the analysis.
    private AnalysisResults results;

    @FXML
    // Declaring a private variable called date that is of type Label.
    private Label date;

    @FXML
    // A private variable that is used to store the results of the analysis.
    private Label time;

    @FXML
    // A private variable that is used to store the results of the analysis.
    private TextArea differences;

    @FXML
    // A private variable that is used to store the results of the analysis.
    private TextArea maxSum;

    @FXML
    // Declaring a private variable called sum that is of type Label.
    private Label sum;

    @FXML
    // Declaring a private variable called period that is of type Label.
    private Label period;

    
    // A constructor that is used to initialize the results variable.
    public PerformanceAnalysisResultsGUI(AnalysisResults results) {
        this.results = results;
    }

    /**
     * It sets the text of the labels to the values of the variables in the results object
     *
     * @param url The location used to resolve relative paths for the root object, or null if the location is not known.
     * @param resourceBundle The resource bundle that was given to the FXMLLoader. This parameter will be null if the
     * control was not instantiated from an FXML file.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        
        this.date.setText(String.valueOf(this.results.getDate().format(formatter)));
        this.time.setText(String.valueOf(this.results.getTimeInterval()));
        this.differences.setText(Arrays.toString(this.results.getDifferences()));
        this.maxSum.setText(Arrays.toString(this.results.getMaximumSumSublist()));
        this.sum.setText(String.valueOf(this.results.getMaximumSum()));
        this.period.setText(String.valueOf(this.results.getDate().format(formatter) + " " + results.getPeriod()[0] + ", " + results.getDate().format(formatter) + " " + results.getPeriod()[1]));
    }

}
