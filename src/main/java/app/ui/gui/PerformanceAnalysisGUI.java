package app.ui.gui;

import app.controller.AnalysePerformanceController;
import app.domain.model.*;
import app.ui.JavaFX_Launcher;
import app.ui.console.utils.Utils;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;


public class PerformanceAnalysisGUI extends Application implements Runnable {

    @FXML
    // A variable that is used to store the button that is used to confirm the input.
    private Button confirmButton;

    @FXML
    // A variable that is used to store the button that is used to go back to the previous window.
    private Button btn_Back;


    @FXML
    // A variable that is used to store the date picker that is used to select the date.
    private DatePicker datePicker;

    @FXML
    // A variable that is used to store the text field that is used to input the time interval.
    private TextField timeInterval;

    @FXML
    // A variable that is used to store the label that is used to display the algorithm that is used to calculate the
    // maximum sum.
    private Label algorithm;

    @FXML
    // A variable that is used to store the label that is used to display the error message that is related to the date.
    private Label dateErrorMessage;

    @FXML
    // A variable that is used to store the label that is used to display the error message that is related to the time
    // interval.
    private Label timeErrorMessage;

    @FXML
    // A variable that is used to store the label that is used to display the error message that is related to the size of
    // the records.
    private Label sizeErrorMessage;

    // Used to store the previous window.
    private static Runnable previous;

    // Used to store the controller that is used to perform the performance analysis.
    private AnalysePerformanceController controller;

    // Used to store the validation of the date, time and size of the records.
    private boolean dateValidation, timeValidation, sizeValidation;
    // Used to store the algorithm that is used to calculate the maximum sum.
    private String aux, alg;

    // Creating a new instance of the controller.
    public PerformanceAnalysisGUI() {
        controller = new AnalysePerformanceController();
    }

    /**
     * It sets the previous Runnable to the one passed in
     *
     * @param previous The previous Runnable to be executed.
     */
    public static void setPrevious(Runnable previous) {
        PerformanceAnalysisGUI.previous = previous;
    }

    @Override
    // The method that is used to load the fxml file and display it.
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/PerformanceAnalysis.fxml")));
        stage.setTitle("Performance Analysis");
        stage.setScene(new Scene(root, 700, 500));
        stage.setMaxHeight(500);
        stage.setMaxWidth(700);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    // Reading the config.properties file and setting the algorithm to be used.
    public void initialize() throws IOException {

        // Reading the config.properties file and setting the algorithm to be used.
        FileReader file = new FileReader("config.properties");
        Properties prop = new Properties();
        prop.load(file);
        alg = prop.getProperty("Algorithm");

       // Checking the algorithm that is used to calculate the maximum sum.
       if(alg.equals("1")){
        aux = "Brute-Force Algorithm";
       }else{
        aux = "Benchamrk Algorithm";
       }

       // Setting the text of the label to the algorithm that is used to calculate the maximum sum.
       algorithm.setText(aux);
    }

    @Override
    // Used to launch the JavaFX application.
    public void run() {
        JavaFX_Launcher.myLaunch(getClass());
    }

    @FXML
    // This method is used to handle the button that is used to confirm the input.
    void handleConfirmButton(ActionEvent event) throws Exception {

        // Reading the email address that is stored in the login info file.
        String email = controller.readLoginInfoFile();
        // Getting the vaccination center that is associated with the email address that is stored in the login info file.
        VacCenter center = controller.getVaccinationCenter(email);

        // Getting the imported records from the database.
        controller.getImportedRecords(center);

        // Checking the format of the date.
        try {
            Utils.checkLocalDateFormat(datePicker.getValue());
            dateErrorMessage.setText("");
            dateValidation = true;
        } catch (Exception e) {
            dateErrorMessage.setText(e.getMessage());
            dateValidation = false;
        }

        // Checking the format of the time interval.
        try {
            Utils.checkTimeIntervalFormat(timeInterval.getText());
            timeErrorMessage.setText("");
            timeValidation = true;
        } catch (Exception e) {
            timeErrorMessage.setText(e.getMessage());
            timeValidation = false;
        }

        // Getting the entry records from the database that are associated with the selected date.
        List<EntryRecords> entryRecords = controller.getEntryRecordsFromDate(datePicker.getValue(),
                controller.getEntryRecords(center));
        // Getting the departure records from the database that are associated with the selected date.
        List<DepartureRecords> departureRecords = controller.getDepartureRecordsFromDate(datePicker.getValue(),
                controller.getDepartureRecords(center));

        // This is the code that is used to check if the date that is selected has records. If it does, then it will
        // calculate the maximum sum and redirect to the results page.
        if (entryRecords.size() == 0 || departureRecords.size() == 0) {
            sizeErrorMessage.setText("The selected date has no records!");
            sizeValidation = false;
        } else {
            sizeErrorMessage.setText("");
            sizeValidation = true;
        }

        // This is the code that is used to check if the date that is selected has records. If it does, then it will
        // calculate the difference, maximum sum sublist, maximum sum and the period and redirect to the results page.
        if (timeValidation == true && dateValidation == true && sizeValidation == true) {

            // Calculating the differences between the entry and departure records.
            int[] differences = controller.calculateDifferencesEntryDeparture(entryRecords, departureRecords, Integer.parseInt(timeInterval.getText()));
            // Calculating the maximum sum of the sub-array.
            int[] sumSub = controller.calculateMaxSumSub(alg, differences);
            // Calculating the maximum sum of the sub-array.
            int sum = controller.calculateMaxSum(sumSub);
            // Getting the period of the maximum sum sub-array.
            String[] period = controller.getPeriod(differences, Integer.parseInt(timeInterval.getText()));

            // Creating a new instance of the AnalysisResults class.
            AnalysisResults results = new AnalysisResults(differences, sumSub, sum, datePicker.getValue(),
                    Integer.parseInt(timeInterval.getText()), period);
                    
            // It loads the FXML file for the results page, sets the controller for the results page to the results page
            // controller, and then displays the results page.
            redirectToResults(results);
        }

    }

    /**
     * When the back button is clicked, close the current window and run the previous window.
     */
    public void back() {
        Stage stage = (Stage) btn_Back.getScene().getWindow();
        stage.close();
        previous.run();
    }

    /**
     * It loads the FXML file for the results page, sets the controller for the results page to the results page
     * controller, and then displays the results page
     *
     * @param results The results of the analysis.
     */
    private void redirectToResults(AnalysisResults results) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PerformanceAnalysisResults.fxml"));
        loader.setController(new PerformanceAnalysisResultsGUI(results));
        Parent root = (Parent) loader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Performance Analysis Results");
        stage.setScene(new Scene(root, 700, 500));
        stage.setResizable(false);
        stage.show();
    }

}
