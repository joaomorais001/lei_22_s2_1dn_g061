package app.ui.gui;

import app.ui.JavaFX_Launcher;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.util.Objects;

public class CenterCoordinatorGUI extends Application implements Runnable {

    private static Runnable previous;

    @FXML private Button btn_Back;
    @FXML private Button btn2;

    @FXML private Button btn21;
    @FXML
    private Button btn1;


    public CenterCoordinatorGUI() {

    }

    public static void setPrevious(Runnable previous) { CenterCoordinatorGUI.previous = previous; }

    @Override
    public void run() {
        JavaFX_Launcher.myLaunch(getClass());
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/CenterCoordinator.fxml")));
        stage.setTitle("Center Coordinator Menu");
        stage.setScene(new Scene(root, 700, 500));
        stage.setMaxHeight(500);
        stage.setMaxWidth(700);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();

    }

    public void analysePerformance() {
        PerformanceAnalysisGUI.setPrevious(this);
        Stage stage = (Stage) btn1.getScene().getWindow();
        stage.close();
        new PerformanceAnalysisGUI().run();
    }

    public void importData() {
        Stage stage = (Stage) btn2.getScene().getWindow();
        stage.close();
        SortLegacyDataGUI.setPrevious(this);
        new SortLegacyDataGUI().run();
    }

    public void exportData() {
        Stage stage = (Stage) btn21.getScene().getWindow();
        stage.close();
        VacinationStatisticsGUI.setPrevious(this);
        new VacinationStatisticsGUI().run();
    }

    public void back() {
        Stage stage = (Stage) btn_Back.getScene().getWindow();
        stage.close();
        previous.run();
    }
}
