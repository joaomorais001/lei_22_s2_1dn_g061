package app.ui.gui;

import app.ui.JavaFX_Launcher;
import app.ui.console.*;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.util.Objects;

public class SNSUserGUI  extends Application implements Runnable{

        private static Runnable previous;

        @FXML
        private Button btn_Back;

        public SNSUserGUI() {}

        public static void setPrevious(Runnable previous) {
            app.ui.gui.SNSUserGUI.previous = previous;
        }

        @Override
        public void run() {
            JavaFX_Launcher.myLaunch(getClass());
        }

        @Override
        public void start(Stage stage) throws Exception {
            Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/SNSUser.fxml")));
            stage.setTitle("Users Menu");
            stage.setScene(new Scene(root, 700, 500));
            stage.setMaxHeight(500);
            stage.setMaxWidth(700);
            stage.setMinHeight(500);
            stage.setMinWidth(700);
            stage.centerOnScreen();
            stage.setResizable(false);
            stage.show();
        }

        public void scheduleVaccine() {
            new ScheduleVaccineSNSUserUI().run();
        }

        public void back() {
            Stage stage = (Stage) btn_Back.getScene().getWindow();
            stage.close();
            previous.run();
        }



}
