package app.ui.gui;

import app.ui.JavaFX_Launcher;
import app.ui.console.RegisterSNSUserUI;
import app.ui.console.ScheduleVaccineUI;
import app.ui.console.ShowAllSNSUsersUI;
import app.ui.console.WaitRoomUI;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;


import java.util.Objects;

public class ReceptionistGUI extends Application implements Runnable {

    private static Runnable previous;
    @FXML private Button btn_Back;

    public ReceptionistGUI() {}

    public static void setPrevious(Runnable previous) { ReceptionistGUI.previous = previous; }

    @Override
    public void run() {
        JavaFX_Launcher.myLaunch(getClass());
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/Receptionist.fxml")));
        stage.setTitle("Receptionist Menu");
        stage.setScene(new Scene(root, 700, 500));
        stage.setMaxHeight(500);
        stage.setMaxWidth(700);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }

    public void registerClient() {
        new RegisterSNSUserUI().run();
    }

    public void consultSNSUsers() {
        new ShowAllSNSUsersUI().run();
    }

    public void schesuleVaccine() {
        new ScheduleVaccineUI().run();
    }

    public void registerArrival() {
        new WaitRoomUI().run();
    }

    public void back() {
        Stage stage = (Stage) btn_Back.getScene().getWindow();
        stage.close();
        previous.run();
    }
}