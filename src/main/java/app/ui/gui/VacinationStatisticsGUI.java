package app.ui.gui;

import app.controller.StatisticsController;
import app.controller.VacCenterController;
import app.domain.model.Company;
import app.domain.model.FullVaccinatedData;
import app.domain.model.FullyVaccinated;
import app.domain.model.VacCenter;
import app.mapper.dto.FullVaccinationDTO;
import app.ui.JavaFX_Launcher;
import app.ui.console.ListWaitingRoomUI;
import app.ui.console.MainMenuUI;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class VacinationStatisticsGUI extends Application implements Runnable{


    private static Runnable previous;
    public Label center=new Label();

    //@FXML public TableView<FullyVaccinated> vaccinationTable;
    //@FXML public TableColumn<FullyVaccinated, Integer> snsNumberColumn;
    //@FXML public TableColumn<FullyVaccinated, LocalDate> dateColumn;

    @FXML
    private Button btnBack;

    @FXML
    private ComboBox<String> vaccCenterOptions;

    private String vaccCenterOptionsString;

    @FXML
    private Stage stage;
    private List<String> vaccenters;

    @FXML
    private DatePicker firstDate;

    @FXML
    private DatePicker secondDate;


    private Button btnDevs;

    private final StatisticsController statisticsController;

    Company company;
    private VacCenter centerr;

    public Button returnbtn;
    private String centerName;
    private VacCenterController vacCenterStore;


    public VacinationStatisticsGUI(){
        statisticsController = new StatisticsController();
    }

    @FXML
    private void initialize() throws FileNotFoundException {
        centerr = statisticsController.getVaccinationCenter(statisticsController.readLoginInfoFile());
        centerName = centerr.getName();
        center.setText(centerName);
       // snsNumberColumn.setCellValueFactory(new PropertyValueFactory<FullyVaccinated, Integer>("snsNumber"));
       // dateColumn.setCellValueFactory(new PropertyValueFactory<FullyVaccinated, LocalDate>("dateOfFullVaccination"));
        // vaccinationTable.setItems(getFullyVaccinatedList());

       // try {
         //   MainMenuUI menu = new MainMenuUI();
        //} catch (IOException e) {
          //  throw new RuntimeException(e);
        //}
        //vaccenters=new ArrayList<>();
        //List<VacCenter> vacCenterList= this.vacCenterStore.getVacCenterlist();

        //for (VacCenter vacCenter : vacCenterList) {
          //  vaccenters.add(String.valueOf(vacCenter.getName()));
       // }
        //vaccCenterOptions.setItems(FXCollections.observableList(vaccenters));

    }

    public static void setPrevious(Runnable previous) { VacinationStatisticsGUI.previous = previous; }



    @Override
    public void run() {
        JavaFX_Launcher.myLaunch(getClass());
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/VacinationStatistics.fxml")));
        stage.setTitle("Vacination Statistics Menu");
        stage.setScene(new Scene(root, 700, 500));
        stage.setMaxHeight(500);
        stage.setMaxWidth(700);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }

    public void listRoom() {
        new ListWaitingRoomUI().run();
    }

    public void VaccineSelection(){
        Stage stage = (Stage) btnDevs.getScene().getWindow();
        stage.close();
        // administrationProcessGUI.run();
    }


    public void CONFIRM(ActionEvent actionEvent) throws FileNotFoundException {
        StatisticsController statisticsController = new StatisticsController(this.company);
        statisticsController.treatVaccinationData(FullVaccinatedData.fullVaccinationDTOS,FullVaccinatedData.localDate1,FullVaccinatedData.localDate2);
    }
    public ObservableList<FullyVaccinated> getFullyVaccinatedList(){
        ObservableList<FullyVaccinated> fullVaccination = FXCollections.observableArrayList();
        for (FullVaccinationDTO fullyVaccinated:FullVaccinatedData.fullVaccinationDTOS) {
            fullVaccination.add(new FullyVaccinated(Integer.toString(fullyVaccinated.snsNumber),fullyVaccinated.dateOfFullVaccination));
        }
        return fullVaccination;
    }

    public void back(ActionEvent actionEvent) throws IOException {
        new AuthGUI().run();
        Parent centerCoordinator = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("fxml/centerCoordinator.fxml")));
        Scene scene = new Scene(centerCoordinator);
        Stage stage2 = new Stage();
        stage2.setScene(scene);
        stage2.setResizable(true);
        stage = (Stage) returnbtn.getScene().getWindow();
        stage.close();
        stage2.showAndWait();

    }
}
