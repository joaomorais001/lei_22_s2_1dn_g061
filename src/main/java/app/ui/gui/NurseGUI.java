package app.ui.gui;

import app.ui.JavaFX_Launcher;
import app.ui.console.ListWaitingRoomUI;
import app.ui.console.VaccineAdministrationUI;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.util.Objects;

public class NurseGUI extends Application implements Runnable {

    private static Runnable previous;

    @FXML private Button btn_Back;
    @FXML private Button reaction;

    @FXML
    private Button btn_Administration;

    //private final AdministrationProcessGUI administrationProcessGUI;


    public NurseGUI() {
        //administrationProcessGUI = new AdministrationProcessGUI();
    }

  //  public NurseGUI() {
    //// }


    public static void setPrevious(Runnable previous) { NurseGUI.previous = previous; }

    @Override
    public void run() {
        JavaFX_Launcher.myLaunch(getClass());
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/Nurse.fxml")));
        stage.setTitle("Nurse Menu");
        stage.setScene(new Scene(root, 700, 500));
        stage.setMaxHeight(500);
        stage.setMaxWidth(700);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }

    public void listRoom() {
        new ListWaitingRoomUI().run();
    }

    public void VaccineSelection(){
        new VaccineAdministrationUI().run();

    }
    public void addAdverse(){
        ReactionGUI.setPrevious(this);
        Stage stage = (Stage) reaction.getScene().getWindow();
        stage.close();
        new ReactionGUI().run();
    }
    public void back() {
        Stage stage = (Stage) btn_Back.getScene().getWindow();
        stage.close();
        previous.run();
    }
}
