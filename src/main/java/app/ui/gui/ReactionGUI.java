package app.ui.gui;


import app.controller.UserVaccineRecordController;
import app.ui.JavaFX_Launcher;
import app.ui.console.utils.Utils;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.util.Objects;


public class ReactionGUI extends Application implements Runnable {

    @FXML
    private Button btn_Back;
    @FXML
    private Button confirm;
    @FXML public TextField snsNumber;
    @FXML public TextField Adverse;
    public static Runnable previous;
    public Label labelerror;
    private UserVaccineRecordController controller;

    public ReactionGUI() {controller = new UserVaccineRecordController(); }

    public static void setPrevious(Runnable previous) {
        ReactionGUI.previous = previous;
    }
    @Override
    public void run() {
        JavaFX_Launcher.myLaunch(getClass());
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/Reaction.fxml")));
        stage.setTitle("Adverse Reaction");
        stage.setScene(new Scene(root, 700, 500));
        stage.setMaxHeight(500);
        stage.setMaxWidth(700);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }
    public void confirm(){
        int SNSNumber = Integer.parseInt(snsNumber.getText());
        if(!Utils.SNSNumberValid(String.valueOf(SNSNumber))){
            labelerror.setText("SNSNumber not valid");
        }else{
            String reaction = Adverse.getText();

            if(controller.addReaction(reaction,SNSNumber)) {
                labelerror.setText("");
                back();
            }
        }

    }

    public void back() {
        Stage stage = (Stage) btn_Back.getScene().getWindow();
        stage.close();
        previous.run();
    }


}
