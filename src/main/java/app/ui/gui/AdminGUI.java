package app.ui.gui;

import app.ui.JavaFX_Launcher;
import app.ui.console.*;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;


import java.util.Objects;

public class AdminGUI extends Application implements Runnable {

    private static Runnable previous;

    @FXML private Button btn_Back;

    public AdminGUI() {}

    public static void setPrevious(Runnable previous) {
        AdminGUI.previous = previous;
    }

    @Override
    public void run() {
        JavaFX_Launcher.myLaunch(getClass());
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/Admin.fxml")));
        stage.setTitle("Admin Menu");
        stage.setScene(new Scene(root, 700, 500));
        stage.setMaxHeight(500);
        stage.setMaxWidth(700);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }

    public void specifyVaccinationType() {
        new NewVaccineUI().run();
    }

    public void specifyVaccinationCenter() {
        new VacCenterUI().run();
    }

    public void registerEmployee() {
        new RegisterEmployeeUI().run();
    }

    public void listEmployee() {
        new ListEmployeeUI().run();
    }

    public void loadfile() {
        new ImportUsersUI().run();
    }

    public void back() {
        Stage stage = (Stage) btn_Back.getScene().getWindow();
        stage.close();
        previous.run();
    }

}
