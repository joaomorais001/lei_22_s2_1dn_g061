package app.ui;

import java.util.ArrayList;
import java.util.List;

import app.controller.VaccineCounterController;
import app.ui.console.MainMenuUI;
import app.ui.console.utils.Utils;
import app.ui.gui.MainMenuGUI;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

// Teste
public class Main {

    public static void main(String[] args) {
        List<String> options = new ArrayList<>();
        options.add("Graphical application");
        options.add("Command line application");

        int option = 0;
        try {
            do {
                option = Utils.showAndSelectIndex(options, "\n\nApplication options:");

                if (option == 0) {
                    new VaccineCounterController().GenerateReport();
                    MainMenuGUI menu = new MainMenuGUI();
                    menu.run();
                } else if (option == 1) {
                    new VaccineCounterController().GenerateReport();
                    MainMenuUI menu = new MainMenuUI();
                }
            } while (option != 0 && option !=1 && option !=-1);

        } catch (NumberFormatException e) {
            System.out.println("\nPlease choose a valid option!");
            main(args);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}