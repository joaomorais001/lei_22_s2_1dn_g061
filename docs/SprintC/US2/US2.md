# US02 - Schedule a vaccination by the receptionist

## 1. Requirements Engineering

### 1.1. User Story Description

*As a receptionist at one vaccination center, I want to schedule a vaccination.*

### 1.2. Customer Specifications and Clarifications 
>**Question 1**:
>"Regarding US02, i would like to know if a receptionist has the ability to schedule an appointment in different vaccination centres or only on their own."
> 
>**Answer:**
>"The receptionist has the ability to schedule the vaccine in any vaccination center. The receptionist should ask the SNS user to indicate/select the preferred vaccination center."

>**Question 2**:
>"I would like to ask if you cloud tell me if my interpretation of this US was correct.
So my interpretation was that the receptionist whould choose a vaccination center and than in that vacination center she would schedule the second dosage of the vacinne"
>
>**Answer:**
>"The goal of this US is to schedule a vaccination for a SNS user. The SNS user should go to a vaccination center and a receptionist should use the application to schedule a vaccination for the SNS user. The receptionist should ask the SNS user for data required to schedule a vaccination. The data needed to schedule a vaccination is the same required in US01. Please check the Project Description available in moodle."

>**Information given by client**:
>"The acceptance criteria for US1 and US2 are: a. A SNS user cannot schedule the same vaccine more than once. b. The algorithm should check if the SNS User is within the age and time since the last vaccine".
This means:
a. At a given moment, the SNS user cannot have more than one vaccine (of a given type) scheduled;
b. The algorithm has to check which vaccine the SNS user took before and check if the conditions (age and time since the last vaccine) are met. If the conditions are met the vaccination event should be scheduled and registered in the system. When scheduling the first dose there is no need to check these conditions."

>**Question 3**:
>"Relatively to USs 1 and 2, our group found a question about vaccine types.
When the chosen vaccination center is a Community Mass Vaccination Center(CMVC), it will provide only one vaccine type to be scheduled. However, vaccine type is not an atributte of CMVC.
Are we allowed to create an attribute of vaccine type in CMVC object?"
>
>**Answer:**
>"Please study ESOFT and discuss this issue with ESOFT teachers."

>**Question 4**:
>"For the US1, the acceptance criteria is: A SNS user cannot schedule the same vaccine more than once. For the US2, the acceptance criteria is: The algorithm should check if the SNS User is within the age and time since the last vaccine.
[1] Are this acceptance criteria exclusive of each US or are implemented in both?"
[2] To make the development of each US more clear, could you clarify the differences between the two US?"
>
>**Answer:**
>1- The acceptance criteria for US1 and US2 should be merged. The acceptance criteria por US1 and US2 is: A SNS user cannot schedule the same vaccine more than once. The algorithm should check if the SNS User is within the age and time since the last vaccine."
2- In US1 the actor is the SNS user, in US2 the actor is the receptionist. In US1 the SNS user is already logged in the system and information that is required and that exists in the system should be automatically obtained. In US2 the receptionist should ask the SNS user for the information needed to schedule a vaccination. Information describing the SNS user should be automatically obtained by introducing the SNS user number.

>**Question 5**:
>"When a receptionist schedules a vaccination for an SNS user, should they be presented with a list of available vaccines (brands, that meet acceptance criteria) from which to choose? Or should the application suggest only one?"
>
>**Answer:**
>The receptionist do not select the vaccine brand.
When the user is at the vaccination center to take the vaccine, the nurse selects the vaccine. In Sprint D we will introduce new USs where the nurse records the administration of a vaccine to a SNS user.

### 1.3. Acceptance Criteria
**AC01:**
*A SNS user cannot schedule the same vaccine more than
once.*

### 1.4. Found out Dependencies

Depdency from: 

US3 SNS users info;

US12 Vaccine type information;

US9 Vaccine Centers.



### 1.5 Input and Output Data

**Input Data:**

- User's SNS Number

**Output Data:**

Confirmation.

### 1.6. System Sequence Diagram (SSD)

![US2-SSD](US2-SSD.svg)


### 1.7 Other Relevant Remarks

None

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US2-DM](US2-DM.svg)

### 2.2. Other Remarks

None


## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer                      | Justification (with patterns)                                     |
| :------------- |:--------------------------------------------|:----------------------------|:------------------------------------------------------------------|
| Step 1         | ...interacting with the actor?              | ScheduleVaccineUI           | Pure Fabrication                                                  |
|                | ...coordinating the user story?             | ScheduleVaccineController   | Controller                                                        |
| Step 2         | ...requesting the information?              | ScheduleVaccineUI           |                                                                   |
| Step 3         | ...saving the inputted information?         | ScheduleVaccineStore        | IE: has its own data                                              |
| Step 4         | n/a                                         |                             |                                                                   |
| Step 5         | n/a                                         |                             |                                                                   |
| Step 6         | ...showing the confirmation?                | ScheduleVAccineUI           | IE: responsible for user interactions                             | 


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

* scheduleVaccine
* Company
* scheduleVaccineDto


Other software classes (i.e. Pure Fabrication) identified: 
 * scheduleVaccineUI
 * scheduleVaccineStore
 * scheduleVaccineController

## 3.2. Sequence Diagram (SD)

![US2-SD](US2-SD.svg)

## 3.3. Class Diagram (CD)

![US2-CD](US2-CD.svg)

# 4. Tests 

## Test 1

	    private final ScheduleVaccineStore appointmentList;

    public static final String data = "24-02-2003";
    public static final String time = "11:00";
    private static final String name = "João";
    public static final int SNSNumber = 123456789;
    private static final String email = "joao@isep.ipp.pt";
    private static final String address = "Rua sao joao";
    private static final int PhoneNumber = 123456789;
    private static final int CCNumber = 12345678;
    static SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    static Date birthday;

    static {
        try {
            birthday = df.parse("24-09-2003");
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
    private static final Date birthdate = birthday;
    private static final String sex = "masculino";
    private static final String vaccineType = "covid";
    private static final String vacName = "centro";
    private static final String vacAddress = "Rua de Guimaraes";
    private static final String vacPhoneNumber = "912626999";
    private static final String vacFaxNumber = "019283746";
    private static final String vacWebsite = "website@test.com";
    private static final int vacOpeningHour = 9;
    private static final int vacClosingHour = 19;
    private static final int vacSlotDuration = 12;
    private static final int vacMaxVaccines = 200;

    SNSUser snsuser = new SNSUser(email,address,PhoneNumber,SNSNumber,name, CCNumber,birthdate,sex);

    VacCenter vacCenter = new VacCenter(vacName, vacAddress, vacPhoneNumber, vacFaxNumber, vacWebsite, vacOpeningHour, vacClosingHour, vacSlotDuration, vacMaxVaccines);



    public scheduleVaccineTest() {
        this.appointmentList = new ScheduleVaccineStore();
    }


    @Test
    public void ensureAppointmentIsSaved() {
        ScheduleVaccine Appointment = new ScheduleVaccine(SNSNumber,data,time,vaccineType,vacName);
        boolean res;
        res = ScheduleVaccineStore.addSNSUserAppointment(Appointment);
        Assertions.assertTrue(res);
    }





# 5. Construction (Implementation)

## Class SheculeVaccine

    private int SNSNumber;

    private String Data;

    private String Time;

    private String Vaccinetype;

    private String VaccineCenter;

    public ScheduleVaccine(int SNSNumber, String data, String Time, String vaccinetype, String vaccineCenter) {

        this.SNSNumber = SNSNumber;
        this.Data = data;
        this.Time = Time;
        this.Vaccinetype = vaccinetype;
        this.VaccineCenter = vaccineCenter;
    }

    /**
     * The toString() method returns a string representation of the object
     * 
     * @return The toString method is returning a string with the information of the appointment.
     */
    @Override
    public String toString() {
        return "\n--Appointment--\n" + "SNSNumber= " + SNSNumber+ "\n" + "Data= " + Data +"\n"+ "Time= " + Time + "\n" + "Vaccine Type= " + Vaccinetype +"\n"+ "Vaccine center= "+VaccineCenter+"\n";
    }

    /**
     * This function returns the number of SNS messages that have been sent
     * 
     * @return The SNSNumber is being returned.
     */
    public int getSNSNumber() {
        return SNSNumber;
    }

    /**
     * This function returns the data of the node
     * 
     * @return The data is being returned.
     */
    public String getData() {
        return Data;
    }

    /**
     * This function returns the time of the event
     * 
     * @return The time of the event.
     */
    public String getTime() {
        return Time;
    }

    /**
     * This function returns the vaccine type of the patient
     * 
     * @return The Vaccine type is being returned.
     */
    public String getVaccine() {
        return Vaccinetype;
    }

    /**
     * This function returns the VaccineCenter of the object
     * 
     * @return The VaccineCenter is being returned.
     */
    public String getVaccineCenter() {
        return VaccineCenter;
    }

    /**
     * This function sets the SNSNumber of the user
     * 
     * @param SNSNumber The number of SNS messages to send.
     */
    public void setSNSNumber(int SNSNumber) {
        this.SNSNumber = SNSNumber;
    }

    /**
     * This function sets the data of the object to the data passed in.
     * 
     * @param data The data to be sent to the server.
     */
    public void setData(String data) {
        Data = data;
    }

    /**
     * This function sets the time of the event
     * 
     * @param time The time of the event.
     */
    public void setTime(String time) {
        Time = time;
    }

    /**
     * This function sets the vaccine type
     * 
     * @param vaccinetype The type of vaccine.
     */
    public void setVaccine(String vaccinetype) {
        Vaccinetype = vaccinetype;
    }

    /**
     * This function sets the vaccine center of the patient
     * 
     * @param vaccineCenter The name of the vaccine center
     */
    public void setVaccineCenter(String vaccineCenter) {
        VaccineCenter = vaccineCenter;
    }

## Class ScheduleVaccineController

    private final ScheduleVaccineStore Store;

    private final VacCenterStore vacCenters;

    private static ArrayList<Vaccine> vaccineList = null;

    private Company company;


    public ScheduleVaccineController(){
        this.company = App.getInstance().getCompany();
        Store = company.getScheduleStore();
        vacCenters = company.getVacStore();

    }
    

    /**
     * It saves the appointment data to the database.
     *
     * @param appointment This is the appointment object that you want to save.
     */
    public void saveAppointmentData(ScheduleVaccine appointment){
        Store.addSNSUserAppointment(appointment);

    }

    /**
     * Get all the appointments from the company's schedule store.
     *
     * @return A list of all the appointments in the schedule store.
     */
    public List<ScheduleVaccine> getAppointments() {
        return this.company.getScheduleStore().getAppointments();
    }



    /**
     * The function returns a list of strings that contains the names of the vaccines
     *
     * @return A list of strings
     */
    public static List<String> VaccineTypes(){
        ArrayList<String> Vaccinetypes = new ArrayList<>();

        Vaccinetypes.add("covid");
        Vaccinetypes.add("tetanus");
        Vaccinetypes.add("hiv");
        Vaccinetypes.add("hepatitis");

        return Vaccinetypes;
    }

    /**
     * This function checks if the user is registered in the system
     *
     * @param SNSNumber The SNS number of the user.
     */
    public void vallidateSNSUser(int SNSNumber){
        boolean val= false;
        for (int i = 0; i < RegisterSNSUserStore.getSNSUsers().size(); i++) {

            if(SNSNumber == RegisterSNSUserStore.getSNSUsers().get(i).getSNSNumber())
                val = true;

        }
        if (val == false)
            throw new IllegalArgumentException("User is not registered in the system");
    }

    /**
     * It validates the time of the appointment
     *
     * @param time the time that the user wants to book
     * @param centro The name of the center
     */
    public void vallidateTime(String time, String centro){
        VacCenter center = null;
        
        for (int i = 0; i < company.getVacStore().getVacCenters().size(); i++) {

            if(centro == company.getVacStore().getVacCenters().get(i).getName())
                center = company.getVacStore().getVacCenters().get(i);

        }
        
        String[] timeSplit = time.split(":");
        int[] times = new int[2];
        times[0] = Integer.parseInt(String.valueOf(valueOf(timeSplit[0])));
        times[1] = Integer.parseInt(String.valueOf(valueOf(timeSplit[1])));
            
        if (!(times[0] > center.getOpeningHour() && times[0] < center.getClosingHour()))
            throw new IllegalArgumentException("Invalid Hours.");

        if (time == null){
            throw new IllegalArgumentException("Please insert a time");}
    }

    /**
     * This function checks if the vaccine type is valid or not
     *
     * @param VaccineType The type of vaccine that is being administered.
     */
    public void vallidateVaccinetype(String VaccineType){

        if(!ScheduleVaccineController.VaccineTypes().contains(VaccineType))
            throw new IllegalArgumentException("Vaccine Type Invalid!!");
    }

## ScheduleVaccineStore

    private static ArrayList<ScheduleVaccine> appointmentList;


    public ScheduleVaccineStore() {
        appointmentList = new ArrayList<>();
    }

    /**
     * This function adds a new appointment to the appointment list
     * 
     * @param Appointment The appointment object that is to be added to the list.
     * @return A boolean value.
     */
    public boolean addSNSUserAppointment(ScheduleVaccine Appointment){
        if (!validateAppointment(Appointment)) {
            return false;
        }
        return appointmentList.add(Appointment);
    }

    /**
     * This function checks if the appointment is already made
     * 
     * @param Appointment The appointment that is being validated.
     * @return The method is returning a boolean value.
     */
    public static boolean validateAppointment(ScheduleVaccine Appointment) {
        if (Appointment == null) {
            throw new IllegalArgumentException("You have to insert a SNSuser!");

        }
        if (appointmentList.contains(Appointment)) {
            throw new IllegalArgumentException("Appointment already made!");
        }
        return !appointmentList.contains(Appointment);
    }

    /**
     * It returns a copy of the appointmentList
     * 
     * @return A list of all the appointments
     */
    public static List<ScheduleVaccine> getAppointments() {
        return  new ArrayList<>(appointmentList);
    }

## ScheduleVaccineUI

    private final ScheduleVaccineController controller;

    public ScheduleVaccineUI() {
        controller = new ScheduleVaccineController();
    }

    /**
     * This function is responsible for the scheduling of a vaccine
     */
    public void run() {

        int SNSNumber;
        String vaccinetype;
        boolean val;
        String option;
        String vaccineCenter;
        String data;
        String time;


        do {
            do {
                SNSNumber = Utils.readIntegerFromConsole("Insert SNS Number: ");
                val = true;
                try {
                    SNSUser.validateSNSNumber(SNSNumber);
                } catch (Exception e) {
                    System.out.println("Invalid SNSNumber!");
                    val = false;
                }
                try {
                    controller.vallidateSNSUser(SNSNumber);
                } catch (Exception e) {
                    System.out.println("The user is not registered!");
                    val = false;
                }
            } while (!val);
            do {
                System.out.println("Chose a vaccination center:");
                vaccineCenter = displayVacCenters();
                val = true;
            } while (!val);
            do {
                showVaccineTypes();
                vaccinetype = Utils.readLineFromConsole("Chose a vaccine type:");
                val = true;
                try {
                    controller.vallidateVaccinetype(vaccinetype);
                } catch (IllegalArgumentException e) {
                    System.out.println("Invalid vaccine!");
                    val = false;
                }
            } while (!val);
            do {
                data = String.valueOf(Utils.readDateFromConsole("Insert the day: (DD-MM-YYYY)"));
                val = true;
            } while (!val);
            do {
                time = Utils.readLineFromConsole("Insert the time: (HH:MM)");
                val = true;
                try {

                    controller.vallidateTime(time,vaccineCenter);
                } catch (IllegalArgumentException e) {
                    System.out.println("Invalid time!");
                    val = false;
                }
            } while (!val);

            displayInformation(SNSNumber,vaccinetype,data,time,vaccineCenter);
            option = Utils.readLineFromConsole("\n Is the information correct? \n (Type Yes to confirm)");

        }while (!Objects.equals(option, "yes") && !Objects.equals(option, "Yes"));


        ScheduleVaccine newAppointment = new ScheduleVaccine(SNSNumber,data,time,vaccinetype,vaccineCenter);

        controller.saveAppointmentData(newAppointment);

        System.out.println("The vaccine was scheduled!");
    }

    /**
     * This function displays the information of the appointment
     * 
     * @param SNSNumber The SNS number of the patient
     * @param vaccinetype The type of vaccine that the user wants to get.
     * @param data The date of the appointment
     * @param time The time of the appointment
     * @param vaccinecenter The name of the vaccine center
     */
    public void displayInformation(int SNSNumber,String vaccinetype, String data,String time,String vaccinecenter) {
        String info = String.format("\n *** Appointment info *** \n SNSNumber: %d\n Data: %s\n Time: %s \n Vaccine type: %s \n VaccineCenter: %s",SNSNumber, data, time,vaccinetype,vaccinecenter);
        Utils.printToConsole(info);
    }
    
    /**
     * The function `displayVacCenters` returns the name of the vaccination center that the user chooses
     * 
     * @return The name of the vaccination center.
     */
    public String displayVacCenters (){
        return chooseVacCenter().getName();
    }

    /**
     * This function prints out all the vaccine types in the database
     */
    public void showVaccineTypes(){
        List<String> Vaccinetypes = controller.VaccineTypes();

        System.out.println("\nVaccine Types\n");
        for (String i : Vaccinetypes) {
            System.out.println(i);
        }
    }


# 6. Integration and Demo 

*We had to make efforts to connect the various UC used for this UC and the main problem was not having the UC 12 because it wasn't assigned to our group in the past sprint.*


# 7. Observations

No observations.