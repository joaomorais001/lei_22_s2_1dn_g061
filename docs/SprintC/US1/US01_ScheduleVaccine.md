# US 01 - To schedule a new vaccine

## 1. Requirements Engineering


### 1.1. User Story Description


As a SNS user, I intend to use the application to schedule a vaccine.


### 1.2. Customer Specifications and Clarifications 


**From the specifications document:**

>	"To take a vaccine, the SNS user should use the application to schedule his/her vaccination. The user
should introduce his/her SNS user number, select the vaccination center, the date, and the time (s)he
wants to be vaccinated as well as the type of vaccine to be administered (by default, the system
suggests the one related to the ongoing outbreak). Then, the application should check the
vaccination center capacity for that day/time and, if possible, confirm that the vaccination is
scheduled and inform the user that (s)he should be at the selected vaccination center at the
scheduled day and time. The SNS user may also authorize the DGS to send a SMS message with
information about the scheduled appointment. If the user authorizes the sending of the SMS, the
application should send an SMS message when the vaccination event is scheduled and registered in
the system. Some users (e.g.: older ones) may want to go to a healthcare center to schedule the
vaccine appointment with the help of a receptionists at one vaccination center."
> 
> "On the scheduled day and time, the SNS user should go to the vaccination center to get the vaccine."



**From the client clarifications:**

> **Question:** Regarding the user story 1 "As a SNS user, I intend to use the application to schedule a vaccine.", should the application already send the SMS with the confirmation of the schedule or it will be other user story in the future.
>
> [**Awnser:**](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16331#p20953)

-

> **Question:** We are unsure if it's in this user stories that's asked to implement the "send a SMS message with information about the scheduled appointment" found on the Project Description available in moodle. Could you clarify?
>
> [**Awnser:**](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16457#p21124)  In a previous clarification that I made on this forum, I said: "[The user should receive a] SMS Message to warn of a scheduling [and the message] should include: Date, Time and vaccination center". Teams must record the answers! A file named SMS.txt should be used to receive/record the SMS messages. We will not use a real word service to send SMSs.

-

> **Question:** We would like to know if when scheduling a vaccine, should a list of  existing vaccine types and vaccination centers be displayed in order for him to choose one option , or should he just enter them?
>
> [**Awnser:**](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16482#p21149) Please check carefully the project description available in moodle.

-

> **Question:** in order to schedule a vaccine, the SNS user should:
>> 1 - introduce his/her SNS user number;
> 
>> 2 - select the vaccination center;
> 
>> 3 - select the date and time;
> 
>> 4 - select the type of vaccine to be administered.
>
> [**Awnser:**](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16513#p21185)

-

> **Question:**
> For the US1, the acceptance criteria is: A SNS user cannot schedule the same vaccine more than once.
>
> For the US2, the acceptance criteria is: The algorithm should check if the SNS User is within the age and time since the last vaccine.
> 
> Are this acceptance criteria exclusive of each US or are implemented in both?
>
> To make the development of each US more clear, could you clarify the differences between the two US?
>
> [**Awnser:**](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16520#p21192)  
> 1- The acceptance criteria for US1 and US2 should be merged. The acceptance criteria por US1 and US2 is: A SNS user cannot schedule the same vaccine more than once. The algorithm should check if the SNS User is within the age and time since the last vaccine."
>
> 2- In US1 the actor is the SNS user, in US2 the actor is the receptionist. In US1 the SNS user is already logged in the system and information that is required and that exists in the system should be automatically obtained. In US2 the receptionist should ask the SNS user for the information needed to schedule a vaccination. Information describing the SNS user should be automatically obtained by introducing the SNS user number.


-

> **Question:** The project description says "To take a vaccine, the SNS user should use the application to schedule his/her vaccination. The user should introduce his/her SNS user number, select the vaccination center, the date, and the time(s)he wants to be vaccinated as well as the type of vaccine to be administered [...]".
> 
> Does the user have to enter the date and time they want or does the system have to show the available dates and times for the user to select?
>
> [**Awnser:**](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16615#p21323) In this sprint the actor should introduce (using the keyboard) the date and time (s)he wants to be vaccinated.

-

> **Question:** A question arose in our group about the Acceptance Criteria of this US : "A SNS user cannot schedule the same vaccine more than once". When a User is Scheduling a vaccination appointment, should he choose a vaccine or a typeOfVaccine? And how can the user comply with this Acceptance Criteria, since a vaccine has several doses, so he will have to choose that type of vaccine again in the future in order to complete the vaccination process.
>
> [**Awnser:**](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16616#p21324)

-

> **Question:** You said "The application should present a message saying that the SNS user did not scheduled a vaccination." and "The receptionist registers the arrival of a SNS user only after confirming that the user has a vaccine scheduled for that day and time.". So our question is:
> 
> [1] - What defines that a user doesn't have an appointment? For example: If a user goes to a vaccination center at 25/05/2022, 11:00 and only has an appointment at 18:00 for that day, does this count as not ready to take the vaccine? What is the criteria for not having an appointment?
>
> [**Awnser:**](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16623#p21331)

-

> [**CLARIFICATION:**](https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=16655#p21366)
> The acceptance criteria for US1 and US2 are: a. A SNS user cannot schedule the same vaccine more than once. b. The algorithm should check if the SNS User is within the age and time since the last vaccine".
>
> This means:
>
>> a. At a given moment, the SNS user cannot have more than one vaccine (of a given type) scheduled;
>
>> b. The algorithm has to check which vaccine the SNS user took before and check if the conditions (age and time since the last vaccine) are met. If the conditions are met the vaccination event should be scheduled and registered in the system. When scheduling the first dose there is no need to check these conditions.




### 1.3. Acceptance Criteria

* **AC1:** A SNS user cannot schedule the same vaccine more than once.

### 1.4. Found out Dependencies

* There is a dependency to "US3: As a receptionist, I want to register a SNS user" since the SNS User needs to be created in order for the SNS User schedule a vaccine.
* There is a dependency to "US9: As an administrator, I want to register a vaccination center to respond to a certain pandemic." since the SNS User needs to choose the vaccination center.
* There is a dependency to "US12 Specify a new vaccine type" since a vaccine will be associated with a type of vaccine, therefore, the types of vaccines must be stipulated in advance.

### 1.5 Input and Output Data

**Input Data:**

* Typed data:
  * SNS User Number
  * Date
  * Time
  
* Selected data:
  * Vaccination Center 
  * Vaccination Type

**Output Data:**

* (In)Success of the operation

### 1.6. System Sequence Diagram (SSD)

![US01_SSD](US01_SSD.svg)

### 1.7 Other Relevant Remarks

* In one of the client's meetings, he said that the part of notifying the client by SMS was not to be done. It is represented in Diagrams, however it is as if it is not.

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US01_MD](US01_DM.svg)

### 2.2. Other Remarks

n/a


## 3. Design - User Story Realization 

### 3.1. Rationale

**SSD - Alternative 1 is adopted.**

| Interaction ID | Question: Which class is responsible for...                     | Answer                        | Justification (with patterns)                                                                                                                                                                          |
|:-------------  |:--------------------------------------------------------------- |:-----------------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Step 1  		 | ... interacting with the actor?                                 | ScheduleVaccinationUI         | **Pure Fabrication**: There is no justification for assigning this responsibility to any existing class in the Domain Model.                                                                           |
|                | ... coordinating the US?                                        | ScheduleVaccinationController | **Controller**                                                                                                                                                                                         |
| Step 2  		 |                                                                 |                               |                                                                                                                                                                                                        |
| Step 3         | ... instantiating a new ScheduleVaccineStore?                   | Company                       | **Creator (R1)**                                                                                                                                                                                       |
|                | ... knowing the Schedule Vaccinations                           | ScheduleVaccinationStore      | **IE**: Knows all the schedule vaccinations. **HC+LC**: Company delegates that responsibility to the "ScheduleVaccinationStore"                                                                        |
|                | ... instantiating a new Schedule Vaccine?                       | ScheduleVaccinationStore      | **Creator (R1)**                                                                                                                                                                                       |
|                | ... validate a new Schedule Vaccine (local validation)          | ScheduleVaccinationStore      | **Pure Fabrication**: There is no justification for assigning this responsibility to any existing class in the Domain Model.                                                                           |
|                | ... knowing Schedule Vaccine Data?                              | ScheduleVaccinations          | **IE**: Owns its data.                                                                                                                                                                                 |
|                | ... transferring business data in DTO?                          | ScheduleVaccinationMapper     | **DTO**: In order for the UI not to have direct access to business objects, it is best to choose to use a DTO.                                                                                         |
|                | ... instantiating a new VaccinationCenterStore?                 | Company                       | **Creator (R1)**                                                                                                                                                                                       |
|                | ... knowing the Vaccination Center                              | VaccinationCenterStore        | **IE**: Knows all the vaccination centers. **HC+LC**: Company delegates that responsibility to the "VaccinationCenterStore"                                                                            |
|                | ... instantiating a new Vaccination Center?                     | VaccinationCenterStore        | **Creator (R1)**                                                                                                                                                                                       |
|                | ... transferring business data in DTO?                          | VaccinationCenterMapper       | **DTO**: In order for the UI not to have direct access to business objects, it is best to choose to use a DTO.                                                                                         |
|                | ... knowing Vaccination Center Data?                            | VaccinationCenter             | **IE**: Owns its data.                                                                                                                                                                                 |
| Step 4  		 |                                                                 |                               |                                                                                                                                                                                                        |              
| Step 5  		 | ... notifying SNS User                                          | ScheduleVaccinationController |                                                                                                                                                                                                        |                         
| Step 6  		 |                                                                 |                               | **Controller**                                                                                                                                                                                         |                        
| Step 7  		 | ... validate a new Schedule Vaccine (global validation)         | ScheduleVaccinationStore      | **Pure Fabrication**: There is no justification for assigning this responsibility to any existing class in the Domain Model.                                                                           |
|                | ... informing operation success?                                | ScheduleVaccinationUI         | **IE**: Is responsible for user interactions.                                                                                                                                                          |


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Company
 * Vaccine
 * VaccineType

Other software classes (i.e. Pure Fabrication) identified: 

 * SpecifyVaccineUI 
 * SpecifyVaccineController
 * VaccineStore
 * VaccineTypeStore

## 3.2. Sequence Diagram (SD)

![US01_SD](US01_SD.svg)

## 3.3. Class Diagram (CD)

**From alternative 1**

![US01_CD](US01_CD.svg)

# 4. Tests 

**Test1** Schedule Vaccination

    @Test
   


# 5. Construction (Implementation)
     public SNSUser getSNSUserByNumber(int snsnum) {
        SNSUser usersns = null;
        for (int i = 0; i < snsuserstore.getSNSUsers().size(); i++) {
            if (snsnum == snsuserstore.getSNSUsers().get(i).getSNSNumber()) {
                usersns = snsuserstore.getSNSUsers().get(i);
            }
        }
        return usersns;
    }


    public ScheduleVaccineController(){
    this.company = App.getInstance().getCompany();
    Store = company.getScheduleStore();
    vacCenters = company.getVacStore();
    this.mapper = new VacCenterMapper();
    }


    /**
     * It saves the appointment data to the database.
     *
     * @param appointment This is the appointment object that you want to save.
     */
    public void saveAppointmentData(ScheduleVaccine appointment){
        Store.addSNSUserAppointment(appointment);

    }

    /**
     * Get all the appointments from the company's schedule store.
     *
     * @return A list of all the appointments in the schedule store.
     */
    public List<ScheduleVaccine> getAppointments() {
        return this.company.getScheduleStore().getAppointments();
    }



    /**
     * The function returns a list of strings that contains the names of the vaccines
     *
     * @return A list of strings
     */
    public static List<String> VaccineTypes(){
        ArrayList<String> Vaccinetypes = new ArrayList<>();

        Vaccinetypes.add("covid");
        Vaccinetypes.add("tetanus");
        Vaccinetypes.add("hiv");
        Vaccinetypes.add("hepatitis");

        return Vaccinetypes;
    }

    /**
     * This function checks if the user is registered in the system
     *
     * @param SNSNumber The SNS number of the user.
     */
    public static void validateSNSUser(int SNSNumber){
        boolean val= false;
        for (int i = 0; i < RegisterSNSUserStore.getSNSUsers().size(); i++) {

            if(SNSNumber == RegisterSNSUserStore.getSNSUsers().get(i).getSNSNumber())
                val = true;

        }
        if (val == false)
            throw new IllegalArgumentException("User is not registered in the system");
    }

    /**
     * It validates the time of the appointment
     *
     * @param time the time that the user wants to book
     * @param centro The name of the center
     */
    public void validateTime(String time, String centro){
        VacCenter center = null;
        
        for (int i = 0; i < company.getVacStore().getVacCenters().size(); i++) {

            if(centro == company.getVacStore().getVacCenters().get(i).getName())
                center = company.getVacStore().getVacCenters().get(i);

        }
        
        String[] timeSplit = time.split(":");
        int[] times = new int[2];
        times[0] = Integer.parseInt(String.valueOf(valueOf(timeSplit[0])));
        times[1] = Integer.parseInt(String.valueOf(valueOf(timeSplit[1])));
            
        if (!(times[0] > center.getOpeningHour() && times[0] < center.getClosingHour()))
            throw new IllegalArgumentException("Invalid Hours.");

        if (time == null){
            throw new IllegalArgumentException("Please insert a time");}
    }

    /**
     * This function checks if the vaccine type is valid or not
     *
     * @param VaccineType The type of vaccine that is being administered.
     */
    public void validateVaccinetype(String VaccineType){

        if(!ScheduleVaccineController.VaccineTypes().contains(VaccineType))
            throw new IllegalArgumentException("Vaccine Type Invalid!!");
    }

    public boolean validateMaxVaccines(String date, String centro){
        int count = 0;
        int maxvaccines;

        for (int i = 0; i < company.getScheduleStore().getAppointments().size(); i++) {
            if(centro == company.getScheduleStore().getAppointments().get(i).getVaccineCenter() && date == company.getScheduleStore().getAppointments().get(i).getData())
                count ++;
        }

        for (int j = 0; j < company.getVacStore().getVacCenters().size(); j++) {
            if(centro == company.getVacStore().getVacCenters().get(j).getName()) {
                maxvaccines = company.getVacStore().getVacCenters().get(j).getMaxVaccines();
                if (maxvaccines <= count) {
                    throw new IllegalArgumentException("Max vaccines achieved");
                }
            }
        }
        return true;
    }
# 6. Integration and Demo 


# 7. Observations





