# OO Analysis #

The construction process of the domain model is based on the client specifications, especially the nouns (for _concepts_) and verbs (for _relations_) used. 

## Rationale to identify domain conceptual classes ##
To identify domain conceptual classes, start by making a list of candidate conceptual classes inspired by the list of categories suggested in the book "Applying UML and Patterns: An Introduction to Object-Oriented Analysis and Design and Iterative Development". 


### _Conceptual Class Category List_ ###

**Business Transactions**

* Vaccination process
---

**Transaction Line Items**


---

**Product/Service related to a Transaction or Transaction Line Item**

* Administration process
* Vaccination certificate
* Vaccines
---


**Transaction Records**

* Appointment
* Notifications
* Vaccination Certificate
* Vaccination Details
---  


**Roles of People or Organizations**

* DGS
* ARS
* AGES
* Administrator
* Receptionist
* Nurse
* Center Coordinator
* SNS user
---


**Places**

* Mass Vaccination Centers
* Healthcare Centers
---

**Noteworthy Events**

* Vaccine appointment
* Vaccine administration
---


**Physical Objects**

* Vaccines
---


**Descriptions of Things**

* Vaccine type
---


**Catalogs**
  

---


**Containers**
  

---


**Elements of Containers**
  

---


**Organizations**

* DGS
* ARS
* AGES
---

**Other External/Collaborating Systems**

* DGS IT Department
---


**Records of finance, work, contracts, legal matters**


---


**Financial Instruments**


---


**Documents mentioned/used to perform some work/**

 
---



###**Rationale to identify associations between conceptual classes**###

An association is a relationship between instances of objects that indicates a relevant connection and that is worth of remembering, or it is derivable from the List of Common Associations: 

+ **_A_** is physically or logically part of **_B_**
+ **_A_** is physically or logically contained in/on **_B_**
+ **_A_** is a description for **_B_**
+ **_A_** known/logged/recorded/reported/captured in **_B_**
+ **_A_** uses or manages or owns **_B_**
+ **_A_** is related with a transaction (item) of **_B_**
+ etc.


| Concept (A) 		                    |    Association     |               Concept (B) |
|:----------------------------------|:------------------:|--------------------------:|
| DGS | owns | Vaccination center | 
| DGS | employs | Administrator |
| DGS | employs | Nurse |
| DGS | employs | Receptionist |
| DGS | employs | Center Coordinator |
| Administrator | manages | Employee |
| Administrator | manages | Vaccination Center |
| Nurse | is a | Employee | 
| Receptionist | is a | Employee |
| Center Coordinator | is a | Employee |
| Mass Vaccination Center | is of | Vaccination Center |
| Healthcare Center | is of | Vaccination Center |
| Healthcare Center | is associated with | ARS |
| Healthcare Center | is associated with | AGES |
| Vaccine | is of | Vaccine type |
| Vaccine type | created by | Administrator | 
| Vaccination appointment | created by/for | SNS User |
| Nurse | administrates | Vaccine |
| Nurse | registers | Vaccination details | 
| SNS User | requests | Vaccine Appointment |
| SNS User | requests | Vaccine |
| SNS User | requests | Vaccination certificate |
| SNS User | is sent to | Recovery Room |
| SNS User | is sent to | Waiting Room |
| SNS User | receives | Notification |
| Vaccination Appointment | emits | Notification |
| Receptionist | checks | Vaccination Appointment |
| Receptionist | manages | Waiting Room |
| Center Coordinator | coordinates | Vaccination Center |
| Vaccination Certificate | needs | Vaccination Details |