# US5 - To list the waiting room

## 1. Requirements Engineering

_In this section, it is suggested to capture the requirement description and specifications as provided by the client as well as any further clarification on it. It is also suggested to capture the requirements acceptance criteria and existing dependencies to other requirements. At last, identify the involved input and output data and depicted an Actor-System interaction in order to fulfill the requirement._

### 1.1. User Story Description

As a nurse, I intend to consult the users in the waiting room of a vacination center.

### 1.2. Customer Specifications and Clarifications

> **Question:**
> I would like to know which are the attributes of the waiting room.
>
> **Answer:**
> The waiting room will not be registered or defined in the system. The waiting room of each vaccination center has the capacity to receive all users who take the vaccine on given slot.

> **Question:**
> We need to know if the nurse have to chose the vaccination center before executing the list or if that information comes from employee file?
>
> **Answer:**
> When the nurse starts to use the application, firstly, the nurse should select the vaccination center where she his working. The nurse wants to check the list of SNS users that are waiting in the vaccination center where she his working.

> **Question:**
> Regarding US05, the listing is supposed to be for the day itself or for a specific day?
>
> **Answer:**
> The list should show the users in the waiting room of a vaccination center.

> **Question:**
> Regarding the US05. In the PI description it is said that, by now, the nurses and the receptionists will work at any center. Will this information remain the same on this Sprint, or will they work at a specific center?
>
> **Answer:**
> Nurses and receptionists can work in any vaccination center.

> **Question:**
> Is it supposed to remove a SNS user of the wait list when he leaves the waiting room to get the vaccine? If yes, how do we know when the sns user leaves the waiting room?
>
> **Answer:**
> US5 is only to list users that are in the waiting room of a vaccination center. In Sprint D we will introduce new user stories.

> **Question:**
> What information about the Users (name, SNS number, etc) should the system display when listing them?
>
> **Answer:**
> Name, Sex, Birth Date, SNS User Number and Phone Number.

### 1.3. Acceptance Criteria

- **AC01:** SNS Users’ list should be presented by order of arrival.
- **AC02:** The nurses can work in any vaccination center.
- **AC03:** The waiting room shall not be registered or defined in the system and has unlimited capacity.
- **AC04:** The list must show the following attributes of the SNS user: Name, birthdate, sex, sns number and phone number.

### 1.4. Found out Dependencies

- This us has a dependency from us1 which consists in adding users to the waiting room.

### 1.5 Input and Output Data

**Input data:**

- Vaccination center

**Output data:**

- Waiting room list

### 1.6. System Sequence Diagram (SSD)

_Insert here a SSD depicting the envisioned Actor-System interactions and throughout which data is inputted and outputted to fulfill the requirement. All interactions must be numbered._

![US5-SSD](US5-SSD.svg)

### 1.7 Other Relevant Remarks

_Use this section to capture other relevant information that is related with this US such as (i) special requirements ; (ii) data and/or technology variations; (iii) how often this US is held._

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

_In this section, it is suggested to present an excerpt of the domain model that is seen as relevant to fulfill this requirement._

![US5-MD](US5-MD.svg)

### 2.2. Other Remarks

_Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams)._

## 3. Design - User Story Realization

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer                    | Justification (with patterns)         |
| :------------- | :------------------------------------------ | :------------------------ | :------------------------------------ |
| Step 1         | ...interacting with the actor?              | ListWaitingRoomUI         | Pure Fabrication                      |
|                | ...coordinating the user story?             | ListWaitingRoomController | Controller                            |
| Step 2         | ...showing the list of centers?             | VacCenterStore            | Creator (Rule 1)                      |
| Step 3         | ...saving the inputted center?              | VacCenter                 | IE: has its own data                  |
| Step 4         | n/a                                         |                           |                                       |
| Step 5         | n/a                                         |                           |                                       |
| Step 6         | ...showing the obtained list?               | ListWaitingRoomUI         | IE: responsible for user interactions |

### Systematization

According to the taken rationale, the conceptual classes promoted to software classes are:

- Company
- SNSUser

Other software classes (i.e. Pure Fabrication) identified:

- ListWaitingRoomUI
- ListWaitingRoomController
- VacCenterStore
- SNSUserDTO
- SNSUserMapper

## 3.2. Sequence Diagram (SD)

_In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement._

![US5-SD](US5-SD.svg)

## 3.3. Class Diagram (CD)

_In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods._

![US5-CD](US5-CD.svg)

# 4. Tests

_In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling._

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Checks if the created waiting room list is equal to a manually created one

    public void testList() throws ParseException {
        int i = 0;

        List<SNSUserDTO> actual;
        List<SNSUserDTO> expected = new ArrayList<>();
        List<SNSUser> list2 = new ArrayList<>();

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String date = "10-10-2020";
        Date birthdate = df.parse(date);

        SNSUserDTO user1 = new SNSUserDTO(123456789, 123456789, "fabio", birthdate, "M");
        SNSUser user2 = new SNSUser("fabio@email.com", "rua", 123456789, 123456789, "fabio", 12345678, birthdate,
                "M");

        expected.add(user1);
        list2.add(user2);

        actual = controller.createWaitingRoomList(list2);

        Assertions.assertTrue(
                expected.size() == actual.size() && expected.get(i).toString().equals(actual.get(i).toString()) && expected.get(i).getClass() == actual.get(i).getClass());
    }

_It is also recommended to organize this content by subsections._

# 5. Construction (Implementation)

## Class ListWaitingRoomUI

    private ListWaitingRoomController controller;
    private String confirmation;
    private VacCenter center;

    public ListWaitingRoomUI() {
        controller = new ListWaitingRoomController();
    }

    public void run() {

        do {
            center = Utils.chooseVacCenter();
            showData(center);
            confirmation = Utils.readLineFromConsole("Is the chosen center right? (Y/N)");
        } while (!confirmation.equalsIgnoreCase("yes") && !confirmation.equalsIgnoreCase("y"));

        controller.createWaitingRoomList(controller.getWaitingRoom(center));
        printList(controller.getWaitingRoomList());

    }

    public List<SNSUserDTO> getWaitingRoomList() {
        return controller.getWaitingRoomList();
    }

    private void printList(List<SNSUserDTO> lista) {

        if (lista.size() == 0) {
            System.out.println("\nThe waiting room is empty!");
        } else {
            System.out.println("\nWaiting room list:\n");

            for (SNSUserDTO user : lista) {
                System.out.println("Name: " + user.getName());
                System.out.println("SNS number: " + user.getSNSNumber());
                System.out.println("Sex: " + user.getSex());
                Date birthdate = user.getBirthdate();
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                String strDate = dateFormat.format(birthdate);
                System.out.println("Birthdate: " + strDate);
                System.out.println("Phone number: " + user.getPhoneNumber());
                System.out.println();
            }
        }
    }

    public void showData(VacCenter center) {
        String data = String.format("\nChosen center: %s\n", center.getName());
        Utils.printToConsole(data);
    }

## Class ListWaitingRoomController

    private Company company;
    private SNSUserMapper mapper;

    public ListWaitingRoomController() {
        this.company = App.getInstance().getCompany();
        this.mapper = new SNSUserMapper();
    }

    public List<SNSUserDTO> listWaitingRoom;

    public List<SNSUser> getWaitingRoom(VacCenter center) {
        return center.getWaitroom();
    }

    public List<SNSUserDTO> createWaitingRoomList(List<SNSUser> list) {
        listWaitingRoom = mapper.toListDTO(list);
        return listWaitingRoom;
    }

    public List<SNSUserDTO> getWaitingRoomList() {
        return listWaitingRoom;
    }

    }

## Class SNSUserMapper

    public SNSUserMapper() {

    }

    public List<SNSUserDTO> toListDTO(List<SNSUser> users) {
        List<SNSUserDTO> usersDTO = new ArrayList();

        for (SNSUser user : users) {
            usersDTO.add(toDTO(user));
        }

        return usersDTO;
    }

    public SNSUserDTO toDTO(SNSUser user){
        SNSUserDTO userDTO = new SNSUserDTO();

        userDTO.setName(user.getName());
        userDTO.setBirthdate(user.getBirthdate());
        userDTO.setSex(user.getSex());
        userDTO.setSNSNumber(user.getSNSNumber());
        userDTO.setPhoneNumber(user.getPhoneNumber());

        return userDTO;
    }

## Class SNSUserDTO

    private String name;
    private int PhoneNumber;
    private int SNSNumber;
    private String sex;
    private Date birthdate;

    public SNSUserDTO(int PhoneNumber, int SNSNumber, String name, Date birthdate, String sex) {

        this.name = name;
        this.PhoneNumber = PhoneNumber;
        this.SNSNumber = SNSNumber;
        this.birthdate = birthdate;
        this.sex = sex;
    }

    public SNSUserDTO(){

    }

    public String getName() {
        return name;
    }

    public int getPhoneNumber() {
        return PhoneNumber;
    }

    public int getSNSNumber() {
        return SNSNumber;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public String getSex() {
        return sex;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoneNumber(int phonenumber) {
        this.PhoneNumber = phonenumber;
    }

    public void setSNSNumber(int snsnumber) {
        this.SNSNumber = snsnumber;
    }

    public void setBirthdate(Date birthdate){
        this.birthdate = birthdate;
    }

    public void setSex(String Sex) {
        this.sex = Sex;
    }

# 6. Integration and Demo

_In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system._

# 7. Observations

_In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work._
