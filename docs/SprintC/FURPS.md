# Supplementary Specification (FURPS+)

## Functionality

_Specifies functionalities that:_

- _are common across several US/UC;_
- _are not related to US/UC, namely: Audit, Reporting and Security._


Authentication with a password is necessary to use the application, holding seven alphanumeric characters, including three capital letters and two digits.
Only the nurses are allowed to access all user’s health data.

## Usability 

_Evaluates the user interface. It has several subcategories,
among them: error prevention; interface aesthetics and design; help and
documentation; consistency and standards._

The user interface must be simple, intuitive and consistent.
The system of the application must be able to easily support not only Covid-19 but also any pandemic that requires a mass vaccination or any other type of vaccination required.
The vaccination must happen according to the health and age of the user.    
A manual is provided by the company to help the user understand how to use the program.

## Reliability
_Refers to the integrity, compliance and interoperability of the software. The requirements to be considered are: frequency and severity of failure, possibility of recovery, possibility of prediction, accuracy, average time between failures._


## Performance
_Evaluates the performance requirements of the software, namely: response time, start-up time, recovery time, memory consumption, CPU usage, load capacity and application availability._


## Supportability
_The supportability requirements gathers several characteristics, such as:
testability, adaptability, maintainability, compatibility,
configurability, instability, scalability and more._ 


The unit tests should be implemented using the JUnit 5 framework.  
The JaCoCo plugin should be used to generate the coverage report.  
All the images/figures produced during the software development process should be recorded in SVG format.  
The application must support the English and Portuguese language.   
The application has its own unit tests to test the various implemented methods.  
The application supports various pandemics. 
The software is made to be commercialized.


## +

### Design Constraints
_Specifies or constraints the system design process. Examples may include: programming languages, software process, mandatory standards/patterns, use of development tools, class library, etc._


Programming Language: Java, using IntelliJ IDE or Visual Studio Code  
Platform for the graphical interface: JavaFX 11

### Implementation Constraints
_Specifies or constraints the code or construction of a system such as: mandatory standards/patterns, implementation languages,
database integrity, resource limits, operating system._

The team should adopt recognized coding standards: E.g. CamelCase
Programming Language: Java, using IntelliJ IDE or Visual Studio Code
The comments have to be written in JavaDoc.
The images are stored in svg format.


### Interface Constraints
_Specifies or constraints the features inherent to the interaction of the
system being developed with other external systems._

The user interface must be simple and intuitive, supporting at least portuguese and english languages.


### Physical Constraints
_Specifies a limitation or physical requirement regarding the hardware used to house the system, as for example: material, shape, size or weight._



