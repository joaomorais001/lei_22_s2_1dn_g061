# US9 - To register a vaccination center to respond to a certain pandemic

## 1. Requirements Engineering


### 1.1. User Story Description

As a administrator, i want to register a vaccination center to respond to a certain pandemic

### 1.2. Customer Specifications and Clarifications 

**Specification 1**

Question: "How will be a center registered in the system (what data must be asked)?"
Answer: This information is available in the project description. When asking a question the student should show to be aware of all the information that the client has provided.(name,address,Phone number,e-maill,fax number,website,opening and closing hours,slot duration,maximum number of vaccines per slot.)



### 1.3. Acceptance Criteria
**AC 1**

**AC 2**

### 1.4. Found out Dependencies

No dependencies were found.

### 1.5 Input and Output Data



**Input Data**

Typed data:name,address,Phone number,e-maill,fax number,website,opening and closing hours,slot duration,maximum number of vaccines per slot.

Selected data: (none)


**Output Data**

The vaccinationCenter id added to the list.


### 1.6. System Sequence Diagram (SSD)



![US9-SSD](US9-SSD.svg)


### 1.7 Other Relevant Remarks

Nothing to reffer.


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US9-DM](US9-DM.svg)

### 2.2. Other Remarks

Nothing to reffer.


## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer     | Justification (with patterns)    |
|:---------------| :------------------------------------------ |:-----------|:---------------------------------|
| Step 1         | Starts the registration of a new vaccination center   | Controller | Creator                          |
| Step 2         | ...saving the input data?                   | Store      | Saving the information           |
| Step 3         | ...validating the data locally?             | vaccination center   |                                  |
| Step 4         | ...saving the created vaccination center?             | Store      |                                  |
| Step 5         | ...informing operation success?             | UI         | Responsible for user interaction |


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 


* VaccinationCenter
* RegisterVacCenterStore

Other software classes (i.e. Pure Fabrication) identified: 
 * RegisterVacCenterUI  
 * RegisterVacCenterController

## 3.2. Sequence Diagram (SD)


![US9-SD](US9-SD.svg)

## 3.3. Class Diagram (CD)


![US9-CD](US9-CD.svg)

# 4. Tests 

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** 

# 5. Construction (Implementation)

# 6. Integration and Demo 

The only feature that was modified was the Admin UI.


# 7. Observations

