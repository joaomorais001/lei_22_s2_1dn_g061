# US3 - To register a SNS user

## 1. Requirements Engineering


### 1.1. User Story Description

As a receptionist, I want to register a SNS user. 

### 1.2. Customer Specifications and Clarifications 

**Specification 1**

Question: "Regarding US3: "As a receptionist, I want to register an SNS User.", What are the necessary components in order to register an SNS User?"

Answer:
The attributes that should be used to describe a SNS user are: Name, Address, Sex, Phone Number, E-mail, Birth Date, SNS User Number and Citizen Card Number.
The Sex attribute is optional. All other fields are required.
The E-mail, Phone Number, Citizen Card Number and SNS User Number should be unique for each SNS user.


### 1.3. Acceptance Criteria
**AC 1**

The SNS user must become a system user. The "auth" component available on the repository must be reused (without modifications).

**AC 2**

All those who wish to use the application must be authenticated with a password holding seven alphanumeric characters, including three capital letters and two digits.
### 1.4. Found out Dependencies

No dependencies were found.

### 1.5 Input and Output Data



**Input Data**

Typed data: citizen card number,address, Health number (SNS Number) number, birth date, sex, phone number, e-mail and name.

Selected data: (none)


**Output Data**

The user id added to the list.


### 1.6. System Sequence Diagram (SSD)



![US3-SSD](US3-SSD.svg)


### 1.7 Other Relevant Remarks

Nothing to reffer.


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US3-DM](US3-DM.svg)

### 2.2. Other Remarks

Nothing to reffer.


## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer     | Justification (with patterns)    |
|:---------------| :------------------------------------------ |:-----------|:---------------------------------|
| Step 1         | Starts the registration of a new SNS user   | Controller | Creator                          |
| Step 2         | ...saving the input data?                   | Store      | Saving the information           |
| Step 3         | ...validating the data locally?             | SNS user   |                                  |
| Step 4         | ...saving the created SNS user?             | Store      |                                  |
| Step 5         | ...informing operation success?             | UI         | Responsible for user interaction |


### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 


* SNSUser
* RegisterSNSUserStore

Other software classes (i.e. Pure Fabrication) identified: 
 * RegisterSNSUserUI  
 * RegisterSNSUserController

## 3.2. Sequence Diagram (SD)


![US3-SD](US3-SD.svg)

## 3.3. Class Diagram (CD)


![US3-CD](US3-CD.svg)

# 4. Tests 
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.* 

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Example class with null values. 

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.* 

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.* 

# 6. Integration and Demo 

The only feature that was modified was the receptionist UI.


# 7. Observations

