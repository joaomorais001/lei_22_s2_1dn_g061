# US5 - As a center coordinator, I intend to analyze the performance of a center.

## 1. Requirements Engineering

_In this section, it is suggested to capture the requirement description and specifications as provided by the client as well as any further clarification on it. It is also suggested to capture the requirements acceptance criteria and existing dependencies to other requirements. At last, identify the involved input and output data and depicted an Actor-System interaction in order to fulfill the requirement._

### 1.1. User Story Description

As a center coordinator, I intend to analyze the performance of a center.

### 1.2. Customer Specifications and Clarifications

**Customer Specifications**
>The goal of this US is to implement a procedure that, for a specific day, and time intervals of m minutes (for m = 30, 20, 10, 5, and l , for example) chosen by the coordinator of the center, with a daily work from 8 a.m. to 8 p.m., the procedure creates a list of length 720/m (respectively, lists of length 24, 36, 72, 144, 720), where the i-th value of the list is the difference between the number of new clients arriving and the number of clients leaving the center in that i-th time interval.

>Then, the application should implement a brute-force algorithm (an algorithm which examines all the contiguous sublists of the input one) to determine the contiguous sublist with maximum sum. The output should be the input list, the maximum sum contiguous sublist and its sum, and the time interval corresponding to this contiguous sublist (for example, for time intervals of 1 hour, a list of length 12 is created; if, for example, the maximum sum contiguous sublist starts at the 2nd and ends at the 5th entries of the input list, with a sum s, it means that the vaccination center was less effective in responding from 9 a.m. to I p.m, with s clients inside the center).

>The performance analysis should be documented in the application user manual (in the annexes) that must be delivered with the application. Also in the user manual, the implemented algorithm should be analyzed in terms of its worst-case time complexity. The complexity analysis must be accompanied by the observation of the execution time of the algorithms, and it should be compared to a benchmark algorithm provided, for inputs of variable size m, with m = 24, 36, 72, 144, 720, in order to observe the asymptotic behavior.

**Customer Clarifications**

> **Question:**
> In US 16, should the coordinator have the option to choose which algorithm to run (e.g. via a configuration file or while running the application) in order to determine the goal sublist, or is the Benchmark Algorithm strictly for drawing comparisons with the Bruteforce one?
>
> **Answer:**
> The algorithm to run should be defined in a configuration file.

> **Question:**
> From the Sprint D requirements it is possible to understand that we ought to implement a procedure that creates a list with the differences between the number of new clients arriving and the number of leaving clients for each time interval. My question then is, should this list strictly data from the legacy system (csv file from moodle which is loaded in US17), or should it also include data from our system?
>
> **Answer:**
> US 16 is for all the data that exists in the system.

> **Question:**
> The file loaded in US17 have only one day to analyse or it could have more than one day(?) and in US16 we need to select the day to analyse from 8:00 to 20:00
>
> **Answer:**
> The file can have data from more than one day. In US16 the center coordinator should select the day for which he wants to analyse the performance of the vaccination center.

> **Question:**
> I would like to know if we could strict the user to pick only those intervals (m) (i.e. 1, 5, 10, 20, 30) as options for analyzing the performance of a center, since picking intervals is dependent on the list which is 720/m (which the length is an integer result). If we let the user pick an interval that results in a non-integer result, this will result in an invalid list since some data for the performance analysis will be lost. Can you provide a clarification on this situation?
>
> **Answer:**
> The user can introduce any interval value. The system should validate the interval value introduced by the user.

> **Question:**
> I would like to ask that if to analyse the performance of a center, we can assume (as a pre requirement) that the center coordinator was already attributed to a specific vaccination center and proceed with the US as so (like the center coordinator does not have to choose at a certain point where he is working. This is already treated before this US happens). Could you clarify this?
>
> **Answer:**
> A center coordinator can only coordinate one vaccination center. The center coordinator can only analyze the performance of the center that he coordinates.

> **Question:**
> Is the time of departure of an SNS user the time he got vaccinated plus the recovery time or do we have another way of knowing it?
>
> **Answer:**
> The time of departure of an SNS user is the time he got vaccinated plus the recovery time.


### 1.3. Acceptance Criteria

- **AC01:** The output should be the input list, the maximum sum contiguous sublist and its sum, and the time interval corresponding to the sublist. 
- **AC02** The center coordinator can choose any time interval.

### 1.4. Found out Dependencies

- Has a dependency from us17, which imports files for analysis.
- Has a dependency from us4, which registers the entry of an user.
- Has a dependency from us8, which registers the departure of an user.

### 1.5 Input and Output Data

**Input data:**

- Date
- Time interval

**Output data:**

- Center performance analysis

### 1.6. System Sequence Diagram (SSD)

![US16-SSD](US16-SSD.svg)

### 1.7 Other Relevant Remarks

In contrary to the info that was given for the receptionist/nurse the center coordinator can only be associated with one vaccination center.

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt

![US16-MD](US16-MD.svg)

### 2.2. Other Remarks

_Use this section to capture some aditional notes/remarks that must be taken into consideration into the design activity. In some case, it might be usefull to add other analysis artifacts (e.g. activity or state diagrams)._

## 3. Design - User Story Realization

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID                                             | Question: Which class is responsible for...                                                                      | Answer                       | Justification (with patterns)                                |
|:-----------------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------|:-----------------------------|:-------------------------------------------------------------|
| Step 1: starts to analyze the performance of a center   		 | ... interacting with the actor?							                                                                           | AnalysePerformanceUI         | Pure Fabrication                                             |
|                                                            | ... coordinating the US?                                                                                         | AnalysePerformanceController | Controller                                                   |
|                                                            | ... having the email of the center coordinator?                                                                  | UserSession                  | IE: the object has its own data                              |
|                                                            | ... having the vaccination center where the center coordinator works?                                            | EmployeeStore                | IE: records the vaccination center of the center coordinator |
| 		                                                         | ... checking if a center has entry records?							                                                               | EntryRecords                 | IE: records all the entries                                  |
| 		                                                         | ... checking if a center has departure records?							                                                           | DepartureRecords             | IE: records all the departures                               |
| Step 2: requests the date  		                              | n/a							                                                                                                       |                              |                                                              |
| Step 3: types requested data                               | ... validating the date?                                                                                         | Utils                        | Pure Fabrication                                             |
| 		                                                         | ... providing the entry records from a center in the requested date?							                                      | EntryRecords                 | IE: records all the entries                                  |
|                                                            | ... providing the departure records from a center in the requested date?                                         | DepartureRecords             | IE: records all the departures                               |
| Step 4: requests the time interval  		                     | n/a							                                                                                                       |                              |                                                              |
| Step 5: types requested data                               | ... validating the time interval?                                                                                | Utils                        | Pure Fabrication                                             |
|                                                            | ... calculating the differences between the entries and departures from a center of the requested time interval? | AnalysePerformanceController | Controller                                                   |
|                                                            | ... calculating the maximum sum contiguous sublist from the differences?                                         | AnalysePerformanceController | Controller                                                   |
|                                                            | ... calculating the sum?                                                                                         | AnalysePerformanceController | Controller                                                   |
| Step 6: Asks for confirmation 		                              | n/a							                                                                                                       |                              |                                                              |
| Step 7: confirms choices  		                              | n/a							                                                                                                       |                              |                                                              |
| Step 8: shows the results of the analysis  		              | ... showing the analysis results?							                                                                         | AnalysePerformanceUI   | IE: is responsible for user interactions                     |              
### Systematization

According to the taken rationale, the conceptual classes promoted to software classes are:

- EntryRecords
- DepartureRecords
- VacCenter
- Algorithms

Other software classes (i.e. Pure Fabrication) identified:

- UserSession
- AnalysePerformanceUI
- AnalysePerformanceController

## 3.2. Sequence Diagram (SD)

![US16-SD](US16-SD.svg)

## 3.3. Class Diagram (CD)

![US16-CD](US16-CD.svg)

# 4. Tests

_In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling._

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Tests if a entry and departure records are being added correctly

    public void ensureEntryandDeparturePopulation(){
        VacCenter center = new VacCenter("Centro", "Rua","0","0","centro.pt", 8, 20, 5,10);

        center.addEntryRecord(LocalDateTime.now(), 123456789);
        center.addDepartureRecord(LocalDateTime.now(), 123456789);

        boolean res = center.getEntryRecords().size() == center.getDepartureRecords().size();

        Assertions.assertTrue(res);
    }

**Test 2:** Tests if records from the right dates are being returned

    public void ensureReturnRightDateRecords(){
        VacCenter center = new VacCenter("Centro", "Rua","0","0","centro.pt", 8, 20, 5,10);
        AnalysePerformanceController controller = new AnalysePerformanceController();

        center.addEntryRecord(LocalDateTime.now(), 123456789);
        center.addDepartureRecord(LocalDateTime.now(), 123456789);

        List<EntryRecords> entries = controller.getEntryRecordsFromDate(LocalDate.now(), center.getEntryRecords());
        List<DepartureRecords> departures = controller.getDepartureRecordsFromDate(LocalDate.now(), center.getDepartureRecords());

        Assertions.assertEquals(entries.size(), departures.size());
    }

**Test 3:** Tests if records from wrong dates are being returned

    public void ensureDoesntReturnWrongDateRecords(){
        VacCenter center = new VacCenter("Centro", "Rua","0","0","centro.pt", 8, 20, 5,10);
        AnalysePerformanceController controller = new AnalysePerformanceController();

        String date = "10/10/2022 08:00";
        String date2 = "20/10/2022 10:00";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

        LocalDateTime localDateTime = LocalDateTime.parse(date, formatter);
        LocalDate localDate = LocalDate.parse(date2, formatter);

        center.addEntryRecord(localDateTime, 123456789);
        center.addDepartureRecord(localDateTime, 123456789);

        List<EntryRecords> entries = controller.getEntryRecordsFromDate(localDate, center.getEntryRecords());
        List<DepartureRecords> departures = controller.getDepartureRecordsFromDate(localDate, center.getDepartureRecords());

        boolean res = (entries.size() == 0 && departures.size() == 0);

        Assertions.assertTrue(res);
    }


_It is also recommended to organize this content by subsections._

# 5. Construction (Implementation)

## AnalysePerformanceController

    private final Company company;
    private final AuthFacade authFacade;
    private final EmployeeStore store;

    public AnalysePerformanceController() {
        this.company = App.getInstance().getCompany();
        this.authFacade = company.getAuthFacade();
        this.store = company.getEmployeeStore();
    }

    public List<EntryRecords> getEntryRecords(VacCenter center) {
        return center.getEntryRecords();
    }

    public List<DepartureRecords> getDepartureRecords(VacCenter center) {
        return center.getDepartureRecords();
    }

    public void getImportedRecords(VacCenter center) {

        ArrayList<String> data = center.getLegacydata();
        String[] dataInfo;
        String[] infoArrival;
        String[] infoDeparture;

        for (int i = 0; i < data.size(); i++) {
            boolean existsEntry = false, existsDeparture = false;

            dataInfo = data.get(i).split(",|]");

            String strArrival = dataInfo[7].trim();
            String strDeparture = dataInfo[9].trim();

            infoArrival = strArrival.split(" ");
            infoDeparture = strDeparture.split(" ");

            String[] arrivalDate = infoArrival[0].trim().split("/"), departureDate = infoDeparture[0].trim().split("/");
            String dayArrival = arrivalDate[1], dayDeparture = departureDate[1];
            String monthArrival = arrivalDate[0], monthDeparture = departureDate[0];
            String hourArrival = infoArrival[1].trim(), hourDeparture = infoDeparture[1].trim();
            String finalArrivalDate, finalDepartureDate;

            if (dayArrival.length() == 1) {
                dayArrival = "0".concat(dayArrival);
            }
            if (monthArrival.length() == 1) {
                monthArrival = "0".concat(monthArrival);
            }
            if (hourArrival.length() == 4) {
                hourArrival = "0".concat(hourArrival);
            }

            if (dayDeparture.length() == 1) {
                dayDeparture = "0".concat(dayDeparture);
            }
            if (monthDeparture.length() == 1) {
                monthDeparture = "0".concat(monthDeparture);
            }
            if (hourDeparture.length() == 4) {
                hourDeparture = "0".concat(hourDeparture);
            }

            finalArrivalDate = monthArrival + "/" + dayArrival + "/" + arrivalDate[2];
            finalDepartureDate = monthDeparture + "/" + dayDeparture + "/" + departureDate[2];

            strArrival = finalArrivalDate + " " + hourArrival;
            strDeparture = finalDepartureDate + " " + hourDeparture;

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");

            LocalDateTime entryTime = LocalDateTime.parse(strArrival, formatter);

            LocalDateTime departureTime = LocalDateTime.parse(strDeparture, formatter);

            int SNSnumber = Integer.parseInt(dataInfo[0].substring(1));

            for (EntryRecords records : center.getEntryRecords()) {
                if (records.getSnsUser() == SNSnumber && records.getEntryRecord().equals(entryTime)) {
                    existsEntry = true;
                    break;
                }
            }

            for (DepartureRecords records : center.getDepartureRecords()) {
                if (records.getSnsUser() == SNSnumber && records.getDepartureRecord().equals(departureTime)) {
                    existsDeparture = true;
                    break;
                }
            }

            if (!existsEntry) {
                center.addEntryRecord(entryTime, SNSnumber);
            }

            if (!existsDeparture) {
                center.addDepartureRecord(departureTime, SNSnumber);
            }
        }
    }

    public List<EntryRecords> getEntryRecordsFromDate(LocalDate date, List<EntryRecords> list) {
        List<EntryRecords> updatedList = new ArrayList<>();

        for (EntryRecords record : list) {
            if (record.getEntryRecord().toLocalDate().equals(date)) {
                updatedList.add(record);
            }
        }

        return updatedList;
    }

    public List<DepartureRecords> getDepartureRecordsFromDate(LocalDate date, List<DepartureRecords> list) {
        List<DepartureRecords> updatedList = new ArrayList<>();

        for (DepartureRecords record : list) {
            if (record.getDepartureRecord().toLocalDate().equals(date)) {
                updatedList.add(record);
            }
        }

        return updatedList;
    }

    public String getUserEmail() {
        UserSession info = authFacade.getCurrentUserSession();

        return info.getUserId().toString();
    }

    public VacCenter getVaccinationCenter(String email) {

        VacCenter center = null;

        List<Employee> employeeList = store.getEmployees();

        for (Employee employee : employeeList) {
            if (email.equals(employee.getEmail())) {
                center = employee.getVacCenter();
            }
        }
        return center;
    }

    public String readLoginInfoFile() throws FileNotFoundException {

        File file = new File("LoginInfo.txt");
        Scanner reader = new Scanner(file);
        String email = reader.nextLine();
        reader.close();

        return email;
    }

    private long getTimeInMS(LocalDateTime dateTime) {
        long timeS = TimeUnit.HOURS.toMillis(dateTime.getHour()) + TimeUnit.MINUTES.toMillis(dateTime.getMinute())
                + TimeUnit.SECONDS.toMillis(dateTime.getSecond());

        return timeS;
    }

    private static Time[] timeInt(int timeInterval) {
        Time[] interval = new Time[720 / timeInterval];
        long millisSum = 0;

        for (int i = 0; i < interval.length; i++) {
            Time time = new Time(8, 0, 0);
            millisSum = millisSum + (timeInterval * 60000);
            interval[i] = time;
            time.setTime(time.getTime() + millisSum);
        }
        return interval;
    }

    private int[] entryRecordInterval(List<EntryRecords> entryRecords, int timeInterval) {

        int[] recordsTimeInterval = new int[720 / timeInterval];
        Time[] interval = new Time[720 / timeInterval];
        long millisSum = 0;

        for (int i = 0; i < interval.length; i++) {
            Time time = new Time(8, 0, 0);
            millisSum = millisSum + (timeInterval * 60000);
            interval[i] = time;
            time.setTime(time.getTime() + millisSum);
        }

        for (EntryRecords record : entryRecords) {
            Time actualTime = new Time(record.getEntryRecord().getHour(), record.getEntryRecord().getMinute(), record.getEntryRecord().getSecond());
            for (int i = 0; i < interval.length; i++) {
                if ((actualTime.getTime()) < interval[i].getTime()) {
                    recordsTimeInterval[i]++;
                    break;
                }
            }
        }

        return recordsTimeInterval;
    }

    private int[] departureRecordInterval(List<DepartureRecords> departureRecords, int timeInterval) {
        int[] recordsByTimeInterval = new int[720 / timeInterval];
        Time[] hoursInterval = new Time[720 / timeInterval];
        long millis = 0;

        for (int i = 0; i < hoursInterval.length; i++) {
            Time time = new Time(8, 0, 0);
            millis = millis + (timeInterval * 60000);
            hoursInterval[i] = time;
            time.setTime(time.getTime() + millis);
        }

        for (DepartureRecords record : departureRecords) {
            Time actualTime = new Time(record.getDepartureRecord().getHour(), record.getDepartureRecord().getMinute(), record.getDepartureRecord().getSecond());
            for (int i = 0; i < hoursInterval.length; i++) {
                if ((actualTime.getTime()) < hoursInterval[i].getTime()) {
                    recordsByTimeInterval[i]++;
                    break;
                }
            }
        }

        return recordsByTimeInterval;
    }

    public int[] calculateDifferencesEntryDeparture(
            List<EntryRecords> entryRecords, List<DepartureRecords> departureRecords, int timeInterval) {

        int[] entryRecord = entryRecordInterval(entryRecords, timeInterval);
        int[] departureRecord = departureRecordInterval(departureRecords, timeInterval);

        int[] difference = new int[departureRecord.length];

        for (int i = 0; i < difference.length; i++) {
            difference[i] = entryRecord[i] - departureRecord[i];
        }

        return difference;
    }

    public String getAlgorithm() {
        String alg = "";
        try {
            FileReader file = new FileReader("config.properties");
            Properties prop = new Properties();
            prop.load(file);
            alg = prop.getProperty("Algorithm");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return alg;
    }

    public int[] calculateMaxSumSub(String alg, int[] seq) {
        if (alg.equals("1")) {
            return Algorithms.BruteForceAlgorithm(seq);
        } else {
            return Algorithms.BenchmarkAlgorithm(seq);
        }
    }

    public int calculateMaxSum(int[] seq) {
        int sum = 0;
        for(Integer val: seq) {
            sum += val;
        }
        return sum;
    }
    
    private static int[] startEndSublist(int[] seq) {
        int maxSoFar = 0;
        int maxEndingHere = 0;
        int startMaxSoFar = 0;
        int endMaxSoFar = 0;
        int startMaxEndingHere = 0;
        for (int i = 0; i < seq.length; ++i) {
            final int elem = seq[i];
            final int endMaxEndingHere = i + 1;
            if (maxEndingHere + elem < 0) {
                maxEndingHere = 0;
                startMaxEndingHere = i + 1;
            }
            else {
                maxEndingHere += elem;
            }
            if (maxSoFar < maxEndingHere) {
                maxSoFar = maxEndingHere;
                startMaxSoFar = startMaxEndingHere;
                endMaxSoFar = endMaxEndingHere;
            }
        }

        return new int[]{startMaxSoFar, endMaxSoFar-1};
    }

    public String[] getPeriod(int[] differences, int timeInterval) {

        Time[] hoursInterval = timeInt(timeInterval);
        int[] startAndEnd = startEndSublist(differences);

        hoursInterval[startAndEnd[0]].setTime(hoursInterval[startAndEnd[0]].getTime() - timeInterval*60000);
        return new String[]{hoursInterval[startAndEnd[0]].toString(), hoursInterval[startAndEnd[1]].toString()};
    } 

## Algorithms

        public static int[] BenchmarkAlgorithm(int[] seq) {
        int maxSoFar = 0;
        int maxEndingHere = 0;
        int startMaxSoFar = 0;
        int endMaxSoFar = 0;
        int startMaxEndingHere = 0;

        for(int i = 0; i < seq.length; ++i) {
            int elem = seq[i];
            int endMaxEndingHere = i + 1;
            if (maxEndingHere + elem < 0) {
                maxEndingHere = 0;
                startMaxEndingHere = i + 1;
            } else {
                maxEndingHere += elem;
            }

            if (maxSoFar < maxEndingHere) {
                maxSoFar = maxEndingHere;
                startMaxSoFar = startMaxEndingHere;
                endMaxSoFar = endMaxEndingHere;
            }
        }
        return Arrays.copyOfRange(seq, startMaxSoFar, endMaxSoFar);
    }

    public static int[] BruteForceAlgorithm(int[] seq) {
        if (seq.length <= 1) {
            return seq;
        }

        int maxSoFar = Integer.MIN_VALUE;

        int maxEndingHere = 0;

        int start = 0, end = 0;

        int beg = 0;

        for (int i = 0; i < seq.length; i++) {
            maxEndingHere = maxEndingHere + seq[i];

            if (maxEndingHere < seq[i]) {
                maxEndingHere = seq[i];
                beg = i;
            }

            if (maxSoFar < maxEndingHere) {
                maxSoFar = maxEndingHere;
                start = beg;
                end = i;
            }
        }
        return Arrays.copyOfRange(seq, start, end + 1);
    }

## AnalysePerformanceUI

    private AnalysePerformanceController controller;

    private boolean validation;
    private String date, alg;
    private int timeInterval;

    public AnalysePerformanceUI() {
        this.controller = new AnalysePerformanceController();
    }

    public void run() {

        VacCenter center = controller.getVaccinationCenter(controller.getUserEmail());

        controller.getImportedRecords(center);

        System.out.println("\nCenter Perfomance analysis:");

        do {
            date = Utils.readLineFromConsole("Type the pretended date: (MM/dd/yyyy)");
            try {
                Utils.checkDateFormat(date);
                validation = true;
            } catch (Exception e) {
                validation = false;
                System.out.println(e.getMessage());
            }
        } while (!validation);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate localDate = LocalDate.parse(date, formatter);

        List<EntryRecords> entryRecords = controller.getEntryRecordsFromDate(localDate,
                controller.getEntryRecords(center));
        List<DepartureRecords> departureRecords = controller.getDepartureRecordsFromDate(localDate,
                controller.getDepartureRecords(center));

        if (entryRecords.size() == 0 || departureRecords.size() == 0) {
            System.out.println("\nNo records for the chosen date!");

        } else {
            do {
                timeInterval = Utils.readIntegerFromConsole("Type the pretended time interval:");
                try {
                    String timeIntervalString = String.valueOf(timeInterval);
                    Utils.checkTimeIntervalFormat(timeIntervalString);
                    validation = true;
                } catch (Exception e) {
                    validation = false;
                    System.out.println(e.getMessage());
                }
            } while (!validation);

            int[] differences = controller.calculateDifferencesEntryDeparture(entryRecords, departureRecords,
                    timeInterval);

            alg = controller.getAlgorithm();

            int[] sumSub = controller.calculateMaxSumSub(alg, differences);

            int sum = controller.calculateMaxSum(sumSub);

            String[] period = controller.getPeriod(differences, timeInterval);

            System.out.println("\nDifferences: " + Arrays.toString(differences));
            System.out.println("\nMaximum Sum Sublist: " + Arrays.toString(sumSub));
            System.out.println("\nMaximum Sum: " + sum);
            System.out.println("\nPeriod: " + date + " " + period[0] + ", " + date + " " + period[1]);
        }
    }

## PerformanceAnalysisGUI

    @FXML
    private Button confirmButton;

    @FXML
    private Button btn_Back;


    @FXML
    private DatePicker datePicker;

    @FXML
    private TextField timeInterval;

    @FXML
    private Label algorithm;

    @FXML
    private Label dateErrorMessage;

    @FXML
    private Label timeErrorMessage;

    @FXML
    private Label sizeErrorMessage;

    private static Runnable previous;

    private AnalysePerformanceController controller;

    private boolean dateValidation, timeValidation, sizeValidation;
    private String aux, alg;

    public PerformanceAnalysisGUI() {
        controller = new AnalysePerformanceController();
    }

    public static void setPrevious(Runnable previous) {
        PerformanceAnalysisGUI.previous = previous;
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/PerformanceAnalysis.fxml")));
        stage.setTitle("Performance Analysis");
        stage.setScene(new Scene(root, 700, 500));
        stage.setMaxHeight(500);
        stage.setMaxWidth(700);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    public void initialize() throws IOException {

        FileReader file = new FileReader("config.properties");
        Properties prop = new Properties();
        prop.load(file);
        alg = prop.getProperty("Algorithm");

       if(alg.equals("1")){
        aux = "Brute-Force Algorithm";
       }else{
        aux = "Benchamrk Algorithm";
       }

       algorithm.setText(aux);
    }

    @Override
    public void run() {
        JavaFX_Launcher.myLaunch(getClass());
    }

    @FXML
    void handleConfirmButton(ActionEvent event) throws Exception {

        System.out.println(algorithm.getText());

        String email = controller.readLoginInfoFile();
        VacCenter center = controller.getVaccinationCenter(email);

        controller.getImportedRecords(center);

        try {
            Utils.checkLocalDateFormat(datePicker.getValue());
            dateErrorMessage.setText("");
            dateValidation = true;
        } catch (Exception e) {
            dateErrorMessage.setText(e.getMessage());
            dateValidation = false;
        }

        try {
            Utils.checkTimeIntervalFormat(timeInterval.getText());
            timeErrorMessage.setText("");
            timeValidation = true;
        } catch (Exception e) {
            timeErrorMessage.setText(e.getMessage());
            timeValidation = false;
        }

        List<EntryRecords> entryRecords = controller.getEntryRecordsFromDate(datePicker.getValue(),
                controller.getEntryRecords(center));
        List<DepartureRecords> departureRecords = controller.getDepartureRecordsFromDate(datePicker.getValue(),
                controller.getDepartureRecords(center));

        if (entryRecords.size() == 0 || departureRecords.size() == 0) {
            sizeErrorMessage.setText("The selected date has no records!");
            sizeValidation = false;
        } else {
            sizeErrorMessage.setText("");
            sizeValidation = true;
        }

        if (timeValidation == true && dateValidation == true && sizeValidation == true) {

            int[] differences = controller.calculateDifferencesEntryDeparture(entryRecords, departureRecords, Integer.parseInt(timeInterval.getText()));
            int[] sumSub = controller.calculateMaxSumSub(alg, differences);
            int sum = controller.calculateMaxSum(sumSub);
            String[] period = controller.getPeriod(differences, Integer.parseInt(timeInterval.getText()));

            AnalysisResults results = new AnalysisResults(differences, sumSub, sum, datePicker.getValue(),
                    Integer.parseInt(timeInterval.getText()), period);
                    
            redirectToResults(results);
        }

    }

    public void back() {
        Stage stage = (Stage) btn_Back.getScene().getWindow();
        stage.close();
        previous.run();
    }

    private void redirectToResults(AnalysisResults results) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PerformanceAnalysisResults.fxml"));
        loader.setController(new PerformanceAnalysisResultsGUI(results));
        Parent root = (Parent) loader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Performance Analysis Results");
        stage.setScene(new Scene(root, 700, 500));
        stage.setResizable(false);
        stage.show();
    }

## PerformanceAnalysisResultsGUI

    private AnalysisResults results;

    @FXML
    private Label date;

    @FXML
    private Label time;

    @FXML
    private TextArea differences;

    @FXML
    private TextArea maxSum;

    @FXML
    private Label sum;

    @FXML
    private Label period;

    
    public PerformanceAnalysisResultsGUI(AnalysisResults results) {
        this.results = results;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        
        this.date.setText(String.valueOf(this.results.getDate().format(formatter)));
        this.time.setText(String.valueOf(this.results.getTimeInterval()));
        this.differences.setText(Arrays.toString(this.results.getDifferences()));
        this.maxSum.setText(Arrays.toString(this.results.getMaximumSumSublist()));
        this.sum.setText(String.valueOf(this.results.getMaximumSum()));
        this.period.setText(String.valueOf(this.results.getDate().format(formatter) + " " + results.getPeriod()[0] + ", " + results.getDate().format(formatter) + " " + results.getPeriod()[1]));
    }

## CenterCoordinatorGUI

    private static Runnable previous;

    @FXML private Button btn_Back;
    @FXML private Button btn2;

    @FXML private Button btn21;
    @FXML
    private Button btn1;


    public CenterCoordinatorGUI() {

    }

    public static void setPrevious(Runnable previous) { CenterCoordinatorGUI.previous = previous; }

    @Override
    public void run() {
        JavaFX_Launcher.myLaunch(getClass());
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/CenterCoordinator.fxml")));
        stage.setTitle("Center Coordinator Menu");
        stage.setScene(new Scene(root, 700, 500));
        stage.setMaxHeight(500);
        stage.setMaxWidth(700);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();

    }

    public void analysePerformance() {
        PerformanceAnalysisGUI.setPrevious(this);
        Stage stage = (Stage) btn1.getScene().getWindow();
        stage.close();
        new PerformanceAnalysisGUI().run();
    }

    public void importData() {
        Stage stage = (Stage) btn2.getScene().getWindow();
        stage.close();
        SortLegacyDataGUI.setPrevious(this);
        new SortLegacyDataGUI().run();
    }

    public void exportData() {
        Stage stage = (Stage) btn21.getScene().getWindow();
        stage.close();
        VacinationStatisticsGUI.setPrevious(this);
        new VacinationStatisticsGUI().run();
    }

    public void back() {
        Stage stage = (Stage) btn_Back.getScene().getWindow();
        stage.close();
        previous.run();
    }

# 6. Integration and Demo

This us requires several features from older us's and some developed ate the same time, that register arrivals and departures from the center and import data to the system.

# 7. Observations

The algorithm is chosen in a configuration file called *config.properties*, and can 
be changed by the user easily, using numbers to make it simpler than tying the whole algorithm name.
Both implemented algorithms are very similar in the running time, with a slight advantage to the Brute-Force that is slightly quicker.