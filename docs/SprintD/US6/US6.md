# US02 - Schedule a vaccination by the receptionist

## 1. Requirements Engineering

### 1.1. User Story Description

*DGS wants to record daily the total number of people vaccinated in each vaccination center.*

### 1.2. Customer Specifications and Clarifications 
>**Question 1**:
>"Dear client, I'd like to clarify something, should we implement some sort of message for when the file is saved or warning if there were any errors saving the file using JavaFX or use JavaFX in any other away in US06? Also, should the time be edited directly in the configuration file or should we have a way for an administrator or some other employee to change it."  
> 
>**Answer:**
>"1- No. The user story runs automatically without user interaction.
2- I already answered this question. The algorithm should run automatically at a time defined in a configuration file and should register the date, the name of the vaccination center and the total number of vaccinated users.
Please pay attention to the client answers and study ESOFT."

>**Question 2**:
>"After asking you to be more clear with your answer to my previous questions, you said that we can either access directly the "vaccinations report" file or create the option for center coordinators and administrators to view it's content, is this correct?"
>
>**Answer:**
>"You misunderstood, I didn't say that. The file should be available in the file system and anyone having access to the file system can read the file contents."

>**Question 3**:
>"In this situation, the system automatically prints what is requested so we dont need any user input.
So, my question is: Do we need to create an UI for this US? ">
> 
>**Answer:**
>"Answer: No. Please study ESOFT!"

>**Question 4**:
>"Should the Company choose first the vaccination center that wants to analyze or should the program show the information of all the vaccination centers?"
> 
>**Answer:**
>"Please read carefully the USs requirements and discuss them with your team. The application should show the information for all vaccination centers."

### 1.3. Acceptance Criteria
**AC01:**
*The algorithm should run automatically at a time defined in
a configuration file and should register the date, the name of the vaccination center
and the total number of vaccinated users.*

### 1.4. Found out Dependencies

Depdency from: 

US8 SNS users in recovery room;

US9 Vaccine Centers.



### 1.5 Input and Output Data


**Output Data:**
The output is a file with the requested information (Vaccines administrated by day in each vaccination center)

### 1.6. System Sequence Diagram (SSD)

Not Used.


### 1.7 Other Relevant Remarks

None

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US6-DM](US6-DM.svg)

### 2.2. Other Remarks

None


## 3. Design - User Story Realization 

### 3.1. Rationale

None

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

* CounterReport
* Company



Other software classes:

* VaccineCounterController


## 3.2. Sequence Diagram (SD)

![US6-SD](US6-SD.svg)

## 3.3. Class Diagram (CD)

![US6-CD](US6-CD.svg)

# 4. Tests 


# 5. Construction (Implementation)

## Class VaccineCounterController

    private VacCenter center;

    private String vacname;

    private static VacCenterStore vacCenterStore;

    private int Counter=0;



    public void GenerateReport(){

        this.company = App.getInstance().getCompany();
        this.vacCenterStore = company.getVacStore();

        try(FileReader reader =  new FileReader("config.properties")) {

            Properties properties = new Properties();
            properties.load(reader);
            String runTime = properties.getProperty("Time");
            LocalTime time = LocalTime.parse(runTime);
            Timer timer = new Timer();
            LocalDateTime currentTime = LocalDateTime.now();


            TimerTask task = new TimerTask() {
                @Override
                public void run() {

                    LocalDate todaysdate = LocalDate.now();


                    File file = new File("Report");
                    if (!file.exists()) {
                        try {
                            Files.createDirectory(Path.of(String.valueOf(file)));
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    file = new File("Report/1");
                    if (!file.exists()) {
                        try {
                            Files.createFile(Path.of(String.valueOf(file)));
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }

                    PrintWriter writer;
                    try {
                        writer = new PrintWriter(file);
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException(e);
                    }

                    for (int i = 0; i < vacCenterStore.getVacCenterlist().size(); i++) {
                        center = vacCenterStore.getVacCenters().get(i);
                        vacname = vacCenterStore.getVacCenterlist().get(i).getName();
                        for (int j = 0; j < center.getRecoveryRoom().size(); j++) {
                            if (center.getRecoveryRoom().get(j).getDay() == todaysdate) {
                                Counter++;
                                CounterReport report = new CounterReport(vacname, Counter);
                                writer.println(report);

                            }
                        }
                    }
                    writer.close();
                }

            };

            Calendar date = Calendar.getInstance();
            date.set(Calendar.HOUR_OF_DAY, time.getHour());
            date.set(Calendar.MINUTE, time.getMinute());
            date.set(Calendar.SECOND, time.getSecond());
            timer.schedule(task, date.getTime());
        }
        catch (FileNotFoundException e ) {
            System.out.println("Could not save data, file not found.");
        }
        catch (Exception e ) {
            e.printStackTrace();
        }
    }
}

# 6. Integration and Demo 

*None*


# 7. Observations

No observations.