 # US17 -   As a center coordinator, I want to import data from a legacy system that was used in the past to manage centers



## 1. Requirements Engineering

### 1.1. User Story Description

*As a center coordinator, I want to import data from a legacy system that was used in the past to manage centers. The imported data should be presented to the user sorted by arrival time or by the center leaving time. The name of the SNS user and the vaccine type Short Description attribute should also be presented to the user* 
### 1.2. Customer Specifications and Clarifications 
>**Question:**
>In the Sprint D requirements is stated that two sorting algorithms should be implemented and that the imported data should be sorted by arrival time  or center leaving time.
*Should each algorithm be capable of both sortings or is one of the algorithms supposed to do one (e.g. arrival time) and the other the remaining sorting criteria (e.g. leaving time)?*

>**Answer:**
>Each algorithm should be capable of doing both sortings. The application should be prepared to run both algorithms. The algorithm that will be used to sort data should be defined in a configuration file.

>**Question:** 
> "Is there any correct format for the lot number? Should we simply assume that the lot number will always appear like this 21C16-05 ,like it's written in the file, and not validate it?"

>**Answer:** 
>The lot number has five alphanumeric characters an hyphen and two numerical characters (examples: 21C16-05 and A1C16-22 )

> **Question:**
> When sorting data by arrival time or central leaving time, should we sort from greater to smallest or from smallest to greater? 2 - Also, should we consider only time or date also? So, for example, if we sort from smaller to greater and consider a date also, 20/11/2020 11:00 would go before 20/12/2020 08:00. Without considering the date (only time) it would be 20/12/2020 08:00 before 20/11/2020 11:00.

>**Answer:**
> The user must be able to sort in ascending and descending order.
>Date and time should be used to sort the data. Sort the data by date and then by time. 
>**Question:** 
> You answered to a previous question saying that the user should be able to sort by ascending or descending order. Should the user choose in the UI, the order in which the information should be presented? Or should this feature be defined in the configuration file?

>**Answer:** 
> The center coordinator must use the GUI to select the sorting type (ascending or descending).
>**Question:** 
> Should we show the sorted list in the GUI or in a exported file?
>**Answer:**
> Should show the sorted list in the GUI. In this US the application does not export data...

### 1.3. Acceptance Criteria
**AC01:**
*Imported file must be sorted following the time factors(Arrival or Center Leaving Time)*
**AC02**
*Two different algorithms should be implemented*
**AC03**
*SNSUser Name and vaccine discription must be displayed as all other provided information*
**AC04**
*Must be performed in GUI*
### 1.4. Found out Dependencies

Depdency from: 

- US3 : as this requires SNS users

- US9 : as this requires a Vaccination center;

- US10 : as this requires Employees (Center Coordinator);

### 1.5 Input and Output Data

**Input Data:**
- Time factor(Arrival or Center Leaving time)
- Algorithm (BubbleSort or InsertionSort Sort)
- File (list)

**Output Data:**
- Shows sorted imported list

### 1.6. System Sequence Diagram (SSD)

![US17-SSD](US17-SSD.svg)


### 1.7 Other Relevant Remarks

Nothing to reffer.

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US17-DM](US17-DM.svg)

### 2.2. Other Remarks

Nothing to reffer.


## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for...                           | Answer                   | Justification (with patterns)                                |
|:---------------|:----------------------------------------------------------------------|:-------------------------|:-------------------------------------------------------------|
| Step 1  		     | 		... interacting with the actor?						                               | SortLegacyDataGUI        | Pure Fabrication                                             |
| ... 		         | 		...coordinating the user story?				                                 | SortLegacyDataController | Controller                                                   |
|                | ... having the email of the center coordinator?                       | UserSession              | IE: the object has its own data                              |
|                | ... having the vaccination center where the center coordinator works? | EmployeeStore            | IE: records the vaccination center of the center coordinator |
| Step 2         | 		...Sorting data?					                                               | BubbleSort               | Interface                                                    |
|                | 		...Sorting data?				                                   | InsertionSort                  |              Interface                                                |
| Step 3		       | 			...fetching SNSUser information?				                               | SNSUserDTO               | Data Transfer Object (DTO)                                   |



### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

* Employee
* Company
* User
* SNSUserDTO


Other software classes (i.e. Pure Fabrication) identified: 
 * WaitRoomUI 
 * WaitRoomController
## 3.2. Sequence Diagram (SD)

![US17-SD](US17-SD.svg)

## 3.3. Class Diagram (CD)

![US17-CD](US17-CD.svg)

# 4. Tests 
**Test:**
*Sorting a list using BubbleSort algorithm comes out sorted*

    @Test
    public void BubbleSortIsSorted() throws ParseException {
    BubbleSort bubbleSort = new BubbleSort();
    String[][] list = new String[2][8], listsortemanual = new String[2][8];
    String dados1 = "161593120;Spikevax;Primeira;21C16-05;5/30/2022 8:00;5/30/2022 8:24;5/30/2022 9:11;5/30/2022 9:43";
    String dados2 = "161593121;Spikevax;Primeira;21C16-05;5/30/2022 8:00;5/30/2022 8:00;5/30/2022 8:17;5/30/2022 8:51";
    String[] dadosf1 = dados1.split(";"), dadosf2 = dados2.split(";");
    list[0] = dadosf1;
    list[1] = dadosf2;
    listsortemanual[0] = dadosf2;
    listsortemanual[1] = dadosf1;
    String[][] listsortedbuble = bubbleSort.sortByTime(list, 5);
    Assertions.assertArrayEquals(listsortemanual[0], listsortedbuble[0]);
    }
**Test:**
*Sorting a list using SelectionSort algorithm comes out sorted*


    @Test
    public void SelectionSortIsSorted() throws ParseException {
        SelectionSort selectionSort = new SelectionSort();
        String[][] list = new String[2][8], listsortemanual = new String[2][8];
        String dados1 = "161593120;Spikevax;Primeira;21C16-05;5/30/2022 8:00;5/30/2022 8:24;5/30/2022 9:11;5/30/2022 9:43";
        String dados2 = "161593121;Spikevax;Primeira;21C16-05;5/30/2022 8:00;5/30/2022 8:00;5/30/2022 8:17;5/30/2022 8:51";
        String[] dadosf1 = dados1.split(";"), dadosf2 = dados2.split(";");
        list[0] = dadosf1;
        list[1] = dadosf2;
        listsortemanual[0] = dadosf2;
        listsortemanual[1] = dadosf1;
        String[][] listsortedbuble = selectionSort.sortByTime(list, 5);
        Assertions.assertArrayEquals(listsortemanual[0], listsortedbuble[0]);
    }
**Test:**
*How many lines the .csv file has*


    @Test
    public void Howmanylines() throws IOException {
        SortLegacyDataController controller = new SortLegacyDataController();
        File file = new File("C:\\Users\\jnmte\\Documents\\lei_22_s2_1dn_g061\\testes.csv");
        int lines = controller.HowManyLines(file);
        Assertions.assertTrue(lines == 18);
    }
**Test:**
*User information is added to the list*


    @Test
    public void ListwithSnsName() {
        RegisterSNSUserController registerSNSUserController = new RegisterSNSUserController();
        SortLegacyDataController sortLegacyDataController = new SortLegacyDataController();
        SNSUser snsUser1 = new SNSUser("dasd@gmail.com", "Ruas", 123456789, 161593120, "Joao Pedro", 12345678, new Date(), "male");
        registerSNSUserController.saveSNSUser(snsUser1);
        String dados1 = "161593120;Spikevax;Primeira;21C16-05;5/30/2022 8:00;5/30/2022 8:24;5/30/2022 9:11;5/30/2022 9:43";
        String[] dadosf1 = dados1.split(";");
        String[][] listfinal = new String[1][9];
        listfinal[0] = dadosf1;
        Assertions.assertTrue(sortLegacyDataController.getListWithExtraInfo(listfinal)[0][1] == "Joao Pedro");
    }
**Test:**
*File selected is imported*


    @Test
    public void Importfile() throws IOException {
        SortLegacyDataController controller = new SortLegacyDataController();
        File filemanual = new File("C:\\Users\\jnmte\\Documents\\lei_22_s2_1dn_g061\\onlyfortestes.csv");
        File imported = controller.ImportFile("onlyfortestes.csv");
        Assertions.assertEquals(filemanual.getName(), imported.getName());
    }
**Test:**
*Getting the SNSUser via the SNSNumber*

    @Test
    public void SNSNumber() {
        RegisterSNSUserController registerSNSUserController = new RegisterSNSUserController();
        SortLegacyDataController sortLegacyDataController = new SortLegacyDataController();
        String name = "João";
        String email = "joao@isep.ipp.pt";
        String address = "Rua sao joao";
        int PhoneNumber = 123456789;
        int CCNumber = 12345678;
        int SNSNumber = 123456789;
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date birthday;
        {
            try {
                birthday = df.parse("24-09-2003");
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }
        Date birthdate = birthday;
        String sex = "masculino";
        SNSUser snsuser = new SNSUser(email, address, PhoneNumber, SNSNumber, name, CCNumber, birthdate, sex);
        registerSNSUserController.saveSNSUser(snsuser);
        Assertions.assertTrue(sortLegacyDataController.getSNSUserByNumber(SNSNumber)==snsuser);

    }
**Test:** 
*If imported data is added to the system*

    @Test
    public void addLegacyData(){
        SortLegacyDataController sortLegacyDataController = new SortLegacyDataController();
        VacCenter vacCenter = new VacCenter(
                "Vacinação Guimaraes",
                "Rua de Guimaraes",
                "912626999",
                "019283746",
                "website@test.com",
                9,
                19,
                12,
                200);
        String[][] list = new String[1][8];
        String dados1 = "161593120;Spikevax;Primeira;21C16-05;5/30/2022 8:00;5/30/2022 8:24;5/30/2022 9:11;5/30/2022 9:43";
        list[0]= dados1.split(";");
        sortLegacyDataController.addLegacyData(vacCenter,list);
        Assertions.assertEquals(dados1,vacCenter.getLegacydata().get(0));
    }

# 5. Construction (Implementation)
###SortLegacyDataController

    package app.controller;
    
    import app.domain.model.*;
    import app.domain.store.EmployeeStore;
    import app.domain.store.RegisterSNSUserStore;
    import app.domain.store.VacCenterStore;
    import app.mapper.SNSUserMapper;
    import app.mapper.dto.SNSUserDTO;
    import app.ui.console.utils.Utils;
    import pt.isep.lei.esoft.auth.AuthFacade;
    import pt.isep.lei.esoft.auth.UserSession;
    
    import java.io.*;
    import java.nio.file.Files;
    import java.nio.file.Path;
    import java.text.ParseException;
    import java.util.Arrays;
    import java.util.List;
    import java.util.Scanner;
    
    
    public class SortLegacyDataController {
    boolean val;
    private String fileName;
    private File file;
    private BufferedReader csvReader;
    public String[] header;
    private String firstline;
    private String line;
    private BubbleSort bubbleSort;
    private boolean isnull;
    private final RegisterSNSUserStore snsuserstore;
    private Company company;
    private SNSUserMapper mapper;
    public SelectionSort selectionSort;
    private RegisterVaccineController vacinecontroller;
    private VacCenterStore vacCenterStore;
    private AuthFacade authFacade;
    private EmployeeStore employeeStore;
    public int SNSUSerNumber = 0, VaccineName = 1, Dose = 2, LotNumber = 3, ScheduledDateTime = 4, ArrivalDateTime = 5, NurseAdministrationDateTime = 6, LeavingDateTime = 7;

    public SortLegacyDataController() {
        this.company = App.getInstance().getCompany();
        bubbleSort = new BubbleSort();
        selectionSort = new SelectionSort();
        snsuserstore = company.getSNSUserStore();
        this.mapper = new SNSUserMapper();
        vacinecontroller = new RegisterVaccineController();
        vacCenterStore = new VacCenterStore();
        authFacade = new AuthFacade();
        this.employeeStore = company.getEmployeeStore();
    }

    public File ImportFile(String fileName) {
        if (fileName == "onlyfortestes.csv") {
            file = new File(fileName);
        } else {
            do {
                fileName = Utils.readLineFromConsole("Please insert the CSV file name (filename.csv): ");
                file = new File(fileName);
                val = true;

                if (!Files.exists(Path.of(fileName)) || !fileName.endsWith(".csv")) {
                    System.out.println("File does not exists or its not a csv file");
                    val = false;
                }

            } while (!val);
        }
        return file;
    }

    public String[][] ReadFileAndCreateList(File file) throws IOException {
        int lines;
        csvReader = new BufferedReader(new FileReader(file));
        firstline = csvReader.readLine();
        header = firstline.split(";");
        String[] dados;
        int count = 0;
        Boolean hasHeader = firstline.contains("SNSUSerNumber");
        if (hasHeader == true)
            lines = -1;
        else
            lines = 0;
        int totallines = HowManyLines(file);
        String[][] listdata = new String[totallines][8];
        if (hasHeader == true)
            for (int i = 0; i < header.length; i++) {
                switch (header[i].toUpperCase()) {
                    case "SNSUSERNUMBER":
                        SNSUSerNumber = i;
                        break;
                    case "VACCINENAME":
                        VaccineName = i;
                        break;
                    case "DOSE":
                        Dose = i;
                        break;
                    case "LOTNUMBER":
                        LotNumber = i;
                        break;
                    case "SCHEDULEDATETIME":
                        ScheduledDateTime = i;
                        break;
                    case "ARRIVALDATETIME":
                        ArrivalDateTime = i;
                        break;
                    case "NURSEADMINISTRATIONDATETIME":
                        NurseAdministrationDateTime = i;
                        break;
                    case "LEAVINGDATETIME":
                        LeavingDateTime = i;
                        break;
                }
            }
        while ((line = csvReader.readLine()) != null) {
            dados = line.split(";");
            listdata[count][0] = dados[SNSUSerNumber];
            listdata[count][1] = dados[VaccineName];
            listdata[count][2] = dados[Dose];
            listdata[count][3] = dados[LotNumber];
            listdata[count][4] = dados[ScheduledDateTime];
            listdata[count][5] = dados[ArrivalDateTime];
            listdata[count][6] = dados[NurseAdministrationDateTime];
            listdata[count][7] = dados[LeavingDateTime];
            count++;
        }
        return listdata;}

    public int HowManyLines(File file) throws IOException {
        int lines = 0;
        BufferedReader readfile = new BufferedReader(new FileReader(file));
        firstline = readfile.readLine();
        while ((line = readfile.readLine()) != null)
            lines++;
        return lines;
    }

    public String[][] getListWithExtraInfo(String[][] list) {
        String[][] finalversion = new String[list.length][10];
        String name;
        SNSUserDTO user;
        String descripton = "Not registered";
        int snsnumber;
        for (int i = 0; i < list.length; i++) {
            finalversion[i][0] = list[i][SNSUSerNumber];
            snsnumber = Integer.parseInt(list[i][SNSUSerNumber]);
            if (getSNSUserByNumber(snsnumber) == null)
                name = "Not registered";
            else
                name = mapper.toDTO(getSNSUserByNumber(snsnumber)).getName();
            finalversion[i][1] = name;
            finalversion[i][2] = list[i][VaccineName];
            if (vacinecontroller.getVaccines() != null) {
                for (int j = 0; j < vacinecontroller.getVaccines().size(); j++) {
                    if (vacinecontroller.getVaccines().get(j).getBrand() == (list[i][VaccineName])) {
                        finalversion[j][3] = vacinecontroller.getVaccines().get(i).getDescription();
                    }
                }
            }
            finalversion[i][3] = descripton;
            finalversion[i][4] = list[i][Dose];
            finalversion[i][5] = list[i][LotNumber];
            finalversion[i][6] = list[i][ScheduledDateTime];
            finalversion[i][7] = list[i][ArrivalDateTime];
            finalversion[i][8] = list[i][NurseAdministrationDateTime];
            finalversion[i][9] = list[i][LeavingDateTime];

        }

        return finalversion;
    }

    public void ShowList(String[][] list) {
        for (int i = 0; i < list.length; i++) {
            System.out.println(Arrays.toString(list[i]));
        }
    }

    public String[][] bubbleSort(String[][] listdata, int timechosen) throws ParseException {
        return (bubbleSort.sortByTime(listdata, timechosen));
    }

    public String[][] selectionSort(String[][] listdata, int timechosen) throws ParseException {
        return (selectionSort.sortByTime(listdata, timechosen));
    }

    public SNSUser getSNSUserByNumber(int snsnum) {
        SNSUser usersns = null;
        for (int i = 0; i < snsuserstore.getSNSUsers().size(); i++) {
            if (snsnum == snsuserstore.getSNSUsers().get(i).getSNSNumber()) {
                usersns = snsuserstore.getSNSUsers().get(i);
            }
        }
        return usersns;
    }

    public boolean addLegacyData(VacCenter centeratm, String[][] list) {
       centeratm.addLegacyData(list);
        return false;

    }
    public String getUserEmail() {
        UserSession info = authFacade.getCurrentUserSession();

        return info.getUserId().toString();
    }
    public VacCenter getVaccinationCenter(String email) {

        VacCenter center = null;

        List<Employee> employeeList = employeeStore.getEmployees();

        for (Employee employee : employeeList) {
            if (email.equals(employee.getEmail())) {
                center = employee.getVacCenter();
            }
        }
        return center;
    }
    public String readLoginInfoFile() throws FileNotFoundException {

        File file = new File("LoginInfo.txt");
        Scanner reader = new Scanner(file);
        String email = reader.nextLine();
        reader.close();

        return email;
    }

    }

###BuubleSort
    public String[][] sortByTime(String[][] listdata,int timechosen) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm");
        String[] aux;
        for (int i = 0; i < listdata.length; i++) {
            for (int j = 0; j < listdata.length - 1 - i; j++) {
                Date temp = df.parse(listdata[j][timechosen]);
                Date temp2 = df.parse(listdata[j+1][timechosen]);
                if ((temp).after(temp2)){
                    aux=listdata[j];
                    listdata[j]=listdata[j+1];
                    listdata[j+1]=aux;
                }
            }
        }
        return listdata;
    }
###SelectionSort
    public String[][] sortByTime(String[][] listdata, int timechosen) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm");
        int pos;
         String[] aux;
        for (int i = 0; i < listdata.length; i++)
        {
            pos = i;
            for (int j = i+1; j < listdata.length; j++)
            {
                Date jdate = df.parse(listdata[j][timechosen]);
                Date posdate = df.parse(listdata[pos][timechosen]);
                if ((jdate.before(posdate)))                  //find the index of the minimum element
                {
                    pos = j;
                }
            }
            aux=listdata[pos];
            listdata[pos]=listdata[i];
            listdata[i]=aux;
        }
        return listdata;
    }

###SortLegacyDataUI

    package app.ui.console;
    import app.domain.model.VacCenter;
    import app.ui.console.utils.Utils;
    import app.controller.SortLegacyDataController;
    import java.io.File;
    import java.io.FileNotFoundException;
    import java.io.IOException;
    import java.text.ParseException;
    public class SortLegacyDataUI implements Runnable {
    private SortLegacyDataController controller;

    public SortLegacyDataUI() {controller = new SortLegacyDataController();}

    File file;

    public void run() {
        int time = 0;
        String email = null;
        try {
            email = controller.readLoginInfoFile();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        VacCenter centeratm = controller.getVaccinationCenter(email);
        file = controller.ImportFile("");
        try {
            String[][] list = controller.ReadFileAndCreateList(file);
            if (list == null) {
                Utils.printToConsole("File is corrupted!");
            } else {
                if (list[0].length != 8) {
                    Utils.printToConsole("File is corrupted!");
                } else {
                    Utils.printToConsole("File imported correctly\n");
                    controller.ShowList(list);
                    boolean val1;
                    String Opt = "";
                    do {
                        val1 = false;
                        Opt = Utils.readLineFromConsole("Confirm the File? (y-yes/n-no)");
                        if (Opt.equalsIgnoreCase("y") || Opt.equalsIgnoreCase("yes") || Opt.equalsIgnoreCase("n")
                                || Opt.equalsIgnoreCase("no")) {
                            val1 = true;
                        } else
                            Utils.printToConsole("Please input only : (y-yes/n-no)");
                    } while (!val1);
                    if (Opt.equalsIgnoreCase("y") || Opt.equalsIgnoreCase("yes")) {
                        int timechosen;
                        int alg;
                        boolean val;
                        do {
                            timechosen = Utils.readIntegerFromConsole("Sort by: 1- Arrival time | 2 - Center Leaving Time ");
                            if(timechosen!= 1 && timechosen!=2) Utils.printToConsole("ERROR : Chose between the options provided!");
                        } while (timechosen!= 1 && timechosen!=2);
                        do{
                            alg = Utils.readIntegerFromConsole("Sort using : 1- BubbleSort | 2-SelectionSort");
                            if(alg!=1 && alg!=2) Utils.printToConsole("ERROR : Chose between the options provided!");
                        }while(alg!=1 && alg!=2);
                        String[][] newlist = new String[0][];
                        if (timechosen == 1 && alg == 1) {
                            newlist = controller.bubbleSort(list, controller.ArrivalDateTime);
                        }
                        if (timechosen == 1 && alg == 2) {
                            newlist = controller.selectionSort(list, controller.ArrivalDateTime);
                        }

                        if (timechosen == 2 && alg == 1) {
                            newlist = controller.bubbleSort(list, controller.LeavingDateTime);
                        }
                        if (timechosen == 2 && alg == 2) {
                            newlist = controller.selectionSort(list, controller.LeavingDateTime);
                        }
                        String[][] listextrainfo = controller.getListWithExtraInfo(newlist);
                        controller.ShowList(listextrainfo);
                        controller.addLegacyData(centeratm,listextrainfo);
                    }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    }
###SortLegacyDataGUI
    package app.ui.gui;
    import app.controller.SortLegacyDataController;
    import app.domain.model.VacCenter;
    import app.ui.JavaFX_Launcher;
    import javafx.application.Application;
    import javafx.collections.ObservableList;
    import javafx.fxml.FXML;
    import javafx.fxml.FXMLLoader;
    import javafx.scene.Parent;
    import javafx.scene.Scene;
    import javafx.scene.control.*;
    import javafx.stage.FileChooser;
    import javafx.stage.Stage;
    import java.io.File;
    import java.io.FileNotFoundException;
    import java.io.IOException;
    import java.text.ParseException;
    import java.util.Arrays;
    import java.util.Objects;
    import javafx.beans.property.SimpleStringProperty;
    import javafx.beans.value.ObservableValue;
    import javafx.collections.FXCollections;
    import javafx.scene.control.TableColumn;
    import javafx.scene.control.TableView;
    import javafx.scene.control.TableColumn.CellDataFeatures;
    import javafx.util.Callback;
    
    public class SortLegacyDataGUI extends Application implements Runnable {
    public Label filepath=new Label();
    public Button ButtonFile = new Button();
    public RadioButton rbt1 = new RadioButton();
    public RadioButton rbt2 = new RadioButton();
    public RadioButton rbalg1 = new RadioButton();;
    public RadioButton rbalg2 = new RadioButton();;
    public static Runnable previous;
    private SortLegacyDataController us17controller;
    public File selectedfile;
    public String[][] unsortedlist;
    public String[][] sortlist ;
    public TableView<String[]> table = new TableView<>();
    ObservableList<String[]> data = FXCollections.observableArrayList();
    public SortLegacyDataGUI() {
    us17controller = new SortLegacyDataController();
    }

    public static void setPrevious(Runnable previous) {
        SortLegacyDataGUI.previous = previous;
    }
    @FXML
    private void initialize() {
        final ToggleGroup algo = new ToggleGroup();
        final ToggleGroup timefactor = new ToggleGroup();
        rbt1.setToggleGroup(timefactor);
        rbt2.setToggleGroup(timefactor);
        rbalg1.setToggleGroup(algo);
        rbalg2.setToggleGroup(algo);
        table.toBack();
        table.setOpacity(0);

    }
    @Override
    public void run() {
        JavaFX_Launcher.myLaunch(getClass());
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/SortLegacyData.fxml")));
        stage.setTitle("Sort Legacy Data");
        stage.setScene(new Scene(root, 700, 500));
        stage.setMaxHeight(500);
        stage.setMaxWidth(700);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.show();
    }


    public void back() {
        new CenterCoordinatorGUI().run();
    }

    public void ButtonFile() throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        selectedfile = fileChooser.showOpenDialog(null);
        if (selectedfile != null) {
            unsortedlist = us17controller.ReadFileAndCreateList(selectedfile);
            filepath.setText(selectedfile.getPath());
        }
    }
    public void ButtonCheck() throws ParseException, FileNotFoundException {
        if (rbt1.isSelected()&&rbalg1.isSelected()){
            sortlist = us17controller.bubbleSort(unsortedlist, us17controller.ArrivalDateTime);
        }
        if (rbt1.isSelected()&&rbalg2.isSelected()){
            sortlist = us17controller.selectionSort(unsortedlist, us17controller.ArrivalDateTime);

        }
        if (rbt2.isSelected()&&rbalg1.isSelected()){
            sortlist = us17controller.bubbleSort(unsortedlist, us17controller.LeavingDateTime);

        }
        if (rbt2.isSelected()&&rbalg2.isSelected()){
            sortlist = us17controller.selectionSort(unsortedlist, us17controller.LeavingDateTime);
        }
        String[][] finallist = us17controller.getListWithExtraInfo(sortlist);
        String email = us17controller.readLoginInfoFile();
        VacCenter center = us17controller.getVaccinationCenter(email);
        data.addAll(Arrays.asList(finallist));
        String[] header= {"SNSUSerNumber","SNSUser Name","VaccineName","Vaccine Description","Dose","LotNumber","ScheduledDateTime","ArrivalDateTime","NurseAdministrationDateTime","LeavingDateTime"};
        for (int i = 0; i < finallist[0].length; i++) {
            TableColumn tc = new TableColumn(header[i]);
            final int colNo = i;
            tc.setCellValueFactory(new Callback<CellDataFeatures<String[], String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(CellDataFeatures<String[], String> p) {
                    return new SimpleStringProperty((p.getValue()[colNo]));
                }
            });
            tc.setPrefWidth(90);
            table.getColumns().add(tc);
            table.setItems(data);
        }
        table.toFront();
        table.setOpacity(1.0);
        us17controller.addLegacyData(center,finallist);

    }

    }
###SortLegacyDataFXML
    <?xml version="1.0" encoding="UTF-8"?>
    
    <?import javafx.scene.control.*?>
    <?import javafx.scene.effect.*?>
    <?import javafx.scene.layout.*?>
    <?import javafx.scene.text.*?>
    
    <AnchorPane prefHeight="500.0" prefWidth="700.0" style="-fx-background-color: white;" xmlns="http://javafx.com/javafx/16" xmlns:fx="http://javafx.com/fxml/1" fx:controller="app.ui.gui.SortLegacyDataGUI">
        <Button layoutX="212.0" layoutY="428.0" mnemonicParsing="false" onAction="#back" prefHeight="30.0" prefWidth="108.0" style="-fx-background-color: #218a68; -fx-background-radius: 50px;" text="BACK" textFill="WHITE">
            <font>
                <Font name="System Bold" size="14.0" />
            </font>
            <effect>
                <DropShadow blurType="TWO_PASS_BOX" />
            </effect>
        </Button>
        <Button fx:id="ButtonFile" layoutX="487.0" layoutY="245.0" mnemonicParsing="false" onAction="#ButtonFile" prefHeight="28.0" prefWidth="108.0" style="-fx-background-color: #218a68;" text="Choose File" textFill="WHITE" />
        <Label fx:id="filepath" alignment="TOP_CENTER" layoutX="434.0" layoutY="282.0" opacity="0.4" prefWidth="214.0" style="-fx-background-color: #e7ebee;" text="...FileDirectory...">
            <font>
                <Font size="14.0" />
            </font>
        </Label>
        <Button layoutX="380.0" layoutY="428.0" mnemonicParsing="false" onAction="#ButtonCheck" prefHeight="30.0" prefWidth="108.0" style="-fx-background-color: #218a68; -fx-background-radius: 50px;" text="CONFIRM" textFill="WHITE">
            <font>
                <Font name="System Bold" size="14.0" />
            </font>
            <effect>
                <DropShadow blurType="TWO_PASS_BOX" />
            </effect>
        </Button>
        <Pane prefHeight="90.0" prefWidth="700.0" style="-fx-background-color: #218a68; -fx-border-color: lightgray;" AnchorPane.bottomAnchor="410.0" AnchorPane.leftAnchor="0.0" AnchorPane.rightAnchor="0.0" AnchorPane.topAnchor="0.0">
            <Label alignment="CENTER" contentDisplay="CENTER" layoutX="207.0" layoutY="26.0" prefHeight="38.0" prefWidth="287.0" text="LEGACY DATA IMPORT" textFill="WHITE">
                <font>
                    <Font name="System Bold" size="23.0" />
                </font>
            </Label>
        </Pane>
        <Pane layoutX="35.0" layoutY="188.0" prefHeight="57.0" prefWidth="345.0" style="-fx-background-color: #e7ebee; -fx-background-radius: 75px;">
            <RadioButton fx:id="rbt1" layoutX="25.0" layoutY="25.0" mnemonicParsing="false" text="Arrival Time">
                <font>
                    <Font size="15.0" />
                </font>
                <toggleGroup>
                    <ToggleGroup fx:id="timefactor" />
                </toggleGroup>
            </RadioButton>
            <RadioButton fx:id="rbt2" layoutX="150.0" layoutY="23.0" mnemonicParsing="false" prefHeight="25.0" prefWidth="181.0" text="Center Leaving Time">
                <font>
                    <Font size="15.0" />
                </font>
                <toggleGroup>
                    <fx:reference source="timefactor" />
                </toggleGroup>
            </RadioButton>
            <Label layoutX="31.0" text="Time factor">
                <font>
                    <Font size="19.0" />
                </font>
            </Label>
        </Pane>
        <Pane layoutX="35.0" layoutY="278.0" prefHeight="57.0" prefWidth="345.0" style="-fx-background-color: #e7ebee; -fx-background-radius: 75px;">
            <RadioButton fx:id="rbalg1" layoutX="22.0" layoutY="33.0" mnemonicParsing="false" text="BubbleSort">
                <font>
                    <Font size="15.0" />
                </font>
                <toggleGroup>
                    <ToggleGroup fx:id="algo" />
                </toggleGroup>
            </RadioButton>
            <RadioButton fx:id="rbalg2" layoutX="147.0" layoutY="33.0" mnemonicParsing="false" text="SelectionSort">
                <font>
                    <Font size="15.0" />
                </font>
                <toggleGroup>
                    <fx:reference source="algo" />
                </toggleGroup>
            </RadioButton>
            <Label layoutX="30.0" layoutY="5.0" text="Algorithm">
                <font>
                    <Font size="19.0" />
                </font>
            </Label>
        </Pane>
       <TableView fx:id="table" layoutX="14.0" layoutY="-13.0" prefHeight="419.0" prefWidth="700.0" AnchorPane.bottomAnchor="81.0" AnchorPane.leftAnchor="0.0" AnchorPane.rightAnchor="0.0" AnchorPane.topAnchor="0.0" />
    
    </AnchorPane>




# 6. Integration and Demo 

Most methods used on this UC were made specifically for this one which made it more challenging as well as implementing the GUI.
# 7. Observations

Nothing to observe.