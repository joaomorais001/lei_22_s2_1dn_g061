# US08 - Record the administration of a vaccine to a SNS user

## 1. Requirements Engineering

### 1.1. User Story Description

*A nurse wants to record the administration of a vaccine to a user.*

### 1.2. Customer Specifications and Clarifications 
>**Question 1**:
>"As we can read in Project Description, the vaccination flow follows these steps: 
> 1- Nurse calls one user that is waiting in the waiting room to be vaccinated 
> 2 -Nurse checks the user's health data as well as which vaccine to administer 
> 3- Nurse administers the vaccine and registers its information in the system.
   The doubt is: do you want US08 to cover steps 2 and 3, or just step 3?"  
> 
>**Answer:**
>"1.The nurse selects a SNS user from a list. 2. Checks user's Name, Age and Adverse Reactions registered in the system. 3. Registers information about the administered vaccine.
"

>**Question 2**:
>" 1: The system displays the list of possible vaccines to be administered (considering the age group of the user); then the nurse selects the dose she is going to administer and gets information about the dosage. But wouldn't it be more correct, since the system knows the vaccination history, in other words, if the user has already take x dose(s) of that vaccine, to simply show the dose and the respective dosage and not ask for the nurse to arbitrarily select it?
2: After giving the vaccine to the user, how should the nurse register the vaccine type? by the code?"
>
>**Answer:**
>"1- If it is the first dose, the application should show the list of possible vaccines to be administered. If is is not a single dose vaccine, when the SNS user arrives to take the vaccine, the system should simply show the dose and the respective dosage.
2- A vaccine is associated with a given vaccine type. Therefore, there is no need to register the vaccine type.
Moreover, the nurse should also register the vaccine lot number (the lot number has five alphanumeric characters an hyphen and two numerical characters (example: 21C16-05))"

>**Question 3**:
>"Supposing that the SNS user has already received a dose of a given vaccine type (for example, COVID-19), the user can only receive the same vaccine or a different one with the same vaccine type? ">
> 
>**Answer:**
>"Answer: The SNS user can only receive the same vaccine.
Related information:
A SNS user is fully vaccinated when he receives all doses of a given vaccine.
A SNS user that has received a single-dose vaccine is considered fully vaccinated and will not take more doses.
A SNS user that is fully vaccinated will not be able to schedule a new vaccine of the type for which he is already fully vaccinated."

>**Question 4**:
>"To access the user info - scheduled vaccine type and vaccination history -, should the nurse enter user's SNS number?"
> 
>**Answer:**
>"The nurse should select a SNS user from a list of users that are in the center to take the vaccine."

### 1.3. Acceptance Criteria
**AC01:**
*The nurse should select a vaccine and the administrated dose *

### 1.4. Found out Dependencies

Depdency from: 

US8 SNS users in recovery room;

US9 Vaccine Centers.



### 1.5 Input and Output Data

**Input Data:**
* Vaccine Center
* Vaccine

**Output Data:**
* Confirmation of Vaccine Administration
* SMS to the User

### 1.6. System Sequence Diagram (SSD)

![US8-SSD](US8-SSD.svg)


### 1.7 Other Relevant Remarks

None

## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US8-MD](US8-DM.svg)

### 2.2. Other Remarks

None


## 3. Design - User Story Realization 

### 3.1. Rationale

| Interaction ID | Question: Which class is responsible for...   | Answer                           | Justification (with patterns)                                                                                 |
|:---------------|:----------------------------------------------|:---------------------------------|:--------------------------------------------------------------------------------------------------------------|
| Step 1  		     | 	... interacting with the actor?              | VaccineAdministrationUI          | Pure Fabrication: there is no reason to assign this responsibility to any existing class in the Domain Model. |
| 	Step 2		  		        | 	... coordinating the US?                     | registerAdministrationController | Controller |              
| 	Step 3 	  		   | 	... getting info  | Company                          |        |

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

* Company



Other software classes (i.e. Pure Fabrication)identified:
* VaccineAdministrationUI
* VaccineCounterController


## 3.2. Sequence Diagram (SD)

![US8-SD](US8-SD.svg)

## 3.3. Class Diagram (CD)

![US8-CD](US8-CD.svg)

# 4. Tests 

## Test 1




# 5. Construction (Implementation)



# 6. Integration and Demo 




# 7. Observations

No observations.