# Integrating Project for the 2nd Semester of LEI-ISEP 2021-22 

# 1. Team Members

The teams consists of students identified in the following table. 

| Student Number	 | Name           |
|-----------------|----------------|
| **1201942**     | Fábio Silva    |
| **1201806**     | João Veríssimo |
| **1211366**     | João Morais    |
| **1200941**     | José Teixeira  |
| **1201694**     | Tiago Castro   |



# 2. Task Distribution ###


Throughout the project's development period, the distribution of _tasks / requirements / features_ by the team members was carried out as described in the following table. 

**Keep this table must always up-to-date.**

| Task                        | [Sprint A](SprintA/README.md)       | [Sprint B](SprintB/README.md) | [Sprint C](SprintC/README.md) |  [Sprint D](SprintD/README.md) |
|-----------------------------|-------------------------------------|------------|------------|------------|
| Glossary                    | [all](SprintA/Glossary.md)          |   [all](SprintB/Glossary.md)  |   [all](SprintC/Glossary.md)  | [all](SprintD/Glossary.md)  |
| Use Case Diagram (UCD)      | [all](SprintA/UCD.md)               |   [all](SprintB/UCD.md)  |   [all](SprintC/UCD.md)  | [all](SprintD/UCD.md)  |
| Supplementary Specification | [all](SprintA/FURPS.md)             |   [all](SprintB/FURPS.md)  |   [all](SprintC/FURPS.md)  | [all](SprintD/FURPS.md)  |
| Domain Model                | [all](SprintA/Domain%20Model/DM.md) |   [all](SprintB/DM.md)  |   [all](SprintC/DM.md)  | [all](SprintD/DM.md)  |
| US 001 (SDP Activities)     | [--](SprintA/US001.md)              |    |   |  |
| US 002 (SDP Activities)     | [--](SprintA/US002.md)              |    | [1211366]  |  |
| US 003 (SDP Activities)     | [--](SprintB/US3/US3.md)            |[1211366]    |   |  |
| US 004 (SDP Activities)     | [--](SprintA/US004.md)              |    | [1200941]  |  |
| US 005 (SDP Activities)     | [--](SprintA/US005.md)              |    | [1201942]  |  |
| US 006 (SDP Activities)     | [--](SprintA/US006.md)              |    |   |  |
| US 007 (SDP Activities)     | [--](SprintA/US007.md)              |    |   |  |
| US 008 (SDP Activities)     | [--](SprintA/US008.md)              |    |   |  |
| US 009 (SDP Activities)     | [--](SprintB/US9/US9.md)            |[1201694]    |   |  |
| US 010 (SDP Activities)     | [--](SprintB/US10/US10.md)          |[1201942]    |   |  |
| US 011 (SDP Activities)     | [--](SprintB/US11/US11.md)          |[1200941]    |   |  |
| US 012 (SDP Activities)     | [--](SprintA/US012.md)              |    |   |  |
| US 013 (SDP Activities)     | [--](SprintC/US 14/US14.md)          |[1201806]    |   |  |
| US 014 (SDP Activities)     | [--](SprintA/US014.md)              |    |   |  |
| US 015 (SDP Activities)     | [--](SprintA/US015.md)              |    |   |  |
| US 016 (SDP Activities)     | [--](SprintA/US016.md)              |    |   |  |
| US 017 (SDP Activities)     | [--](SprintA/US017.md)              |    |   |  |


